import Layout from '../views/Layout';
import Login from '../views/Login';
import UsersIndex from '../views/users/Index'

export default [{
    path: '/',
    component: Layout,
    children: [{
        path: '/users',
        component: UsersIndex,
    }],
}, {
    path: '/login',
    component: Login,
}];
