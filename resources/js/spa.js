import Vue from 'vue'
import VueRouter from 'vue-router'
import Buefy from 'buefy'

import App from './App.vue'
import routes from './routes/routes'

Vue.use(VueRouter)
Vue.use(Buefy, {
    defaultIconPack: 'fas',
})

const router = new VueRouter({
  routes,
  linkActiveClass: 'active',
  mode: 'history',
  base: '/app',
  scrollBehavior: (to) => {
    if (to.hash) {
      return {selector: to.hash}
    } else {
      return { x: 0, y: 0 }
    }
  }
});

new Vue({
  el: '#app',
  render: h => h(App),
  router,
});
