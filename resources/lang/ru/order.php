<?php

return [
    
    'Processing' => 'В обработке',
    'Problem' => 'Имеются проблемы',
    'PaymentDue' => 'Ожидает оплаты',
    'PickupAvailable' => 'Ожидает отправки',
    'InTransit' => 'На доставке',
    'Delivered' => 'Доставлен',
    'Cancelled' => 'Отменен',
    'Returned' => 'Возвращен',

];