<?php

return [

    'unit' => [
        'piece' => 'штука',
        'set' => 'комплект',
        'pack' => 'упаковка',
        'size_range' => 'линейка',
    ],

    'price' => [
    	'per_piece' => 'Оптовая цена',
    	'per_set' => 'Цена за комплект',
    	'per_pack' => 'Цена за упаковку',
    	'per_size_range' => 'Цена за линейку',
    ],

    'availability' => [
        'in_stock' => 'В наличии',
        'out_of_stock' => 'Нет в наличии',
        'pre_order' => 'Под заказ',
        'quarantine' => 'Карантин',
    ],

];
