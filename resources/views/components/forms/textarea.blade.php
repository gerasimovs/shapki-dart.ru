<div class="form-group">
    @if ($markdown ?? false)
        @include('admin.partials.markdown-manual')
    @endif
    <label for="input-{{ $name }}">{{ $slot }}</label>
    <textarea id="input-{{ $name }}" class="form-control{{ $errors->has($name) ? ' is-invalid' : '' }}" name="{{ $name }}" rows="5">{{ $value ?? $entity->name ?? old($name) }}</textarea>
    @if ($errors->has('$name'))
        <span class="invalid-feedback">
            <strong>{{ $errors->first($name) }}</strong>
        </span>
    @endif
</div>