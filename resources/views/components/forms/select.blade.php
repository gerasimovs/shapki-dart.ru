<div class="form-group">
    <label for="input-{{ $name }}">{{ $slot }}</label>
    <select id="input-{{ $name }}" class="form-control{{ $errors->has($name) ? ' is-invalid' : '' }}" name="{{ $name }}">
        @foreach ($options as $value => $option)
            <option value="{{ $value }}" {{ isset($selected) && $selected == $value ? 'selected' : '' }}>{{ $option }}</option>
        @endforeach
    </select>
    @if ($errors->has($name))
        <span class="invalid-feedback">
            <strong>{{ $errors->first($name) }}</strong>
        </span>
    @endif
</div>


