@empty($id)
	@php
		$id = $name;
	@endphp
@endempty
<div class="form-group">
    <label for="input-{{ $id }}">{{ $slot }}</label>
    @component('components.forms._input', ['id' => 'input-' . $id, 'name' => $name, 'type' => $text ?? 'text'])
    	{{ $value ?? '' }}
    @endcomponent
    @if ($errors->has($name))
        <span class="invalid-feedback">
            <strong>{{ $errors->first($name) }}</strong>
        </span>
    @endif
</div>