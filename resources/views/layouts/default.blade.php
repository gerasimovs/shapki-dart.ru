<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    @include('partials.head')
    <style type="text/css">
        @yield('userCss')
    </style>
</head>
<body>
    <header>@include('partials.topbar')</header>
    <main class="container">
        @include('partials.status')
        @yield('content')
    </main>
    <footer>
        <div class="container">
            <ul class="list-inline">
                <li class="list-inline-item">
                    <a href="#">Вверх!</a>
                </li>
                <li class="footer-menu-divider list-inline-item">&sdot;</li>
                <li class="list-inline-item">
                    <a href="{{ route('about') }}">О компании</a>
                </li>
                <li class="list-inline-item">
                    <a href="{{ route('rules') }}">Правила работы</a>
                </li>
                <li class="footer-menu-divider list-inline-item">&sdot;</li>
                <li class="list-inline-item">
                    <a href="{{ route('catalog') }}">Коллекции</a>
                </li>
            </ul>
            <p class="copyright text-muted small">Материалы принадлежат &copy; ООО "Дарт", 2017 &mdash; {{date('Y')}}</p>
        </div> <!-- /.container -->
    </footer>
    @include('partials.javascripts')
    @yield('userJs')
    @stack('scripts')
</body>

</html>

