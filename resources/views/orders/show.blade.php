@extends('layouts.default')

@section('content')
    @if (count($offersGroup))
    <h3 class="my-4">Ваш заказ <span class="text-muted">№{{ $order->id }} от @datetime($order->created_at)</span></h3>
    <div class="table-responsive">
        <table class="table bg-light">
            <thead class="thead-light">
                <tr>
                    <th colspan="2">Товар</th>
                    <th class="cart-qty__title">Кол-во</th>
                    <th class="cart-price__title"><abbr title="Указана оптовая цена">Цена</abbr> <sup>руб.</sup></th>
                    <th class="cart-price__title">Итого <sup>руб.</sup></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($offersGroup as $offers)
                    @foreach ($offers as $offer)
                    <tr>
                        @if ($loop->first)
                            <td rowspan="{{ count($offers) + 1 }}">
                                <div class="cart-thumbs" style="background-image: url({{ $offer->product['images'][0]['url'] }});"></div>
                                @if ($offer->product->trashed())
                                    {{ $offer->pivot->vendor_code }}
                                @else
                                <a href="{{ route('products.show', $offer->pivot->product_id) }}">
                                    {{ $offer->pivot->vendor_code }}
                                </a>
                                @endif
                            </td>
                        @endif
                        <td>{{ $offer->option->name }}</td>
                        <td>
                            <form class="input-group input-group-sm">
                                <input type="text" class="form-control text-center" value="{{ $offer->pivot->qty }}" readonly>
                            </form>
                        </td>
                        <td class="text-right">@money($offer->pivot->price)</td>
                        <td class="text-right">@money($offer->pivot->price * $offer->pivot->qty)</td>
                    </tr>
                    @endforeach
                        <tr>
                            <td class="font-italic">Всего по позиции:</td>
                            <td>
                                <input type="text" class="form-control form-control-sm text-center" value="{{ $offers->sum('pivot.qty') }}" readonly>
                            </td>
                            <td>&nbsp;</td>
                            <td class="font-italic text-right">@money($offers->sum('pivot.qty') * $offer->pivot->price)</td>
                        </tr>
                @endforeach
                <tr>
                    <td colspan="2">Всего по заказу:</td>
                    <td><input type="text" class="form-control text-center" value="{{ $order->totalQty }}" readonly></td>
                    <td>&nbsp;</td>
                    <td class="text-right">@money($totalPrice)</td>
                </tr>
            </tbody>
        </table>
        @if ($order->comment)
            <div class="alert alert-success" role="alert">
                <p class="alert-heading font-italic">Комментарий к заказу:</p>
                <p class="mb-0">{{ $order->comment }}</p>
            </div>
        @endif
    </div>
    @endif

@endsection