<h3>Заказы</h3>

@if (count($orders))
    <div class="table-responsive">
        <table class="table bg-light">
            <thead class="thead-light">
                <tr>
                    <th>#</th>
                    <th>Товаров в заказе</th>
                    <th>Сумма заказа</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($orders as $order)
                    <tr>
                        <td><a href="{{ route('user.orders.show', $order->id) }}">Заказ от @datetime($order->created_at)</a></td>
                        <td>{{ $order->totalQty }}</td>
                        <td>{{ number_format($order->totalPrice, 2, ',', ' ') }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@else
    <p>Отсутствуют</p>
@endif