@isset ($notifications)
    @include('partials.pagination', ['items' => $notifications])
    @foreach ($notifications as $notification)
        <div class="card border-{{ $notification->data['status'] ?? 'secondary' }} my-1">
            <div class="card-body">
                @if ($notification->data['is_html'] ?? false)
                    {!! $notification->data['message'] !!}
                @else
                    {{ $notification->data['message'] }}
                @endif
            </div>
            <div class="card-footer small text-right">
                @if ($notification->created_at->diffInDays(now()) > 0)
                    {{ $notification->created_at->format('d.m.Y в H:i') }}
                @else
                    {{ $notification->created_at->diffForHumans() }}
                @endif
            </div>
        </div>
    @endforeach
    @include('partials.pagination', ['items' => $notifications])
@endisset