@if ($paginator->hasPages())
    <ul class="pagination pagination-sm">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="page-item disabled"><span class="page-link">&laquo;</span></li>
        @elseif ($paginator->currentPage() == 2)
            <li class="page-item"><a class="page-link" href="{{ trim(preg_replace( '#([&\?])' . $paginator->getPageName() . '=1&?#', '$1', $paginator->previousPageUrl()), '?&') }}" aria-label="Предыдущая" rel="prev">&laquo;</a></li>
        @else
            <li class="page-item"><a class="page-link" href="{{ $paginator->previousPageUrl() }}" aria-label="Предыдущая" rel="prev">&laquo;</a></li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="page-item d-none d-sm-block disabled"><span class="page-link">{{ $element }}</span></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="page-item active"><span class="page-link">{{ $page }}</span></li>
                    @else
                        <li class="page-item d-none d-sm-block"><a class="page-link" href="
                            @if ($page == 1)
                                {{ trim(preg_replace( '#([&\?])' . $paginator->getPageName() . '=1&?#', '$1', $url), '?&') }}
                            @else
                                {{ $url }}
                            @endif
                            ">{{ $page }}</a>
                        </li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="page-item"><a class="page-link" href="{{ $paginator->nextPageUrl() }}" aria-label="Следующая" rel="next">&raquo;</a></li>
        @else
            <li class="page-item disabled"><span class="page-link">&raquo;</span></li>
        @endif
    </ul>
@endif
