@extends('layouts.default')

@section('content')
    <ul class="nav nav-pills mb-3" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="pill" href="#tab-order" role="tab" aria-controls="tab-order" aria-selected="true">
                Заказы
                @if (!empty($userInformers['orders']))
                    <span class="badge badge-secondary">{{ $userInformers['orders'] }}</span>
                @endif
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="pill" href="#tab-transactions" role="tab" aria-controls="tab-transactions" aria-selected="false">
                Баланс
                @if (!empty($userInformers['balance']))
                    <span class="badge badge-secondary">{{ $userInformers['balance'] }}</span>
                @endif
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="pill" href="#tab-notifications" role="tab" aria-controls="tab-notifications" aria-selected="false">
                Уведомления
                @if (!empty($userInformers['notifications']))
                    <span class="badge badge-secondary">{{ $userInformers['notifications'] }}</span>
                @endif
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="pill" href="#tab-profile" role="tab" aria-controls="tab-profile" aria-selected="false">Профиль</a>
        </li>
        <li class="nav-item ml-auto">
                <form class="nav-item" action="{{ route('logout') }}" method="POST">
                    {{ csrf_field() }}
                    <button class="btn btn-outline-secondary text-left pl-3 float-right">Выйти</button>
                </form>
        </li>
    </ul>
    
    @hasSection('dashboard-tabs')
        @yield('dashboard-tabs')
    @else
        <div class="tab-content">
            <div class="tab-pane fade show active" id="tab-order" role="tabpanel" aria-labelledby="tab-order-tab">
                @include('orders._list')
            </div>
            <div class="tab-pane fade" id="tab-profile" role="tabpanel" aria-labelledby="tab-profile-tab">
                
            </div>
            <div class="tab-pane fade" id="tab-transactions" role="tabpanel" aria-labelledby="tab-transactions-tab">
                @include('transactions._list')
            </div>
            <div class="tab-pane fade" id="tab-notifications" role="tabpanel" aria-labelledby="tab-notifications-tab">
                @include('notifications._list')
            </div>
        </div>
    @endif

@endsection