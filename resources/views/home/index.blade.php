@extends('layouts.default')

@section('content')

    <header class="intro-header">
        <div class="container">
            <div class="row intro-message">
                <div class="col-12">
                    <h1>
                        Интернет-магазин 
                        <img class="intro-divider" src="http://shapki-dart.ru/images/text_divider.svg"> 
                        <small>головных уборов и аксессуаров</small>
                    </h1>
                </div> 
                {{--<div class="col-md-5">
                    <h3 class="text-center mb-3">Заказ каталога</h3>
                    <form class="form-header" action="https://docs.google.com/forms/d/e/1FAIpQLSfSQaM9MtmvCL-3K2Qs7ytrnYZcJ-6ogVI9MENBK8RkpQr2Kg/formResponse?embedded=true" role="form" method="POST" id="#">
                        <div class="form-group">
                            <input class="form-control input-lg" name="entry.925901488" type="text" placeholder="Ваше имя" required="">
                        </div>
                        <div class="form-group">
                            <input class="form-control input-lg" name="emailAddress" type="email" placeholder="Адрес электронной почты" required="">
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-secondary btn-block btn-lg" value="Заказать">
                        </div>
                        <!-- <p class="privacy text-center">We will not share your email. Read our privacy policy.</p> -->
                    </form>
                </div>--}}
            </div>
        </div>
    </header>

    <section class="content-section-b" id="about">
        <h2 class="sr-only">О компании</h2>
        <div class="container text-center">
            <h3 class="section-heading">Наши преимущества:</h3>
            <div class="row text-center">
                <div class="col-md-4">
                    <span class="fa-stack fa-3x">
                        <i class="fas fa-circle fa-stack-2x text-primary"></i>
                        <i class="fas fa-ruble-sign fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4>Низкие цены</h4>
                </div>
                <div class="col-md-4">
                    <span class="fa-stack fa-3x">
                        <i class="fas fa-circle fa-stack-2x text-primary"></i>
                        <i class="fas fa-heart fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4>Высокое качество</h4>
                </div>
                <div class="col-md-4">
                    <span class="fa-stack fa-3x">
                        <i class="fas fa-circle fa-stack-2x text-primary"></i>
                        <i class="fas fa-list fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4>Большой ассортимент</h4>
                </div>
            </div>
        </div> <!-- /.container -->
    </section> <!-- /.content-section-b -->

    <section class="content-section-a">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 mr-auto order-lg-2">
                    <hr class="section-heading-spacer"><div class="clearfix"></div>
                    <h3 class="section-heading">Важнейший элемент женского&nbsp;образа</h3>
                    <p class="lead">Одним из важнейших элементов женского образа в любое время года является головной убор. 
                    Помимо эстетической красоты и полноты образа он несёт ещё и практическую функцию – популярные вязаные шапки защищают от зимнего холода, 
                    а широкополые летние шляпы спасают в летний зной.</p>
                </div>
                <div class="col-lg-5 ml-auto order-lg-1">
                    <img class="img-fluid" src="http://php.shapki-dart.ru/img/main-block-abouthats.png" alt="">
                </div>
            </div>
        </div> <!-- /.container -->
    </section> <!-- /.content-section-a -->

    <section class="content-section-b">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 ml-auto">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h3 class="section-heading">Модные аксессуары</h3>
                    <p class="lead">Хорошо выглядеть в новом модном сезоне помогут правильно выбранная одежда и аксессуары. Мы уделяем много внимания при выборе аксессуаров, добавляем их как элемент для завершения нашего образа.</p>
                </div>
                <div class="col-lg-5 mr-auto">
                    <img class="img-fluid" src="https://pp.userapi.com/c837636/v837636217/5cc2b/0DbAEk5Rkso.jpg" alt="">
                </div>
            </div>
        </div> <!-- /.container -->
    </section> <!-- /.content-section-b -->

    <aside class="banner" id="contacts">
        <div class="container">
            <div class="text-right">
                @if (Auth::check())
                    <h2>Директор ООО "Дарт" <br>
                    Волкова Ольга Дмитриевна <br>
                    +7 (913) 784-80-07</h2>
                @endif
            </div>
        </div> <!-- /.container -->
    </aside> <!-- /.banner -->

@endsection