<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="theme-color" content="#f8f9fa">
<meta property="og:title" content="{{ config('app.name') }}">
<meta property="og:image" content="{{ asset('images/logo.png') }}">
<meta property="og:url" content="{{ config('app.url') }}">
<meta property="og:type" content="website">
<meta property="og:site_name" content="{{ config('app.name') }}">
<meta property="og:locale" content="{{ app()->getLocale() }}">
<title>@yield('title', 'Головные уборы Новосибирск') - {{ config('app.name') }}</title>
<!-- Bootstrap CSS -->
<link rel="icon" href="{{ asset('favicon.ico') }}">
<link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
<!-- User's CSS -->
<link href="{{ asset('css/default.css') }}?v=201811073" rel="stylesheet">