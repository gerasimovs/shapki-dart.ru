@if ($items->total() > min($items->perPage(), 12))

<form class="form-row justify-content-end" action="{{ Request::url() }}" method="GET">
    <div class="col-auto">
        {{ $items
            ->appends(array_merge(['count' => Request::input('count'), 'text' => Request::input('text')], $appends ?? []))
            ->links() 
        }}
    </div>
    <div class="col-auto">
        @if (Request::has('text')) 
            <input type="hidden" name="text" value="{{ Request::input('text') }}">
        @endif
            @foreach ($appends ?? [] as $param => $value)
                @continue(empty($value))
                <input type="hidden" name="{{ $param }}" value="{{ $value }}">
            @endforeach
        <select class="form-control mb-3" name="count" onchange="this.form.submit()">
                <option value="12">12</option>
            @for ($i = 24; $i < $items->total(); $i = $i * 2)
                <option value="{{ $i }}"@if ($i == $items->perPage()) selected @endif >{{ $i }}</option>
            @endfor
                <option value="{{ $items->total() }}"@if ($items->perPage() == $items->total()) selected @endif >Все</option>
        </select>
    </div>
</form>

@endif