<form class="input-group input-group-sm" method="GET" action="{{ route('products.search') }}">
    <input type="text" name="text" class="form-control" placeholder="Искать товары..." aria-label="Search for..." value="{{ Request::get('text') }}">
    <span class="input-group-append">
        <button class="btn btn-outline-primary" type="submit">Найти!</button>
    </span>
</form>