<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="navbar-main">
    <div class="container">
        <a class="navbar-brand" href="/">{{ config('app.name', 'Laravel') }}</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('home') }}">О компании</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('catalog') }}">Коллекции</a>
                </li> 
                <li class="nav-item">
                    <a class="nav-link text-danger" href="{{ route('pages.show', 2) }}">Бонусная программа</a>
                </li> 
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="helpMenu" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Помощь
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="helpMenu" style="width: 250px;">
                        @foreach (\App\Page::where('is_published', true)->get() as $page)
                            <a class="dropdown-item" href="{{ route('pages.show', $page) }}" style="white-space: normal;">{{ $page->name }}</a>
                        @endforeach
                    </div>
                </li>
                <!-- <li class="nav-item">
                    <a class="nav-link" href="{{ route('contacts') }}#contacts">Контакты</a>
                </li> -->
                @if (Route::has('login'))
                    @auth
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('user.dashboard') }}">Личный кабинет</a>
                        </li>
                        @if (Auth::check()) 
                        <li class="nav-item" @if (!Auth::user()->cart || (Auth::user()->cart && Auth::user()->cart->offers->count()) == 0) style="display: none;" @endif>
                            <a class="btn btn-outline-secondary cart-button" href="{{ route('cart.index') }}">
                                <i class="material-icons">&#xE8CC;</i>  
                                <span class="badge badge-secondary">@if (Auth::user()->cart && Auth::user()->cart->offers->count())
                                    {{ Auth::user()->cart->total_qty }}
                                @endif</span>
                                    
                            </a>
                        </li>
                        @endif
                    @else
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">Вход</a>
                        </li>
                    @endauth
                @endif
            </ul>
        </div>
    </div>
</nav>

@if (Auth::check() && (Auth::user()->id == 1 || Auth::user()->id == 2))
    @include('admin.partials.admin-menu')
@endif