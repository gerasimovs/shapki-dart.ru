@if (session('status'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        @if (is_array(session('status')))
            @foreach (session('status') as $status)
                {{ $status }}
            @endforeach
        @else
            {{ session('status') }}
        @endif
    </div>
@endif
