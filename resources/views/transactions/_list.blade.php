@if (isset($transactions) && $transactions->count() > 0)
    <div class="table-responsive">
        <table class="table bg-light">
            <thead class="thead-light">
                <tr>
                    <th>Дата</th>
                    <th>Сумма</th>
                    <th>Статус</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($transactions as $transaction)
                    <tr>
                        <td>@datetime($transaction->created_at)</td>
                        <td class="text-{{ $transaction->amount > 0 ? 'success' : 'danger'}}">
                            {{ abs($transaction->amount) }}
                        </td>
                        <td>
                            {{ $transaction->status }}
                            <div class="small">{{ $transaction->description }}</div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

@else
    <p class="big">Список операций пуст!</p>
@endif
