<style type="text/css">
    table.table td {
        width: 50%;
    },
    a {
        text-decoration: none;
    }
</style>

{{-- Greeting --}}
<h3> {{ $greeting ?? 'Добрый день!'}} </h3>

<?php
    $count = preg_match_all('#\[\[(.*?)\]\]#', $text, $snippets);

    if ($count) {
        $result = array();
        foreach ($snippets[1] as $key => $value) {
            if (strpos($value, ':') > -1) {
                $params = explode(':', $value);
                if ($params[0] == 'Product') {
                    $product = \App\Product::withTrashed()
                        ->with('images:url')
                        ->select('id', 'vendor_code')
                        ->find($params[1]);

                    if ($product) {
                        $matchKey = $snippets[0][$key];
                        $imageUrl = storage_path('app') . (parse_url($product->images()->value('url')))['path'];
                        $imageData = Image::make($imageUrl)
                            ->resize(300, null, function($constraint) {
                                $constraint->aspectRatio();
                            })->save(dirname(dirname($imageUrl) . '/300/' . basename($imageUrl)));
                        $imageCid = $message->embedData($imageData, basename($imageUrl));
                        $result[$matchKey] = PHP_EOL . '<a href="' . route('products.show', $product->id) . '">'. PHP_EOL . '<img src="' . $imageCid . '">' . PHP_EOL . '</a>' . PHP_EOL;
                    }
                }
            }
        }
    }

    $expression = strtr($text, $result);
?>

{{-- Text --}}
@if (!empty($text))
<?= $expression ?>
@endif

{{-- Salutation --}}
@if (!empty($salutation))
{!! $salutation !!}
@else
С уважением,<br>{{ config('app.name') }}
@endif
