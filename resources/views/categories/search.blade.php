@extends('layouts.default')

@section('content')

    <div class="row">
        <div class="col-lg-3">@include('categories.partials.list', ['parent' => 0])</div>
        <div class="col-lg-9">
            <div class="mb-3">
                @include('partials.search')
            </div>

            @if ($products->count())
                @include('partials.pagination', ['items' => $products])
                <div class="row">
                    @foreach ($products as $product)
                        @include('products.partials.products_card', compact('product'))
                    @endforeach
                </div>
                @include('partials.pagination', ['items' => $products])
            @else
                <div class="card bg-secondary mb-3">
                    <div class="card-body">
                        <p class="text-white text-center m-0">Товары отсутствуют!</p>
                    </div>
                </div>
            @endif

        </div><!-- /col-lg-9 -->
    </div><!-- /.row -->

@endsection