<?php
/**
 * @var \App\ServiceCategory $category
 */
?>
<div class="card has-equal-height">
    <div class="card-header">
        <div class="card-header-title">
            {{ $category->title }}
        </div>
        @if ($category->information)
            <span is="b-tooltip" label="{{ $category->information }}" :delay="100" multilined>
                <a class="card-header-icon">
                    <span is="b-icon" icon="graduation-cap"></span>
                </a>
            </span>
        @endif
    </div>
    <div class="card-image">
        <img src="{{ $category->present()->preview_path ?? '/storage/stub/16-9.svg' }}" alt="">
    </div>
    <div class="card-body">
        {{ $category->description }}
    </div>
</div>
