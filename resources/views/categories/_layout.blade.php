@extends('layouts.default')

@section('content')
    <div class="row">
        <div class="col-lg-3 mb-3">
            @include('categories.partials.list', ['parent' => 0])
        </div>
        <div class="col-lg-9">
            <div class="mb-3">
                @include('partials.search')
            </div>
            @yield('sub_content')
        </div>
    </div>
@endsection