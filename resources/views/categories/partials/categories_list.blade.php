@if (count($categories))

<div class="list-group" id="categories-list">
    @if (Auth::user()->id == 1 || Auth::user()->id == 2)
    <a class="list-group-item text-danger" href="{{ route('catalog') }}">
        <i class="material-icons">&#xE031;</i> Новые поступления
    </a>
    @endif
    @foreach ($categories as $category)
        <a class="list-group-item text-{{ $category->status }} @if ($category->children->count()) font-weight-bold @endif"
            @if ($category->children->count())
                data-toggle="collapse" 
                href="#sub-cat-{{ $category->id }}" 
                role="button" 
                aria-expanded="false" 
                aria-controls="sub-cat-{{ $category->id }}" 
            @else
                href="{{ route('catalog.show', $category->id) }}"
            @endif>
            {{ $category->name }}
        </a>

        @if ($category->children->count())
                @include('categories.partials.children', ['categories' => $category->children, 'parent' => $category->id, 'status' => $category->status, 'prefix' => '-'])
        @endif
    @endforeach
</div>

@else

    <div class="card bg-secondary mb-3">
        <div class="card-body">
            <p class="text-white text-center m-0">Доступ ограничен!</p>
        </div>
    </div>

@endif