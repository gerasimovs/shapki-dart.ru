<div class="collapse bg-light" id="sub-cat-{{ $parent }}" style="height: 0px;">
    @foreach ($categories[$parent] as $category)
        <a class="nav-link text-{{ $category->status ?? 'primary' }} @if (isset($categories[$category->id])) font-weight-bold @endif" href="{{ route('catalog.show', $category->id) }}"
            @if (isset($categories[$category->id])) data-toggle="collapse" data-target="#sub-cat-{{ $category->id }}" aria-expanded="true" aria-controls="sub-cat-{{ $category->id }}" @endif>
            {{ $prefix ?? '-' }} {{ $category->name }}
        </a>
        @if (isset($categories[$category->id]))
            @include('categories.partials.children', ['categories' => $categories, 'parent' => $category->id, 'prefix' => $prefix.'-'])
        @endif
    @endforeach
</div>