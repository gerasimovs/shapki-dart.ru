@if ($categories)

    <div class="list-group" id="categories-list">
        <a class="list-group-item text-danger" href="{{ route('catalog') }}">
            <i class="material-icons">&#xE031;</i> Новые поступления
        </a>
        @foreach ($categories[''] as $category)
            <a class="list-group-item text-{{ $category->status }} @if (isset($categories[$category->id])) font-weight-bold @endif"
                @if (isset($categories[$category->id]))
                    data-toggle="collapse"
                    href="#sub-cat-{{ $category->id }}"
                    role="button"
                    aria-expanded="false"
                    aria-controls="sub-cat-{{ $category->id }}"
                @else
                    href="{{ route('catalog.show', $category->id) }}"
                @endif>
                {{ $category->name }}
            </a>
            @if (isset($categories[$category->id]))
                @include('categories.partials.children', ['categories' => $categories, 'parent' => $category->id, 'prefix' => '-'])
            @endif
        @endforeach
    </div>

@else

    <div class="card bg-secondary mb-3">
        <div class="card-body">
            <p class="text-white text-center m-0">Доступ ограничен!</p>
        </div>
    </div>

@endif