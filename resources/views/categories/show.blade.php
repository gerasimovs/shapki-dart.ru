@extends('categories._layout')

@section('title', $category->name)

@section('sub_content')

    @if ($category->description)
        <div class="alert alert-success mb-3">
            <p class="text-center font-weight-bold h5 text-danger">{{ $category->description }}</p>
        </div>
    @endif

    @if (isset($categories) && $categories->count())
        @foreach ($categories as $subCategory)
            <div>
                <a href="{{ route('catalog.show', $subCategory->id) }}">{{ $subCategory->name }}</a>
            </div>
        @endforeach
    @endif

    @if (isset($products) && $products->count())
        @include('partials.pagination', ['items' => $products])
        <div class="row">
            @foreach ($products as $product)
                @include('products.partials.products_card', compact('product'))
            @endforeach
        </div>
        @include('partials.pagination', ['items' => $products])
    @else
        <div class="card bg-secondary mb-3">
            <div class="card-body">
                <p class="text-white text-center m-0">Товары отсутствуют!</p>
            </div>
        </div>
    @endif

@endsection