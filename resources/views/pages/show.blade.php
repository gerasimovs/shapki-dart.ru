@extends('layouts.default')

@section('content')
<section id="page">
    <h1>{{ $page->name}}</h1>

    @component('components.markdown')
        {!! $page->content !!}
    @endcomponent
</section>
@endsection