@extends('layouts.default')

@section('content')
<section class="section section-feedback">
    <div class="section-content">

        <form method="POST" action="/feedback" class="feedback-form form">
            {{ csrf_field() }}

            <h2 class="section-header">Обратная связь</h2>

            <div class="form-group">
                {{--{{dd($errors)}}--}}
                <input type="text" name="name" class="form-control feedback-input" value="{{old('name')}}" placeholder="Имя Фамилия" required maxlength="255" minlength="2" data-rule-regNameSurName="true"/>
                @if ($errors->has('name'))
                    <label class="error">*{{ $errors->first('name') }}</label>
                @endif
            </div>

            <div class="form-group">
                <input type="tel" name="phone" class="form-control feedback-input user-phone" value="{{old('phone')}}" placeholder="Телефон" required maxlength="255"/>
                @if ($errors->has('phone'))
                    <label class="error">*{{ $errors->first('phone') }}</label>
                @endif
            </div>

            <div class="form-group">
                <input type="email" name="email"  class="form-control feedback-input" value="{{old('email')}}" placeholder="E-mail" required data-rule-regEmail="true" maxlength="255"/>
                @if ($errors->has('email'))
                    <label class="error">*{{ $errors->first('email') }}</label>
                @endif

                @if ($errors->has('errorEmail'))
                    <label class="error">*{{ $errors->first('errorEmail') }}</label>
                @endif
            </div>

            <div class="form-group">
                <textarea name="question" class="form-control feedback-input feedback-input" rows="3" required value="{{old('question')}}" placeholder="Ваш вопрос"></textarea>
                @if ($errors->has('question'))
                    <label class="error">*{{ $errors->first('question') }}</label>
                @endif
            </div>

            <div class="form-group checkbox-wrap checkbox-wrap-feedback">
                <div class="checkbox">
                    <label class="label">
                        <input type="checkbox" name="personalDateAgreeFeedback" value="1" class="form-group__input" required/>
                        <p>Подтверждаю согласие на обработку <br>моих персональных данных в соответствии с <br><a href="https://www.unilever.ru/Images/8infopersonaldata_tcm1315-485017_ru.pdf" target="_blank" class="custom-link  mar-l">Политикой о персональных данных</a></p>
                    </label>
                </div>

                @if ($errors->has('personalDateAgreeFeedback'))
                    <label id="personalDateAgree-error-feedback" class="error">*{{ $errors->first('personalDateAgreeFeedback') }}</label>
                @endif

                <div class="checkbox">
                    <label class="label">
                        <input type="checkbox" name="posterDateAgree" value="1" class="form-group__input" required/>
                        <div class="poster">
                            <p>Подтверждаю согласие на получение<br> рекламной информации ООО «Юнилевер Русь»</p>
                            <div class="descr">
                                <p>Выражаю свое полное согласие на использование моих контактных данных БРЭНДТОН (ДИВИЖН) ЛИМИТЭД, осуществляющего непосредственную деятельность в соответствии с требованием законодательства РФ, и действующим по заданию Общества с ограниченной ответственностью «Юнилевер Русь» .ОГРН: 1027739039240; ИНН: 7705183476. Местонахождение: Российская Федерация, 123022, г. Москва, ул. Сергея Макеева, д. 13. является компания БРЭНДТОН (ДИВИЖН) ЛИМИТЭД, частная кампания с ограниченной ответственностью, учрежденная и осуществляющая свою деятельность в соответствии с законодательством республики Ирландия, действующая через свой филиал - Филиал частной компании с ограниченной ответственностью БРЭНДТОН (ДИВИЖН) ЛИМИТЭД (Ирландия). Местонахождение: Российская Федерация, 123022, г. Москва, ул.2-я Звенигородская, д. 13, корпус 41. ИНН 9909382667, для поддержания связи со мной, осуществления телефонных звонков на указанный стационарный и/или мобильный телефон, отправки sms-сообщений на указанный мобильный телефон, и электронных писем на указанный адрес электронной почты, в том числе рекламного характера с целью предложения услуг, проведения опросов, анкетирования, рекламных и маркетинговых исследований в отношении услуг, предоставляемых как в отдельности, так и совместно.</p>
                            </div>
                        </div>

                    </label>
                </div>
                @if ($errors->has('posterDateAgree'))
                    <label class="error">*{{ $errors->first('posterDateAgree') }}</label>
                @endif

            </div>

            <button type="submit" class="lotary-reg-link next-link">
                <span data-loading-text="Отправка..." class="text-send"></span>
            </button>
        </form>
    </div>
</section>
@endsection