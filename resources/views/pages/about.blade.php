@extends('layouts.default')

@section('content')
    <header class="intro-header">
        <div class="container">
            <div class="row intro-message" style="background: linear-gradient(to top, #f8fafc, #6cb2eb, #f8fafc);">
                <div class="col-12">
                    <h1>
                        Интернет-магазин 
                        <img class="intro-divider" src="http://shapki-dart.ru/images/text_divider.svg"> 
                        <small>головных уборов и аксессуаров</small>
                    </h1>
                </div> 
            </div>
        </div>
    </header>

    <p class="h3">Компания ООО "Дарт" - это 9 лет успешной работы и сотни довольных клиентов.</p>
    <h4>Каждый новый сезон:</h4>
    <ul>
        <li>Мы подбираем для Вас - пожалуй, самое лучшее, что можно выбрать и предложить</li>
        <li>Мы идем в ногу с мировыми тенденциями</li>
        <li>Наш ассортимент направлен на разного покупателя: от эконом до премиум классов и на разные возрастные группы</li>
    </ul>

    <h4>Что Вы получаете, сотрудничая с нами</h4>
    <ul>
        <li>БОЛЬШОЙ ВЫБОР ТОВАРА: каждый сезон около 10 000 новых моделей</li>
        <li>ПОСТОЯННОЕ ОБНОВЛЕНИЕ АССОРТИМЕНТА</li>
        <li>ШИРОКИЙ АССОРТИМЕНТ: у нас Вы всегда найдете полноценные и грамотно сформированные коллекции, рассчитанные на покупателей с разнообразными запросами и ценовыми предпочтениями</li>
        <li>ЭКОНОМИЯ ВРЕМЕНИ И ДЕНЕГ клиентов — самые популярные хиты сезона в одном месте</li>
        <li>ДОСТАВКА любой транспортной компанией</li>
        <li>Доставка до терминала транспортной компании БЕСПЛАТНОЙ</li>
        <li>ЗАКЛЮЧЕНИЕ ДОГОВОРА для продолжительного сотрудничества</li>
        <li>КАЧЕСТВО ОБСЛУЖИВАНИЯ: десятки тысяч довольных покупателей доверяют нам и из года в год приходят к нам за товаром</li>
    </ul>

    <p>Мы благодарны Вам за это, приходите к нам в новом сезоне – будет интересно!</p>



    <section class="content-section-b" id="about">
        <h2 class="sr-only">О компании</h2>
        <div class="container text-center">
            <blockquote class="blockquote text-right text-primary">
                <p class="mb-0">По-моему, женщину в шляпке забыть нельзя</p>
                <footer class="blockquote-footer"><cite title="Софи Лорен">Софи Лорен</cite></footer>
            </blockquote>
            <h3 class="section-heading">Наши преимущества:</h3>
            <div class="row text-center">
                <div class="col-md-4">
                    <span class="fa-stack fa-3x">
                        <i class="fas fa-circle fa-stack-2x text-primary"></i>
                        <i class="fas fa-ruble-sign fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4>Низкие цены</h4>
                </div>
                <div class="col-md-4">
                    <span class="fa-stack fa-3x">
                        <i class="fas fa-circle fa-stack-2x text-primary"></i>
                        <i class="fas fa-heart fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4>Высокое качество</h4>
                </div>
                <div class="col-md-4">
                    <span class="fa-stack fa-3x">
                        <i class="fas fa-circle fa-stack-2x text-primary"></i>
                        <i class="fas fa-list fa-stack-1x fa-inverse"></i>
                    </span>
                    <h4>Большой ассортимент</h4>
                </div>
            </div>
        </div> <!-- /.container -->
    </section> <!-- /.content-section-b -->

    <section class="content-section-a">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 mr-auto order-lg-2">
                    <hr class="section-heading-spacer"><div class="clearfix"></div>
                    <h3 class="section-heading">Важнейший элемент женского&nbsp;образа</h3>
                    <p class="lead">Одним из важнейших элементов женского образа в любое время года является головной убор. 
                    Помимо эстетической красоты и полноты образа он несёт ещё и практическую функцию – популярные вязаные шапки защищают от зимнего холода, 
                    а широкополые летние шляпы спасают в летний зной.</p>
                </div>
                <div class="col-lg-5 ml-auto order-lg-1">
                    <img class="img-fluid" src="https://shapki-dart.storage.yandexcloud.net/dd/ec/85/85/fb/07/96/f7/59/45/c5/15/2b/7c/3c/7d/d0/91/22/01.jpeg" alt="">
                </div>
            </div>
        </div> <!-- /.container -->
    </section> <!-- /.content-section-a -->

    <section class="content-section-b">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 ml-auto">
                    <hr class="section-heading-spacer">
                    <div class="clearfix"></div>
                    <h3 class="section-heading">Модные аксессуары</h3>
                    <p class="lead">Хорошо выглядеть в новом модном сезоне помогут правильно выбранная одежда и аксессуары. Мы уделяем много внимания при выборе аксессуаров, добавляем их как элемент для завершения нашего образа.</p>
                </div>
                <div class="col-lg-5 mr-auto">
                    <img class="img-fluid" src="https://pp.userapi.com/c837636/v837636217/5cc2b/0DbAEk5Rkso.jpg" alt="">
                </div>
            </div>
        </div> <!-- /.container -->
    </section> <!-- /.content-section-b -->

    <aside class="banner" id="contacts" style="background: #e9ecef;">
        <div class="container">
            <div class="text-right">
                @if (Auth::check())
                    <h2>Директор ООО "Дарт" <br>
                    Волкова Ольга Дмитриевна <br>
                    +7 (913) 784-80-07</h2>
                @endif
            </div>
        </div> <!-- /.container -->
    </aside> <!-- /.banner -->

@endsection