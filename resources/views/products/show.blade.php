@extends('layouts.default')
@section('title', $product->vendor_code)

@section('content')
    <nav class="navigation__block" aria-label="breadcrumb" role="navigation">
        <ul class="navigation-breadcrumb__block">
            <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
            <li class="breadcrumb-item"><a href="{{ route('catalog') }}">Каталог</a></li>
            <li class="breadcrumb-item"><a href="{{ route('catalog.show', $product->category_id) }}">{{$product->category->name}}</a></li>
            <li class="breadcrumb-item active" aria-current="page">{{ $product->vendor_code }}</li>
        </ul>
        <div class="navigation-search__block">
            @include('partials.search')
        </div>
    </nav>
    <section id="product" itemscope itemtype="https://schema.org/Product">
        @if ($product->availability == 'pre_order')
            <div class="bar-diagonal__wrapper">
                <div class="bar-diagonal__block">@lang('product.availability.' . $product->availability)</div>
            </div>
        @endif
        @if ($product->is_new)<span class="badge badge-pill badge-danger float-right mt-1 mr-1">Новинка</span>@endif
        <h1 itemprop="name">{{ $product->vendor_code }}</h1>
        <div class="row">
            <div class="col-sm-6">
                <div id="carousel-{{$product->id}}" class="carousel slide" data-ride="carousel" data-interval="2500">
                    <div class="carousel-inner">
                        @foreach ($product->images as $image)
                            <div class="carousel-item @if ($loop->first) active  @endif">
                                <img class="d-block w-100" src="{{ $image->url }}" itemprop="image" alt="{{ $product->name }}">

                                @if ($image->pivot->description)
                                    <div class="carousel-caption d-none d-md-block">
                                    <p>{{ $image->pivot->description }}</p>
                                    </div>
                                @endif
                            </div>
                        @endforeach
                        @if ( $product->images->count() > 1 )
                            <a class="carousel-control-prev" href="#carousel-{{ $product->id }}" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carousel-{{ $product->id }}" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        @endif
                    </div>
                    @if ( $product->images->count() > 1 )
                        <div class="carousel-indicators-static carousel-indicators">
                            @foreach ($product->images as $image)
                                <div data-target="#carousel-{{ $product->id }}" data-slide-to="{{ $loop->iteration - 1 }}" class="carousel-thumbs @if ($loop->first) active @endif ">
                                    <img src="{{ $image->url }}" class="w-100">
                                </div>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
            <div class="col-sm-6">
                <p class="card-header">Описание</p>
                <div class="card-body">
                    <p class="card-text" itemprop="description">
                        {{ $product->description }}
                    </p>
                </div>

                @if ($product->options->count())
                    <p class="card-header">Размер, цвет</p>
                    <div class="row" id="product-options">
                        @foreach ($product->options->chunk(ceil($product->options->count()/2)) as $options)
                            <ul class="col-sm-6 my-3">
                                @foreach ($options as $option)
                                    <li class="nav-link position-relative">
                                        @include('products.partials.add_to_cart', ['offer' => $option->offer, 'option' => $option])
                                    </li>
                                @endforeach
                            </ul>
                        @endforeach
                    </div>
                    <div class="card-header">
                        @lang('product.price.per_' . $product->unit)
                    </div>
                    <div class="card-body" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                        <p class="card-text h5">@money($product->price) руб.</p>
                        <meta itemprop="price" content="{{ intval($product->price * 1.7) }}">
                        <meta itemprop="priceCurrency" content="RUB">
                        <meta itemprop="priceValidUntil" content="{{ date('Y') }}-12-31T23:59:59">
                        <link itemprop="url" href="{{ $product->present()->route }}">
                        <link itemprop="availability" href="http://schema.org/InStock">
                    </div>
                @endif
            </div>
        </div>
        <meta itemprop="productID" content="{{ $product->id }}">
        <meta itemprop="sku" content="{{ $product->present()->sku }}">
    </section>
    @if ($product->related->count())
    <div class="card bg-light mt-5">
        <h5 class="card-header">Вместе с этим товаром покупают</h5>
        <div class="card-body row justify-content-center">
            @foreach ($product->related as $related)
                @include('products.partials.products_card', ['product' => $related])
            @endforeach
        </div>
    </div>
    @endif
@endsection