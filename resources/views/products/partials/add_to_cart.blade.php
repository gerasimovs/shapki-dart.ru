<form class="form-row flex-nowrap add-to-cart" data-offer="{{ $offer->id }}" action="{{ route('cart.add', [$offer]) }}" method="GET">
    <input type="hidden" name="offer" value="{{ $offer->id }}">
    <div class="col-auto" style="width: 75px;">
        <input type="number" class="form-control" name="count" value="1">
    </div>
    <div class="col-auto">
        <button class="btn btn-light btn-block text-left">
            <span class="fa-layers fa-fw">
                <i class="fas fa-sm fa-shopping-cart"></i> 
                {{-- <span class="fa-layers-counter fa-lg" style="background:Tomato">1</span> --}}
            </span>
            {{ ($option->name == '-' || $option->name == '') ? '' : $option->name }}
        </button>
    </div>
    {{ csrf_field() }} 
</form>