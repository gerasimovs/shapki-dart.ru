<div class="col-md-4 col-sm-6 mb-3 product-card">
    <div class="card h-100">
        <a href="{{ route('products.show', $product->id) }}" class="position-relative">
            @if ($product->availability == 'pre_order')
                <div class="bar-diagonal__wrapper">
                    <div class="bar-diagonal__block">@lang('product.availability.' . $product->availability)</div>
                </div>
            @endif
            @if ($product->is_new)<span class="badge badge-pill badge-danger float-right mt-1 mr-1">Новинка</span>@endif
        <div class="img-product" 
            style="background-image: url('@if ($product->images->count()){{ $product->images[0]->url }}@else{{ asset('images/no_photo.png') }}@endif'); transition: background .2s ease-in-out;" 
            @if ($product->images && $product->images->count() > 1)
                onmouseover="$(this).delay(500).css('background-image', 'url({{ $product->images[1]->url }})');"
                onmouseout="$(this).delay(500).css('background-image', 'url({{ $product->images[0]->url }})');"
            @endif
            ></div></a>
        <div class="card-body">
            <h4 class="card-title">
                <a href="{{ route('products.show', $product->id) }}">{{ $product->vendor_code }}</a>
            </h4>
            @if ($product->options_count == 1)
                <div class="py-2 position-relative">
                    @include('products.partials.add_to_cart', ['offer' => $product->options->first()->offer, 'option' => $product->options->first()])
                </div>
            @endif
            <div class="row">
                <div class="col-5 small text-right pr-0">
                    @lang('product.price.per_' . $product->unit)
                </div>
                <div class="col-7">
                    <span class="h3">@money($product->price)</span> руб.
                </div>
            </div>
        </div>
    </div>
</div>