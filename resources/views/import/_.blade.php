@extends('layouts.default')
@section('title', 'Импорт файлов')

@section('content')
    <div class="card">
        <div class="card-header">
            <ul class="nav nav-tabs card-header-tabs">
                <li class="nav-item">
                    <a href="{{route('admin.import.index')}}"
                        class="nav-link{{Request::route()->getName() == 'admin.import.index' ? ' active' : ''}}">
                        Загрузка файла .csv
                    </a>
                </li>
                <li class="nav-item">
                    <span class="nav-link{{Request::route()->getName() == 'admin.import.parse' ? ' active' : ''}}">
                        Сопоставление полей
                    </span>
                </li>
                <li class="nav-item">
                    <span class="nav-link{{Request::route()->getName() == 'admin.import.result' ? ' active' : ''}}">
                        Завершение импорта
                    </span>
                </li>
            </ul>
        </div>
        <div class="card-body">
            @yield('sub_content')
        </div>
    </div>
@endsection