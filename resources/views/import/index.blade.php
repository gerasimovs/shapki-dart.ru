@extends('import._')
@section('title', 'Загрузка файла .csv')

@section('sub_content')
    <form method="POST" action="{{ route('admin.import.parse') }}" enctype="multipart/form-data">
        {{ csrf_field() }}
        @if ($errors->has('file'))
            <div class="alert alert-warning">
                {{ $errors->first('file') }}
            </div>
        @endif
        <div class="form-group row">
            <label class="col-sm-4 col-form-label" for="file">Выберите файл .csv:</label>
            <div class="col-sm-8">
                <input class="form-control-file" type="file" name="file" id="file" accept=".csv" required>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-4 col-form-label" for="fields">Импортировать:</label>
            <div class="col-sm-8">
                <select name="fields" id="fields" class="form-control">
                    <option value="products">Товары</option>
                    <option value="order">Заказ</option>
                </select>
            </div>
        </div>

        <div class="form-group">
            <div class="form-check">
                <label class="form-check-label">
                    <input class="form-check-input" type="checkbox" name="header" checked>
                    Файл содержит строку с заголовками?
                </label>
            </div>
        </div>
        <div class="form-group">
            <div class="form-check">
                <label class="form-check-label">
                    <input class="form-check-input" type="checkbox" name="sorting" checked>
                    Сначала идут новые товары?
                </label>
            </div>
        </div>
        <div class="form-group row">
            <label for="delimiter" class="col-sm-4 col-form-label">Разделитель поля <small class="d-block">(только один символ)</small></label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="delimiter" id="delimiter" placeholder="delimiter" value=";">
            </div>
        </div>
        <div class="form-group row">
            <label for="enclosure" class="col-sm-4 col-form-label">Ограничителя поля <small class="d-block">(только один символ)</small></label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="enclosure" id="enclosure" placeholder="enclosure" value="&quot;">
            </div>
        </div>
        <div class="form-group row">
            <label for="escape" class="col-sm-4 col-form-label">Экранирующий символ <small class="d-block">(только один символ)</small></label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="escape" id="escape" placeholder="escape" value="\">
            </div>
        </div>
        <button type="submit" class="btn btn-block btn-primary">Импортировать файл</button>
    </form>
@endsection