@extends('import._')

@section('title', 'Сопоставление полей')

@section('sub_content')
    <form method="POST" action="{{ route('admin.import.result') }}">
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <input type="hidden" name="file" value="{{ $file }}">
        <input type="hidden" name="imported" value="{{ $imported }}">

        <fieldset class="form-group">
            <legend>Обновление данных:</legend>
            <div class="form-check">
                <label class="form-check-label">
                    <input class="form-check-input" type="checkbox" name="checkDescription"> Описание / цена
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input class="form-check-input" type="checkbox" name="checkImages"> Изображения
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input class="form-check-input" type="checkbox" name="checkOptions"> Размеры / цвета
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input class="form-check-input" type="checkbox" name="checkSuppliers"> Поставщиков
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input class="form-check-input" type="checkbox" name="checkDelete"> Удаление старых товаров
                </label>
            </div>
            <div class="form-check">
                <label class="form-check-label">
                    <input class="form-check-input" type="checkbox" name="checkRename"> Переименование моделей
                </label>
            </div>
        </fieldset>

        <h4>Сопоставление полей:</h4>
        @for ($i = 0; $i < count($header); $i++)
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="item-{{ $i }}">{{ $header[$i] }}</label>
                <div class="col-sm-9">
                    <select name="fields[]" id="item-{{ $i }}" class="form-control">
                        <option value="null">-- Выберите поле --</option>
                        @foreach ($fields as $name => $option)
                            <option value="{{ $name }}"
                                @if (!empty($header[$i]) && strpos($option['synonyms'], mb_strtolower($header[$i])) !== false)
                                    selected 
                                @endif
                            >{{ $option['name'] }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        @endfor
        <button type="submit" class="btn btn-primary">Parse file .csv</button>
    </form>
@endsection