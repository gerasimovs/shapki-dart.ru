@extends('layouts.default')

@section('content')
    <h2 class="my-4">Ваш заказ</h2>
    @if (count($offersGroup))
        <div class="table-responsive">
            <table class="table bg-light">
                <thead class="thead-light">
                    <tr>
                        <th colspan="2">Товар</th>
                        <th class="cart-qty__title">Кол-во</th>
                        <th class="cart-price__title"><abbr title="Указана оптовая цена">Цена</abbr> <sup>руб.</sup></th>
                        <th class="cart-price__title">Итого <sup>руб.</sup></th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($offersGroup as $offers)
                        @foreach ($offers as $offer)
                        <tr>
                            @if ($loop->first)
                                <td rowspan="{{ count($offers) + 1 }}">
                                    <div class="cart-thumbs" style="background-image: url('{{ $offer->product->images->first()->url }}');"></div>
                                    <a href="{{ route('products.show', $offer->product->id) }}">
                                        {{ $offer->product->vendor_code }}
                                    </a>
                                    @if ($offer->product->trashed()) <p class="small text-danger">Товар закончился</p> @endif
                                </td>
                            @endif
                            <td>{{ $offer->option->name }}</td>
                            <td>
                                <form class="input-group input-group-sm">
                                    @if (!$offer->product->trashed())
                                        <span class="inputut-group-prepend">
                                            <a href="{{ route('cart.remove', $offer->id) }}" class="btn btn-outline-secondary btn-sm"> - </a>
                                        </span>
                                    @endif
                                    <input type="text" class="form-control text-center" value="{{ $offer->pivot->count }}" readonly>
                                    @if (!$offer->product->trashed())
                                    <span class="input-group-append">
                                        <a href="{{ route('cart.add', $offer->id) }}" class="btn btn-outline-secondary btn-sm"> + </a>
                                    </span>
                                    @endif
                                </form>
                            </td>
                            <td class="text-right">@money($offer->product->price)</td>
                            <td class="text-right">@money($offer->product->price * $offer->pivot->count)</td>
                            <td><a href="{{ route('cart.delete', $offer->id) }}" class="btn btn-light btn-sm"> &times; </a></td>
                        </tr>
                        @endforeach
                        <tr>
                            <td class="font-italic">Всего по позиции:</td>
                            <td>
                                <input type="text" class="form-control text-center" value="{{ $offers->sum('pivot.count') }}" readonly>
                            </td>
                            <td>&nbsp;</td>
                            <td class="font-italic text-right">@money($offers->sum('pivot.count') * $offer->product->price)</td>
                            <td>&nbsp;</td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="2">Всего:</td>
                        <td>
                            <input type="text" class="form-control form-control-sm text-center" value="{{ $totalQty }}" readonly>
                        </td>
                        <td>&nbsp;</td>
                        <td class="text-right">@money($totalPrice)</td>
                        <td>&nbsp;</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <form action="{{ route('cart.order') }}" method="POST">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="input-comment">Комментарий к заказу:</label>
                <textarea class="form-control" name="comment" id="input-comment" rows="3" placeholder="Вы можете указать свои комментарии к заказу в данном поле..."></textarea>
            </div>
            <div class="d-flex justify-content-between">
                <a href="{{ route('cart.clear') }}" class="btn btn-secondary"><i class="material-icons">&#xE14C;</i> Очистить корзину</a>
                <button type="submit" class="btn btn-success"><i class="material-icons">&#xE5CA;</i> Заказать</button>
            </div>
        </form>
    @else
        <p>Вы еще не выбрали ни одного товара!</p>
        <p>Ознакомьтесь с <a href="{{ route('catalog') }}">нашим ассортиментом</a>. Вы, несомненно, найдете для себя что-то подходящее.</p>

    @endif

@endsection