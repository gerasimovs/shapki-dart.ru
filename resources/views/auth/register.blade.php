@extends('auth.wrapper')

@section('auth-form-register', 'active')

@section('auth-form')
    <form class="card border-top-0" method="POST" action="{{ route('register') }}" name="register"
        style="border-top-left-radius: 0; border-top-right-radius: 0;">
        {{ csrf_field() }}
        <div class="card-body">
            <div class="form-group row">
                <label for="name" class="col-md-4 col-form-label text-md-right">@lang('auth.label.name')</label>
                <div class="col-md-8">
                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                    @if ($errors->has('name'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label for="phone" class="col-md-4 col-form-label text-md-right">@lang('auth.label.phone')</label>
                <div class="col-md-8">
                    <input id="phone" type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone') }}" required>
                    @if ($errors->has('phone'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('phone') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label for="company" class="col-md-4 col-form-label text-md-right">@lang('auth.label.company')</label>
                <div class="col-md-8">
                    <input id="company" type="text" class="form-control{{ $errors->has('company') ? ' is-invalid' : '' }}" name="company" value="{{ old('company') }}" >

                    @if ($errors->has('company'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('company') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label for="city" class="col-md-4 col-form-label text-md-right">@lang('auth.label.city')</label>
                <div class="col-md-8">
                    <input id="city" type="text" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" value="{{ old('city') }}" required>

                    @if ($errors->has('city'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('city') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label for="email" class="col-md-4 col-form-label text-md-right">@lang('auth.label.email')</label>
                <div class="col-md-8">
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label for="password" class="col-md-4 col-form-label text-md-right">@lang('auth.label.password')</label>
                <div class="col-md-8">
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                    @if ($errors->has('password'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">@lang('auth.label.password-confirm')</label>
                <div class="col-md-8">
                    <input id="password-confirm" type="password" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" name="password_confirmation" required>
                    @if ($errors->has('password_confirmation'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group row justify-content-center">
                <div class="col-md-6">
                    <button
                        type="submit"
                        class="btn btn-primary btn-block g-recaptcha {{ $errors->has('g-recaptcha-response') ? ' is-invalid' : '' }}"
                        data-sitekey="{{ config('recaptcha.site_key') }}"
                        data-callback="onRegister"
                        data-action="submit"
                    >
                        @lang('auth.button.register')
                    </button>
                    @if ($errors->has('g-recaptcha-response'))
                        <span class="invalid-feedback text-center">
                            <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
        </div>
    </form>
@endsection

@push('scripts')
    @if (config('recaptcha.is_active'))
        <script src="//www.google.com/recaptcha/api.js"></script>
        <script>
            function onRegister(token) {
                document.querySelector('form[name="register"]').submit();
            }
        </script>
    @endif
@endpush