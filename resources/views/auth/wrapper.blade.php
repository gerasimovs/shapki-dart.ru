@extends('layouts.default')

@section('content')
<div class="row justify-content-center">
    <div class="col-xl-8">
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link bg-light text-danger @yield('auth-form-login')" href="{{ route('login') }}">@lang('auth.login')</a>
            </li>
            <li class="nav-item">
                <a class="nav-link bg-light @yield('auth-form-register')" href="{{ route('register') }}">@lang('auth.register')</a>
            </li>
            <li class="nav-item ml-auto">
                <a class="nav-link bg-light text-danger @yield('auth-form-forgot')" href="{{ route('password.request') }}">@lang('auth.button.password-forgot')</a>
            </li>
        </ul>
        @yield('auth-form')
    </div>
</div>
@endsection
