@extends('auth.wrapper')

@section('auth-form')
    <form class="card border-top-0" method="POST" action="{{ route('password.request') }}" 
        style="border-top-left-radius: 0; border-top-right-radius: 0;">
        {{ csrf_field() }}
        <input type="hidden" name="token" value="{{ $token }}">
        <div class="card-body">
            <div class="form-group row">
                <label for="email" class="col-md-4 col-form-label text-md-right">@lang('auth.label.email')</label>
                <div class="col-md-8">
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label for="password" class="col-md-4 col-form-label text-md-right">@lang('auth.label.password')</label>
                <div class="col-md-8">
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                    @if ($errors->has('password'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">@lang('auth.label.password-confirm')</label>
                <div class="col-md-8">
                    <input id="password-confirm" type="password" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" name="password_confirmation" required>
                    
                    @if ($errors->has('password_confirmation'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group row justify-content-center">
                <div class="col-md-6">
                    <button type="submit" class="btn btn-primary btn-block">
                        @lang('auth.button.password-reset')
                    </button>
                </div>
            </div>
        </div>
    </form>
@endsection
