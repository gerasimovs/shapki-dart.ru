@extends('auth.wrapper')

@section('auth-form-forgot', 'active')

@section('auth-form')
    <form class="card border-top-0" method="POST" action="{{ route('password.email') }}" 
        style="border-top-left-radius: 0; border-top-right-radius: 0;">
        {{ csrf_field() }}
        <div class="card-body">
            <div class="form-group row">
                <label for="email" class="col-md-4 col-form-label text-md-right">@lang('auth.label.email')</label>
                <div class="col-md-8">
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group row justify-content-center">
                <div class="col-md-6">
                    <button type="submit" class="btn btn-primary btn-block">
                        @lang('auth.button.password-send')
                    </button>
                </div>
            </div>
        </div>
    </form>
@endsection