@extends('auth.wrapper')

@section('auth-form-login', 'active')

@section('auth-form')
    <form class="card border-top-0" method="POST" action="{{ route('login') }}" 
        style="border-top-left-radius: 0; border-top-right-radius: 0;">
        {{ csrf_field() }}
        <div class="card-body">
            <div class="form-group row">
                <label for="email" class="col-md-4 col-form-label text-md-right">@lang('auth.label.email')</label>
                <div class="col-md-8">
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <label for="password" class="col-md-4 col-form-label text-md-right">@lang('auth.label.password')</label>
                <div class="col-md-8">
                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                    @if ($errors->has('password'))
                        <span class="invalid-feedback">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-8 offset-md-4">
                    <div class="form-check">
                        <label class="form-check-label">
                            <input type="checkbox" name="remember" class="form-check-input" 
                                @if ($errors->any()) {{ old('remember') ? 'checked' : '' }} @else checked @endif
                            >
                            @lang('auth.label.remember')
                        </label>
                    </div>
                </div>
            </div>
            <div class="form-group row justify-content-center">
                <div class="col-md-6">
                    <button type="submit" class="btn btn-primary btn-block">
                        @lang('auth.button.login')
                    </button>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('javascript')
@endsection