@extends('admin.index')

@section('content')
    <button class="btn btn-sm btn-outline-secondary hidden-print float-right" onclick="window.print()">
        <i class="fas fa-print" aria-hidden="true"></i> На печать
    </button>
    <div class="btn-group btn-group-sm hidden-print float-right mr-2">
        <button class="btn btn-outline-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fas fa-save"></i> Сохранить
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="{{ route('admin.orders.blank', $order->id) }}">Бланк заказа</a>
            <a class="dropdown-item" href="{{ route('admin.orders.toWord', $order->id) }}">В формате Word</a>
            <a class="dropdown-item" href="{{ route('admin.orders.toCsv', $order->id) }}">В формате CSV</a>
        </div>
    </div>
    <div class="btn-group btn-group-sm hidden-print float-right mr-2">
        <a class="btn btn-sm btn-outline-secondary" href="{{ route('admin.orders.edit', $order->id) }}">Редактировать</a>
    </div>
    <div class="btn-group btn-group-sm hidden-print float-right mr-2">
        <button class="btn btn-outline-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            CRM
        </button>
        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="{{ route('admin.orders.toCrm', $order->id) }}">Выставить счёт</a>
        </div>
    </div>

    <h3>Заказ пользователя {{ $order->user->name }}</h3>
    @if (count($offersGroup))
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th colspan="2" style="width: 60%;">Товар</th>
                        <th style="width: 120px;">Кол-во</th>
                        <th>Цена</th>
                        <th>Стоимость</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($offersGroup as $offers)
                        @foreach ($offers as $offer)
                        <tr>
                            @if ($loop->first)
                                <td rowspan="{{ $offers->count()+1 }}">
                                    @if (count($offer->product->images))
                                        <img class="cart-thumbs" src="{{ $offer->product->images->first()->url }}">
                                    @endif
                                    {{--<div class="cart-thumbs" style="background-image: url({{ $offer->product['images'][0]['url'] }});"></div>--}}
                                    <a href="{{ route('products.show', $offer->pivot->product_id) }}">
                                        {{ $offer->pivot->vendor_code }}
                                    </a>
                                    <p class="hidden-print">{{ $offer->product->description }}</p>
                                    @if ($offer->product->suppliers->count())
                                        <p class="hidden-print">
                                            @foreach ($offer->product->suppliers as $supplier)
                                                <span class="btn btn-secondary disabled">{{ $supplier->name }}</span>
                                            @endforeach
                                        </p>
                                    @endif
                                </td>
                            @endif
                            <td>{{ $offer->option->name }}</td>
                            <td>
                                <input type="text" class="form-control text-center" value="{{ $offer->pivot->qty }}" readonly>
                            </td>
                            <td class="text-nowrap">@money($offer->pivot->price) &#8381;</td>
                            <td class="text-nowrap">@money($offer->pivot->price * $offer->pivot->qty) &#8381;</td>
                        </tr>
                        @endforeach
                        <tr>
                            <td class="font-italic">Всего по позиции:</td>
                            <td>
                                <input type="text" class="form-control text-center" value="{{ $offers->sum('pivot.qty') }}" readonly>
                            </td>
                            <td>&nbsp;</td>
                            <td class="font-italic">@money($offers->sum('pivot.qty') * $offer->pivot->price) &#8381;</td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="2">Всего:</td>
                        <td>
                            <input type="text" class="form-control text-center" value="{{ $order->totalQty }}" readonly>
                        </td>
                        <td>&nbsp;</td>
                        <td class="text-nowrap">{{ number_format($order->totalPrice, 2, ',', ' ') }} &#8381;</td>
                    </tr>
                </tbody>
            </table>
        </div>
    @endif

    @if ($order->comment)
        <div class="alert alert-success" role="alert">
            <p class="alert-heading font-italic">Комментарий к заказу:</p>
            <p class="mb-0">{{ $order->comment }}</p>
        </div>
    @endif

@endsection