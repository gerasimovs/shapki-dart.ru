@extends('admin.index')

@section('content')
    <h3>Заказ пользователя {{ $order->user->name }}</h3>
    @forelse ($offersGroup as $supplier => $productsRow)
        <h4>{{$supplier}}</h4>
        <table class="table" style="max-width: 100%;">
            <tbody>
                @foreach ($productsRow->chunk(4) as $products)
                    <tr>
                        @foreach ($products as $offers)
                            <td class="w-25">
                                @foreach ($offers as $offer)
                                    @if ($loop->first)
                                        @if (count($offer->product->images))

                                        {{--<img src="{{ Image::make($offer->product->images->first()->url)->resize(100, null, function ($constraint) {
                                                $constraint->aspectRatio();
                                            })->encode('data-url')->encoded }}"> --}}
                                        <img src="{{ $offer->product->images->first()->url }}" style="width: 2.5cm; max-width: 2.5cm;"> @endif
                                        <div class="card-body">
                                        {{ $offer->pivot->vendor_code }}<br>
                                        <span class="small font-italic hidden-print">{{ $offer->product->description }}</span><br>
                                    @endif
                                    {{ $offer->option->name }} - {{ $offer->pivot->qty }}<br>
                                @endforeach
                                {{$offers->sum('pivot.qty')}} шт. * @money($offer->pivot->price) = @money($offers->sum('pivot.qty') * $offer->pivot->price)
                                </div>
                            </td>
                        @endforeach
                    </tr>
                @endforeach
            </tbody>
        </table>
    @empty
        Ошибка!
    @endforelse

@endsection