@extends('admin.index')

@section('content')

    <h3>Заказы пользователей</h3>
    
    @if (count($orders))
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th>№ заказа</th>
                        <th>Пользователь</th>
                        <th>Товаров в заказе</th>
                        <th>Сумма заказа</th>
                        <th>Статус заказа</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($orders as $order)
                        <tr>
                            <td><a href="{{ route('admin.orders.show', $order->id) }}">Заказ №{{ $order->id }} <br>от&nbsp;@datetime($order->created_at)</a></td>
                            <td>{{ $order->user->name }}<br>{{ $order->user->email }}</td>
                            <td>{{ $order->totalQty }}</td>
                            <td>{{ number_format($order->totalPrice, 2, ',', ' ') }}</td>
                            <td>@lang('order.' . $order->status)</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    @else
        <p>Отсутствуют</p>
    @endif

@endsection