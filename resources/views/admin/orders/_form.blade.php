@component('components.forms.select', ['name' => 'user_id', 'options' => \App\User::pluck('name', 'id') ?? [], 'selected' => $order->user_id ?? old('user_id') ])
    Пользователь
@endcomponent

@component('components.forms.select', ['name' => 'status', 'options' => $order->getStatuses() ?? [], 'selected' => $order->status ?? old('status') ])
    Статус заказа
@endcomponent

<table class="table">
	<thead>
		<tr>
			<td>Артикул</td>
			<td>Опция</td>
			<td>Цена</td>
			<td>Количество</td>
			<td>Подтверждено</td>
		</tr>
	</thead>
	<tbody>
		@foreach ($order->offers as $offer)
			<tr>
				<td>
					{{ $offer->pivot->vendor_code }}
				</td>
				<td>
					{{ $offer->pivot->color }}
				</td>
				<td>
					@component('components.forms._input', ['name' => 'offers[' . $offer->id . '][price]'])
						{{ $offer->pivot->price }}
					@endcomponent
				</td>
				<td>
					@component('components.forms._input', ['name' => 'offers[' . $offer->id . '][qty]'])
						{{ $offer->pivot->qty }}
					@endcomponent
				</td>
				<td>
					@component('components.forms._input', ['name' => 'offers[' . $offer->id . '][confirm]'])
						{{ $offer->pivot->confirm }}
					@endcomponent
				</td>
				
			</tr>
		@endforeach

	</tbody>
</table>
