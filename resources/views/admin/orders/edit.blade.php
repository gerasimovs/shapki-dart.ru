@extends('admin.index')

@section('content')
    <h3>Редактирование <a href="{{ route('admin.orders.show', $order->id) }}">заказа №{{ $order->id }} от&nbsp;@datetime($order->created_at)</a></h3>
    <form method="POST">
        @include('admin.orders._form')
        @csrf @method('PUT')
        <button type="submit" formaction="{{ route('admin.orders.update', $order->id) }}" class="btn btn-primary">Сохранить изменения</button>
    </form>
@endsection