@extends('admin.index')

@section('content')
    @php
        $filter = Request::input('filter');
    @endphp

    <h3>Уведомления ({{ $filter }})</h3>
    
    @if ($notifications->isNotEmpty())
        <ul class="nav nav-pills">
            <li class="nav-item">
                <span class="nav-link">Фильтрация</span>
            </li>
            <li class="nav-item @if (!$filter) active @endif">
                <a class="nav-link" href="{{ url()->current() }}">Все</a>
            </li>
            <li class="nav-item">
                <a class="nav-link @if ($filter == 'no_available') active @endif " 
                    href="{{ request()->fullUrlWithQuery(['filter' => 'no_available']) }}"
                >Закончились</a>
            </li>
            <li class="nav-item @if ($filter == 'available') active @endif ">
                <a class="nav-link" 
                    href="{{ request()->fullUrlWithQuery(['filter' => 'available']) }}"
                >Поступления</a>
            </li>
            <li class="nav-item @if ($filter == 'price') active @endif ">
                <a class="nav-link" 
                    href="{{ request()->fullUrlWithQuery(['filter' => 'price']) }}"
                >Изменение цены</a>
            </li>
        </ul>
        @include('partials.pagination', [
            'items' => $notifications,
            'appends' => compact('filter')
        ])
        @foreach ($notifications as $notification)
            <div class="card border-{{ $notification->data['status'] ?? 'secondary' }} my-1">
                <div class="card-body">
                    @if ($notification->data['is_html'] ?? false)
                        {!! is_string($notification->data['message']) ? $notification->data['message'] : json_encode($notification->data['message']) !!}
                    @else
                        {{ $notification->data['message'] }}
                    @endif
                </div>
                <div class="card-footer small text-right">
                    @if ($notification->created_at->diffInDays(now()) > 0)
                        {{ $notification->created_at->format('d.m.Y в H:i') }}
                    @else
                        {{ $notification->created_at->diffForHumans() }}
                    @endif
                </div>
            </div>
        @endforeach
        @include('partials.pagination', [
            'items' => $notifications,
            'appends' => compact('filter')
        ])
    @else
        <p>Отсутствуют</p>
    @endif

@endsection
