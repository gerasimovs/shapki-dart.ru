@extends('admin.index')

@section('content')
    <h3>Редактирование парсера для <a href="{{ route('admin.products.edit', $parser->product) }}">{{ $parser->product->vendor_code }}</a></h3>
    <form action="{{ route('admin.products.parsers.update', $parser->id) }}" method="POST">
        @component('components.forms.input', ['value' => $parser->url ?? '', 'name' => 'parser.url'])
            Ссылка для парсинга
        @endcomponent
        @component('components.forms.input', ['value' => $parser->price ?? '', 'name' => 'parser.price'])
            Закупочная цена
        @endcomponent
        {{ csrf_field() }} 
        {{ method_field('PUT') }}
        <button type="submit" class="btn btn-primary">Сохранить изменения</button>
    </form>
@endsection