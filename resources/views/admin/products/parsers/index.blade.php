@php
    use App\Properties\ParserProperty;
@endphp

@extends('admin.index')

@section('content')
    <h3>{{ $header ?? 'Автоматический парсинг' }} ({{ $parsers->total() }})</h3>

    @include('partials.pagination', [
        'items' => $parsers, 
    ])
    <div class="table-responsive">
        <table class="table bg-light">
            <thead class="thead-light">
                <tr>
                    <th>Товар</th>
                    <th>Ссылка</th>
                    <th>Действие</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($parsers as $parser)
                    @php
                        $product = $parser->product;
                    @endphp
                    <tr>
                        <td>
                            <a href="{{ route('admin.products.edit', $parser->product->id) }}">{{ $product->vendor_code }}</a>
                            <div>
                                @if ($product->trashed())
                                    <small class="text-danger d-block">Товар удален {{ $product->present()->deleted_at }}</small> 
                                @elseif ($product->created_at)
                                    <small class="text-success d-block">Товар создан {{ $product->present()->created_at }}</small> 
                                @endif

                                @if ($product->updated_at > $product->created_at)
                                    <small class="text-warning d-block">Товар обновлен {{ $product->present()->updated_at }}</small> 
                                @elseif (!$product->created_at)
                                    <small class="text-info d-block">Товар не сушествует</small> 
                                @endif
                            </div>
                        </td>
                        <td>
                            <a href="{{ $parser->url }}" target="_blank">{{ $parser->url }}</a>
                            <div>
                                <form method="POST" class="d-inline-block">
                                    {{ csrf_field() }}
                                    @if ($product->trashed() && $parser->status == ParserProperty::STATUS['arrival'])
                                        <small class="text-success">Товар появился в продаже!</small> 
                                        <input class="btn btn-link btn-sm product-restore" type="submit" value="Восстановить" formaction="{{ route('admin.products.restore', $product->id) }}">
                                    @elseif (!$product->trashed() && $parser->status == ParserProperty::STATUS['missing'])
                                        {{ method_field('DELETE') }}
                                        <small class="text-danger">Товар недоступен!</small> 
                                        <input class="btn btn-link btn-sm product-delete" type="submit" value="Удалить" formaction="{{ route('admin.products.destroy', $product->id) }}">
                                    @endif
                                </form>
                            </div>
                        </td>
                        <td>
                            <a href="{{route('admin.products.parsers.edit', $parser) }}">Редактировать</a>
                            <form class="d-inline" action="{{ route('admin.products.parsers.parse', $parser) }}" method="POST">
                                @csrf
                                <button class="btn btn-link btn-sm" type="submit">Спарсить</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    @include('partials.pagination', [
        'items' => $parsers, 
    ])
@endsection
