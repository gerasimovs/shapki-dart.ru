@extends('admin.index')

@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <h3>Добавление товара</h3>
    <form action="{{ route('admin.products.store') }}" method="POST">
        @include('admin.products.form.index')
        {{ csrf_field() }}
        <button type="submit" class="btn btn-primary">Добавить товар</button>
    </form>
@endsection