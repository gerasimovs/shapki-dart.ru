@extends('admin.index')

@section('content')
    @include('admin.products._partials.search')

    <a class="btn btn-sm btn-outline-secondary float-right" href="{{ route('admin.products.create') }}">Добавить</a>
    <a class="btn btn-sm btn-outline-secondary float-right mx-1" href="{{ route('admin.products.parsers.index') }}">Парсер</a>
    <h3>{{ $header ?? 'Все товары' }}</h3>
    @include('admin.products._list')
@endsection