{{ method_field('DELETE') }}
@if ($product->trashed())
    <input type="hidden" name="restore" value="1">
    <button class="btn btn-success" type="submit" data-tooltip="true" title="Восстановить" formaction="{{ route('admin.products.destroy', $product->id) }}"><i class="material-icons">&#xE8B3;</i></button> 
@else
    <button class="btn btn-danger product-delete" type="submit" data-tooltip="true" title="Удалить" formaction="{{ route('admin.products.destroy', $product->id) }}"><i class="material-icons">&#xE872;</i></button>
@endif