@if (isset($categories[$category->id]))
    <optgroup label="{{ $category->name }}">
        @foreach ($categories[$parent] as $category)
            @include('admin.products._partials.coption', ['parent' => $category->id])
        @endforeach
    </optgroup> 
@else
    <option value="{{ $category->id }}" @if (isset($product) && ($category->id == $product->category_id)) selected @endif>{{ $category->name }}</option>
@endif