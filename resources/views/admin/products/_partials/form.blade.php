    <ul class="nav nav-pills mb-3">
        <li class="nav-item">
            <a class="nav-link active" data-toggle="tab" href="#general-tab" role="tab" aria-controls="general-tab" aria-selected="true">Общее</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#options-tab" role="tab" aria-controls="options-tab" aria-selected="false">Опции</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#images-tab" role="tab" aria-controls="images-tab" aria-selected="false">Изображения</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="tab" href="#extend-tab" role="tab" aria-controls="extend-tab" aria-selected="false">Дополнительно</a>
        </li>
    </ul>
    
    <div class="tab-content">
        <div class="tab-pane fade show active" id="general-tab">
            @include('admin.products._partials.general-form')
        </div>
        <div class="tab-pane fade" id="options-tab">
            @include('admin.products._partials.options-form')
        </div>
        <div class="tab-pane fade" id="images-tab">
            @include('admin.products._partials.images-form')
        </div>
        <div class="tab-pane fade" id="extend-tab">
            @include('admin.products._partials.suppliers-form')
            @include('admin.products._partials.related-form')
        </div>
    </div>