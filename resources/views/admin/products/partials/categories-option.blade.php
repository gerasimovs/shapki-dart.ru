<option value="{{ $category->id }}" 
    @if ( isset($product) && $product->has('categories') && (in_array($category->id, $product->categories->pluck('id')->toArray()) ) ) 
        selected 
    @endif>
    {{ str_repeat("&nbsp;&bull;&nbsp;", $level) }} {{ $category->name }} 
</option>

@if (isset($categories[$category->id]))
    @foreach ($categories[$parent] as $category)
        @include('admin.products.partials.categories-option', ['parent' => $category->id, 'level' => $level + 1])
    @endforeach
@endif