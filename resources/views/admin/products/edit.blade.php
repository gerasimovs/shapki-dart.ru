@extends('admin.index')

@section('content')
    <form  method="POST" class="float-right">
        {{ csrf_field() }}
        @if ($product->trashed())
            <button class="btn btn-sm btn-outline-secondary" type="submit" data-toggle="tooltip" title="Восстановить" formaction="{{ route('admin.products.restore', $product->id) }}"><i class="material-icons">&#xE8B3;</i> Восстановить</button> 
        @else
            {{ method_field('DELETE') }}
            <button class="btn btn-sm btn-outline-secondary" type="submit" data-toggle="tooltip" title="Удалить" formaction="{{ route('admin.products.destroy', $product->id) }}"><i class="material-icons">&#xE872;</i> Удалить</button>
        @endif
    </form>

    <h3>Редактирование товара <a href="{{ route('products.show', $product->id) }}">{{ $product->vendor_code }}</a></h3>
    <p class="small text-muted">Товар добавлен: {{ $product->present()->created_at }}@if ($product->updated_at != $product->created_at), последнее обновление товара: {{ $product->present()->updated_at }}@endif</p>
    <form action="{{ route('admin.products.update', $product->id) }}" method="POST">
        @include('admin.products.form.index')
        {{ csrf_field() }} 
        {{ method_field('PUT') }}
        <button type="submit" class="btn btn-primary">Сохранить изменения</button>
    </form>
@endsection

@push('scripts')
    <script src="{{ asset('js/product.manage.js') }}"></script>
@endpush