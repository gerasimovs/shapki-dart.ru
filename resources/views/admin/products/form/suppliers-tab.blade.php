@if ($errors->has('suppliers'))
    <div class="alert alert-error">
        {{ $errors->first('suppliers') }}
    </div>
@endif

<h4>Поставщики</h4>
<div class="mb-3">
    @if (isset($product->suppliers))
        @foreach ($product->suppliers as $supplier)
            <div class="alert alert-light alert-dismissible border" style="max-width: 18rem;">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h5 class="alert-heading">{{ $supplier->name }}</h5>
                @if ($supplier->address) <p class="small mb-0">Адрес: {{ $supplier->address }}</p> @endif
                @if ($supplier->phone) <p class="small mb-0">Телефон: {{ $supplier->phone }}</p> @endif
                <input type="hidden" name="suppliers[]" value="{{ $supplier->id }}">
            </div>
        @endforeach
    @else
        Поставщики не указаны
    @endif
</div>