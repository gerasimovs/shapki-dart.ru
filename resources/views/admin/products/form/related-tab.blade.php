@if ($errors->has('related'))
    <div class="alert alert-error">
        {{ $errors->first('related') }}
    </div>
@endif

@if (isset($product->related))
    <h4>Сопутствующие товары</h4>
    <div class="row">
        @foreach ($product->related as $related)
            <div class="related-card col-sm-3 mb-3" style="order: {{ $loop->iteration }};">
                <div class="alert alert-light alert-dismissible p-0" style="">
                    <div class="card-header"><a href="{{ route('admin.products.edit', $related->id) }}">{{ $related->vendor_code }}</a></div>
                    {{-- <span class="order-group">
                        <span class="order-control order-dec">-</span>
                        <input class="order-current" type="text" name="related[sort][]" data-order="{{ $loop->iteration }}" value="{{ $loop->iteration }}" readonly>
                        <span class="order-control order-inc">+</span>
                    </span> --}}

                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <img class="card-img-bottom" src="{{ $related->images->first()->url }}">
                    <input type="hidden" name="related[id][]" value="{{ $related->id }}">
                </div>
            </div>
        @endforeach
    </div>
    <div class="input-group mb-3 related-add">
        <input type="text" class="form-control form-control-sm" name="related_code[]">
        <div class="input-group-append">
            <button class="btn btn-sm btn-outline-primary" type="button">Добавить товар</button>
        </div>
    </div>
@endif