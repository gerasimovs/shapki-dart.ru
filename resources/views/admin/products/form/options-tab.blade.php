@if ($errors->has('options'))
    <div class="alert alert-error">
        {{ $errors->first('options') }}
    </div>
@endif

<div class="mb-3">
    @if (isset($product->options))
        @foreach ($product->options as $option)
            <div class="custom-control custom-checkbox">
                <input type="checkbox" name="options[]" class="custom-control-input" id="option{{ $option->id }}" value="{{ $option->id }}" @if ($option->offer->status == 1) checked @endif>
                <label class="custom-control-label" for="option{{ $option->id }}">{{ $option->name }}</label>
            </div>
        @endforeach
    @endif

    <div class="input-group mb-3 options-add">
        <input type="text" class="form-control form-control-sm" name="options_name[]">
        <div class="input-group-append">
            <button class="btn btn-sm btn-outline-primary" type="button" onclick="">Добавить опцию</button>
        </div>
    </div>
</div>
