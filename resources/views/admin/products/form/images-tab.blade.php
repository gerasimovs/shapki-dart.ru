@if ($errors->has('images'))
    <div class="alert alert-error">
        {{ $errors->first('images') }}
    </div>
@endif

@if (isset($product->images))
    <div class="row">
        @foreach ($product->images as $image)
            <div class="image-card col-sm-3 mb-3" style="order: {{ $loop->iteration }};">
                <div class="alert alert-light alert-dismissible p-0" style="">

                    <span class="order-group">
                        <span class="order-control order-dec">-</span>
                        <input class="order-current" type="text" name="images[{{ $image->id }}][sort]" data-order="{{ $image->pivot->sort }}" value="{{ $loop->iteration }}" readonly>
                        <span class="order-control order-inc">+</span>
                    </span>

                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <img class="card-img" src="{{ $image->url }}">
                    <input class="form-control" type="text" name="images[{{ $image->id }}][description]" value="{{ $image->pivot->description }}">
                </div>
            </div>
        @endforeach
    </div>
    <div class="input-group mb-3 images-add">
        <input type="text" class="form-control form-control-sm" name="images_url[]">
        <div class="input-group-append">
            <button class="btn btn-sm btn-outline-primary" type="button">Добавить изображение</button>
        </div>
    </div>
@endif