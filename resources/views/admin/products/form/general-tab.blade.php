@component('components.forms.input', ['value' => $product->vendor_code ?? null, 'name' => 'vendor_code'])
    Артикул
@endcomponent

<div class="form-group">
    <label for="name">Название</label>
    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $product->name ?? old('name') }}">
    @if ($errors->has('name'))
        <span class="invalid-feedback">
            <strong>{{ $errors->first('name') }}</strong>
        </span>
    @endif
</div>

<div class="form-group">
    <label for="price">Цена в рублях</label>
    <input id="price" type="number" step="1" class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}" name="price" value="{{ $product->price ?? old('price') }}" required>
    @if ($errors->has('price'))
        <span class="invalid-feedback">
            <strong>{{ $errors->first('price') }}</strong>
        </span>
    @endif
</div>

<div class="form-group">
    <label for="description">Описание</label>
    <textarea id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description">{{ $product->description ?? old('description') }}</textarea>
    @if ($errors->has('description'))
        <span class="invalid-feedback">
            <strong>{{ $errors->first('description') }}</strong>
        </span>
    @endif
</div>

<div class="form-group">
    <label for="unit">Единица измерения</label>
    <select class="form-control{{ $errors->has('unit_id') ? ' is-invalid' : '' }}" id="input-unit" name="unit_id" required>
        @foreach ($product->getUnits() as $unitId => $unit)
            <option value="{{ $unitId }}" @if ($product->unit_id == $unitId) selected @endif>@lang('product.unit.' . $unit)</option>
        @endforeach 
    </select>
    @if ($errors->has('unit_id'))
        <span class="invalid-feedback">
            <strong>{{ $errors->first('unit_id') }}</strong>
        </span>
    @endif
</div>

<div class="form-group">
    <label for="status">Наличие товара</label>
    <select class="form-control{{ $errors->has('status_id') ? ' is-invalid' : '' }}" id="input-status" name="status_id" required>
        @foreach ($product->getStatuses() as $statusId => $status)
            <option value="{{ $statusId }}" @if ($product->status_id == $statusId) selected @endif>@lang('product.availability.' . $status)</option>
        @endforeach 
    </select>
    @if ($errors->has('status_id'))
        <span class="invalid-feedback">
            <strong>{{ $errors->first('status_id') }}</strong>
        </span>
    @endif
</div>
