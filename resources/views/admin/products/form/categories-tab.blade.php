<div class="form-group">
    <label for="category-input">Коллекция</label>
    <select name="category_id" class="form-control{{ $errors->has('category_id') ? ' is-invalid' : '' }}" id="category-input">
        <option value="0">--- Выберите основную коллекцию ---</option>
        @foreach ($categories[''] as $category)
            @include('admin.products._partials.coption', ['parent' => $category->id])
        @endforeach
    </select>
</div>

<div class="form-group">
    <label for="categories-input">Дополнительные коллекции</label>
    <select multiple name="categories[]" class="form-control{{ $errors->has('categories') ? ' is-invalid' : '' }}" id="categories-input" style="height: 18rem;">
        <option>--- Выберите дополнительную коллекцию ---</option>
        @foreach ($categories[''] as $category)
            @include('admin.products.partials.categories-option', ['parent' => $category->id, 'level' => 0])
        @endforeach
    </select>
</div>