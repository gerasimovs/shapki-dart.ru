@php
    $available = Request::input('available', false);
    $filter = Request::input('filter', []);
    $filterCollection = collect($filter);
    $sorted = Request::input('sorted', false);
@endphp

<ul class="nav nav-pills">
    <li class="nav-item">
        <span class="nav-link">Наличие</a>
    </li>
    <li class="nav-item">
        <a class="nav-link @if (!$available) active @endif " href="{{ url()->current() }}">Все</a>
    </li>
    <li class="nav-item">
        <a class="nav-link @if ($available == 'true') active @endif " 
            href="{{ request()->fullUrlWithQuery(['available' => 'true']) }}"
        >В наличии</a>
    </li>
    <li class="nav-item">
        <a class="nav-link @if ($available == 'false') active @endif " 
            href="{{ request()->fullUrlWithQuery(['available' => 'false']) }}"
        >Архивные</a>
    </li>
</ul>

<ul class="nav nav-pills">
    <li class="nav-item">
        <span class="nav-link">Выводить</a>
    </li>
    <li class="nav-item">
        <a class="nav-link @if ($isFiltered = $filterCollection->contains('no_category')) active @endif " 
            href="{{ request()->fullUrlWithQuery(['filter' => $isFiltered ? $filterCollection->flip()->except('no_category')->keys()->toArray() : array_merge($filter, ['no_category'])]) }}"
        >Без категории</a>
    </li>
    <li class="nav-item">
        <a class="nav-link @if ($isFiltered = $filterCollection->contains('no_photo')) active @endif " 
            href="{{ request()->fullUrlWithQuery(['filter' => $isFiltered ? $filterCollection->flip()->except('no_photo')->keys()->toArray() : array_merge($filter, ['no_photo'])]) }}"
        >Без изображений</a>
    </li>
    <li class="nav-item">
        <a class="nav-link @if ($isFiltered = $filterCollection->contains('no_supplier')) active @endif " 
            href="{{ request()->fullUrlWithQuery(['filter' => $isFiltered ? $filterCollection->flip()->except('no_supplier')->keys()->toArray() : array_merge($filter, ['no_supplier'])]) }}"
        >Без поставщиков</a>
    </li>
    <li class="nav-item">
        <a class="nav-link @if ($isFiltered = $filterCollection->contains('no_parser')) active @endif " 
            href="{{ request()->fullUrlWithQuery(['filter' => $isFiltered ? $filterCollection->flip()->except('no_parser')->keys()->toArray() : array_merge($filter, ['no_parser'])]) }}"
        >Без парсинга</a>
    </li>
</ul>

<ul class="nav nav-pills">
    <li class="nav-item">
        <span class="nav-link">Сортировать:</span>
    </li>
    <li class="nav-item">
        <a class="nav-link @if ($sorted == 'updated') active @endif " 
            href="{{ request()->fullUrlWithQuery(['sorted' => 'updated']) }}"
        >По дате обновления</a>
    </li>
</ul>

@include('partials.pagination', [
    'items' => $products, 
    'appends' => compact('available', 'filter', 'sorted'),
])

<table class="table products-list">
    <thead class="thead-light">
        <tr>
            <th>#</th>
            <th>Артикуль / Название товара</th>
            <th>Цена</th>
            <th>Действия</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($products as $product)
        <tr id="product-{{ $product->id }}">
            <td><input type="checkbox" name="products[]" value="{{ $product->id }}" id=""></td>
            <td>
                <div class="cart-thumbs" style="background-image: url({{ $product->images->first()->url ?? asset('images/no_photo.png') }});"></div>
                <div class="text-muted small">
                    @if ($product->category)
                        {{ $product->category->name }} @if ($product->category->trashed()) <span class="text-danger">(удалена)</span> @endif
                    @else
                        Категория не выбрана
                    @endif
                </div>
                <a href="{{ route('admin.products.edit', $product->id) }}">{{ $product->vendor_code }}</a><br>
                {{ $product->name }}<br>
                @if ($product->trashed())
                    <div><small class="text-danger">Товар удален {{ $product->present()->deleted_at }}</small></div>
                @endif
                <div>
                    @if ($product->updated_at > $product->created_at)
                        <small class="text-warning">Товар обновлен {{ $product->present()->updated_at }}</small> 
                    @elseif ($product->created_at)
                        <small class="text-success">Товар создан {{ $product->present()->created_at }} </small> 
                    @else
                        <small class="text-info">Товар не сушествует</small> 
                    @endif
                </div>

            </td>
            <td>{{ $product->price }} руб.</td>
            <td>
                <a class="btn btn-success" href="{{ route('admin.products.sorting', $product) }}" data-tooltip="true" title="Переместить вверх"><i class="fas fa-arrow-up"></i></a>
                <a class="btn btn-info" href="{{ route('products.show', $product) }}" data-tooltip="true" title="Просмотреть" target="_blank"><i class="fas fa-eye"></i></a>
                <a class="btn btn-success" href="{{ route('admin.products.createAs', $product) }}" data-tooltip="true" title="Скопировать"><i class="fas fa-copy"></i></a>

                <form method="POST" class="d-inline-block">
                    {{ csrf_field() }}
                    @if ($product->trashed())
                        <button class="btn btn-success product-restore" type="submit" data-tooltip="true" title="Восстановить" formaction="{{ route('admin.products.restore', $product->id) }}"><i class="fas fa-trash-restore"></i></button> 
                    @else
                        {{ method_field('DELETE') }}
                        <button class="btn btn-danger product-delete" type="submit" data-tooltip="true" title="Удалить" formaction="{{ route('admin.products.destroy', $product->id) }}"><i class="fas fa-trash"></i></button>
                    @endif
                </form>
                <form method="POST" class="d-inline-block">
                    {{ csrf_field() }}
                    @foreach ($product->suppliers as $supplier)
                        @continue(empty($supplier->pivot->url))
                        <button class="btn btn-info" type="submit" data-tooltip="true" title="Обновить c {{ $supplier->name }}" formaction="{{ route('admin.products.parsers.parse', $supplier->pivot) }}"><i class="fas fa-sync"></i></button>
                    @endforeach
                </form>

        </tr>
        @empty
            <tr><td colspan="4">Товары отсутствуют</td></tr>
        @endforelse
    </tbody>
</table>

@include('partials.pagination', [
    'items' => $products, 
    'appends' => [
        'filter' => Request::input('filter'), 
        'sorted' => Request::input('sorted'),
    ]
])

<div class="mb-1">
    <button id="products-check" class="btn btn-primary btn-sm">Выделить все</button>
    <button class="btn btn-primary btn-sm products-export" data-action="{{ route('admin.products.toCsv') }}" data-method="post">Сохранить в CSV</button>
    <button class="btn btn-primary btn-sm products-export" data-action="{{ route('admin.products.toWord') }}" data-method="post">Сохранить в Word</button>
    <button class="btn btn-primary btn-sm products-export" data-action="{{ route('admin.products.toBlock') }}" data-method="post">Сохранить в Word (4 в ряд)</button>
    <button class="btn btn-primary btn-sm product-mass-change" data-action="{{ route('admin.products.toTop') }}" data-method="post">Поднять вверх</button>
</div>

<div class="input-group input-group-sm mb-1">
    <div class="input-group-prepend">
        <label class="input-group-text" for="categories">Переместить в коллекцию:</label>
    </div>
    <select id="categories" name="category_id" class="form-control{{ $errors->has('category_id') ? ' is-invalid' : '' }}">
        <option value="0">Выберите коллекцию</option>
        @foreach (\App\Category::oldest('name')->get(['id', 'name']) as $category)
            <option value="{{ $category->id }}">{{ $category->name }}</option>
        @endforeach
    </select>
    <div class="input-group-append">
        <button id="products-moving" class="btn btn-primary btn-sm product-mass-change" data-action="{{ route('admin.products.moving') }}" data-method="post">Переместить</button>
    </div>
</div>

<div class="input-group input-group-sm mb-3">
    <div class="input-group-prepend">
        <label class="input-group-text" for="suppliers">Добавить поставщика:</label>
    </div>
    <select id="suppliers" name="supplier_id" class="form-control{{ $errors->has('supplier_id') ? ' is-invalid' : '' }}">
        <option value="0">Выберите поставщика</option>
        @foreach (\App\Supplier::oldest('name')->get(['id', 'name']) as $suppler)
            <option value="{{ $suppler->id }}">{{ $suppler->name }}</option>
        @endforeach
    </select>
    <div class="input-group-append">
        <button class="btn btn-primary btn-sm product-mass-change" data-action="{{ route('admin.products.addSupplier') }}" data-method="post">Добавить</button>
    </div>
</div>