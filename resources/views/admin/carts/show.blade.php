@extends('admin.index')

@section('content')
    <h3>Корзина пользователя {{ $cart->user->name }}</h3>
    @if (count($products))
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th colspan="2" style="width: 60%;">Товар</th>
                        <th style="width: 120px;">Кол-во</th>
                        <th>Цена</th>
                        <th>Стоимость</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($products as $offers)
                        @foreach ($offers as $offer)
                        <tr>
                            @if ($loop->first)
                                <td rowspan="{{ $offers->count()+1 }}">
                                    @if (count($offer->product->images))
                                        <img class="cart-thumbs" src="{{ $offer->product->images->first()->url }}">
                                    @endif
                                    <a href="{{ route('products.show', $offer->product_id) }}">
                                        {{ $offer->product->vendor_code }}
                                    </a>
                                    <p class="hidden-print">
                                        {{ $offer->product->description }}
                                        @if ($offer->product->trashed())
                                            - <span class="small text-danger">Товар удален</span>
                                        @endif
                                    </p>
                                    @if ($offer->product->suppliers->count())
                                        <p class="hidden-print">
                                            @foreach ($offer->product->suppliers as $supplier)
                                                <span class="btn btn-secondary disabled">{{ $supplier->name }}</span>
                                            @endforeach
                                        </p>
                                    @endif
                                </td>
                            @endif
                            <td>{{ $offer->option->name }}</td>
                            <td>
                                <input type="text" class="form-control text-center" value="{{ $offer->pivot->count }}" readonly>
                            </td>
                            <td class="text-nowrap">@money($offer->product->price) &#8381;</td>
                            <td class="text-nowrap">@money($offer->product->price * $offer->pivot->count) &#8381;</td>
                        </tr>
                        @endforeach
                        <tr>
                            <td class="font-italic">Всего по позиции:</td>
                            <td>
                                <input type="text" class="form-control text-center" value="{{ $offers->sum('pivot.count') }}" readonly>
                            </td>
                            <td>&nbsp;</td>
                            <td class="font-italic">@money($offers->sum('pivot.count') * $offer->product->price) &#8381;</td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="2">Всего:</td>
                        <td>
                            <input type="text" class="form-control text-center" value="{{ $totalQty }}" readonly>
                        </td>
                        <td>&nbsp;</td>
                        <td class="text-nowrap">{{ number_format($totalPrice, 2, ',', ' ') }} &#8381;</td>
                    </tr>
                </tbody>
            </table>
        </div>
    @endif

@endsection