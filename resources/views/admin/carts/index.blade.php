@extends('admin.index')

@section('content')
    <h3>Незавершенные заказы</h3>
    
    @if (count($carts))
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th>№ заказа</th>
                        <th>Товаров</th>
                        <th>Пользователь</th>
                        <th>Последнее обновление</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($carts as $cart)
                        <tr>
                            <td><a href="{{ route('admin.carts.show', $cart->id) }}">Корзина №{{ $cart->id }} <br>от&nbsp;@datetime($cart->created_at)</a></td>
                            <td>{{ $cart->offers()->count() }}</td>
                            <td>{{ $cart->user->name }} <div class="text-muted">{{ $cart->user->email }}</div></td>
                            <td>@datetime($cart->updated_at)</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    @else
        <p>Отсутствуют</p>
    @endif

@endsection