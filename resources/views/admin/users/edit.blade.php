@extends('admin.index')

@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <h3>Редактирование пользователя {{ $user->email }}</h3>

    <form action="{{route('admin.users.update', $user->id)}}" method="POST">
        {{ csrf_field() }}
        {{ method_field('PUT') }}

        <div class="form-group">
            <label for="name">ФИО</label>
            <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $user->name ?? old('name') }}">
            @if ($errors->has('name'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group">
            <label for="email">Адрес эл.почты</label>
            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $user->email ?? old('email') }}">
            @if ($errors->has('email'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group">
            <label for="phone">Номер телефона</label>
            <input id="phone" type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ $user->phone ?? old('phone') }}">
            @if ($errors->has('phone'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('phone') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group">
            <label for="city">Город</label>
            <input id="city" type="text" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" value="{{ $user->city ?? old('city') }}">
            @if ($errors->has('city'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('city') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group">
            <label for="company">Компания</label>
            <input id="company" type="text" class="form-control{{ $errors->has('company') ? ' is-invalid' : '' }}" name="company" value="{{ $user->company ?? old('company') }}">
            @if ($errors->has('company'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('company') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group">
            <label for="role_id">Категория</label>
            <select class="form-control" id="role_id" name="role_id">
                <option value="0">Выберите группу</option>
                @foreach ($roles as $rId => $rName)
                    <option value="{{$rId}}" @if ($rId == $user->role_id) selected @endif>{{ $rName }}</option>
                @endforeach
            </select>
            @if ($errors->has('role_id'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('role_id') }}</strong>
                </span>
            @endif
        </div>


        <button type="submit" class="btn btn-primary">Сохранить изменения</button>
    </form>
@endsection