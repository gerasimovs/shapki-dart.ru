@extends('admin.index')

@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <a class="btn btn-sm btn-outline-secondary float-right" href="{{ route('admin.users.invites.create') }}">Пригласить</a>
    <h3>Зарегистрированные пользователи</h3>
    <table class="table">
        <thead class="thead-light">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Имя пользователя</th>
                <th scope="col">E-mail</th>
                <th scope="col">Телефон</th>
                <th scope="col">Дата регистрации</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($users as $user)
                <tr>
                    <th scope="row">{{ $user->id }}</th>
                    <td>
                        <a href="{{ route('admin.users.edit', $user->id) }}">{{ $user->name }}</a><br>
                        <small>{{ $user->role->name ?? 'Доступ ограничен' }}</small>
                        <form action="{{route('admin.users.auth', $user->id)}}" method="POST">
                            {{ csrf_field() }}
                            <button type="submit" class="">Авторизироваться</button>
                        </form>
                    </td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->phone }}</td>
                    <td>@datetime($user->created_at)</td>
                </tr>
            @endforeach
        </tbody>
    </table>    
@endsection
