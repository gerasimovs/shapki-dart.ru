@extends('admin.index')

@section('content')
    <h3>Пригласить пользователя</h3>
    
    <form action="{{ route('admin.users.invites.store') }}" method="POST">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="email">@lang('auth.label.email')</label>
            <input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $role->email ?? old('email') }}" required autofocus>
            @if ($errors->has('email'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>
        <button type="submit" class="btn btn-primary">@lang('auth.button.invite_send')</button>
    </form>
@endsection