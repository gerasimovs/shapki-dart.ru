<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name') }} - @yield('title', 'Головные уборы Новосибирск')</title>
    <link rel="icon" href="{{ asset('favicon.ico') }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link href="{{ asset('css/admin.css') }}?v=20180901.1" rel="stylesheet">
    @stack('styles')
</head>
<body>
<header>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light bg-light" id="navbar-main">
    <div class="container">
        <a class="navbar-brand" href="/">{{ config('app.name', 'Laravel') }}</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('home') }}">О компании</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('catalog') }}">Коллекции</a>
                </li> 
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="helpMenu" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Помощь
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="helpMenu" style="width: 250px;">
                        @foreach (\App\Page::get() as $page)
                            <a class="dropdown-item" href="{{ route('pages.show', $page) }}" style="white-space: normal;">{{ $page->name }}</a>
                        @endforeach
                    </div>
                </li>
                <!-- <li class="nav-item">
                    <a class="nav-link" href="{{ route('contacts') }}#contacts">Контакты</a>
                </li> -->
                @if (Route::has('login'))
                    @auth
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('user.dashboard') }}">Личный кабинет</a>
                        </li>
                        @if (Auth::check()) 
                        <li class="nav-item" @if (!Auth::user()->cart || (Auth::user()->cart && Auth::user()->cart->offers->count()) == 0) style="display: none;" @endif>
                            <a class="btn btn-outline-secondary cart-button" href="{{ route('cart.index') }}">
                                <i class="material-icons">&#xE8CC;</i>  
                                <span class="badge badge-secondary">@if (Auth::user()->cart && Auth::user()->cart->offers->count())
                                    {{ Auth::user()->cart->total_qty }}
                                @endif</span>
                                    
                            </a>
                        </li>
                        @endif
                    @else
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">Вход</a>
                        </li>
                    @endauth
                @endif
            </ul>
        </div>
    </div>
</nav>

@if (Auth::check() && (Auth::user()->id == 1 || Auth::user()->id == 2))
    @include('admin.partials.admin-menu')
@endif

</header>
<main class="container">
    @include('admin.partials.alert-status')
    @yield('content')
</main>
<footer>
    <div class="container">
        <ul class="list-inline">
            <li class="list-inline-item">
                <a href="#">Вверх!</a>
            </li>
            <li class="footer-menu-divider list-inline-item">&sdot;</li>
            <li class="list-inline-item">
                <a href="{{ route('home') }}">О компании</a>
            </li>
            <li class="footer-menu-divider list-inline-item">&sdot;</li>
            <li class="list-inline-item">
                <a href="{{ route('catalog') }}">Коллекции</a>
            </li>
        </ul>
        <p class="copyright text-muted small">Материалы принадлежат &copy; ООО "Дарт", 2017 &mdash; {{date('Y')}}</p>
    </div> <!-- /.container -->
</footer>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js" integrity="sha384-SlE991lGASHoBfWbelyBPLsUlwY1GwNDJo3jSJO04KZ33K2bwfV9YBauFfnzvynJ" crossorigin="anonymous"></script>
<script src="{{ asset('js/index.manage.js') }}"></script>
<script src="{{ asset('js/product.manage.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.4.0/min/dropzone.min.js"></script>
@stack('scripts')
</body>
</html>