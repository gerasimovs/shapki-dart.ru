<form  method="POST">
    @csrf
    <div class="form-group">
        <label for="url">Ссылка на товар</label>
        <input id="url" type="text" class="form-control{{ $errors->has('url') ? ' is-invalid' : '' }}" name="url" value="{{ $parser->url ?? old('url') }}" required autofocus>
        @if ($errors->has('url'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('url') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-check mb-3">
        <label class="form-check-label">
            <input class="form-check-input" type="checkbox" name="with_main" value="1"> Парсить основные фотографии
        </label>
    </div>
    <button type="submit" class="btn btn-primary" formaction="{{ route('admin.parsers.store') }}">Парсить страницу товара</button>
    <button type="submit" class="btn btn-primary" formaction="{{ route('admin.parsers.store.mass') }}">Парсить раздел с товарами</button>
</form>