@extends('admin.index')

@section('content')
    @include('admin.parsers._form')

    <h3>Результаты парсинга</h3>
    @if (count($parsers))
    <div class="table-responsive">
        <table class="table">
            <thead class="thead-light">
                <tr>
                    <th scope="col">Ссылка</th>
                    <th scope="col">Артикул</th>
                    <th scope="col">Название</th>
                    <th scope="col">Описание</th>
                    <th scope="col">Цена</th>
                    <th scope="col">Опции</th>
                    <th scope="col">Изображения</th>
                </tr>
            </thead>
            <tbody>
            @foreach ($parsers as $parser)
                    <tr>
                        <td>{{ $parser->url }}</td>
                        <td>{{ $parser->vendor_code }}</td>
                        <td>{{ $parser->name ?? '' }}</td>
                        <td>{{ $parser->description ?? '' }}</td>
                        <td>{{ $parser->price ?? '' }}</td>
                        <td>
                            @forelse ($parser->options as $option)
                                {{ $option['size'] ?? '' }}
                                {{ $option['color'] }}@if (!$loop->last),@endif
                            @empty
                                <td></td>
                            @endforelse
                        </td>
                        @foreach ($parser->images as $image)
                            <td>
                                {{ $image }}
                            </td>
                        @endforeach
                    </tr>
            @endforeach
            </tbody>
        </table>  
    </div>
    @endif 
@endsection

@push('scripts')
<script>
    document.querySelector('form').addEventListener('submit', function() {
        this.querySelector('button[type="submit"]').innerHTML = '<i class="fas fa-spinner fa-pulse fa-lg"></i> Идёт парсинг';
    });
</script>
@endpush