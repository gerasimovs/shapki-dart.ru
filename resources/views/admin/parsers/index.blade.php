@extends('admin.index')

@section('content')
    <h3>Парсинг товаров</h3>
    
    @include('admin.parsers._form')

    @if (session()->has('parser'))
        <h3Результаты парсинга</h3>
        <table class="table">
            <thead class="thead-light">
                <tr>
                    <th scope="col">Артикул</th>
                    <th scope="col">Название</th>
                    <th scope="col">Описание</th>
                    <th scope="col">Цена</th>
                    <th scope="col">Опции</th>
                    <th scope="col">Изображения</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{ session('parser')->vendor_code }}</td>
                    <td>{{ session('parser')->name }}</td>
                    <td>{{ session('parser')->description }}</td>
                    <td>{{ session('parser')->price }}</td>
                    <td>{{ session('parser')->options->implode(', ') }}</td>
                    <td>{{ session('parser')->images->implode(', ') }}</td>
                </tr>
            </tbody>
        </table>    
    @endif

@endsection

@push('scripts')
<script>
    document.querySelector('form').addEventListener('submit', function() {
        this.querySelector('button[type="submit"]').innerHTML = '<i class="fas fa-spinner fa-pulse fa-lg"></i> Идёт парсинг';
    });
</script>
@endpush