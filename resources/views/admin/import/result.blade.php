@extends('admin.import._')
@section('title', 'Завершение импорта')

@section('sub_content')

@if ($danger)
    <div class="alert alert-danger">
        <ul>
            @foreach ($danger as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if ($success)
    <div class="alert alert-success">
        <ul>
            @foreach ($success as $good)
                <li>{{ $good }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (!empty($products))
    @include('admin.products._partials.list')
@endif


@endsection