@extends('admin.import._')

@section('title', 'Сопоставление полей')

@section('sub_content')
    <form method="POST">
        {{ csrf_field() }}
        <input type="hidden" name="file" value="{{ $file }}">
        <input type="hidden" name="imported" value="{{ $imported }}">
        <div class="form-group row">
            <h3 class="col-12">Обновление данных:</h3>
            <div class="col-sm-6">
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="checkCategory"> Категория
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="checkDescription"> Описание
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="checkPrice"> Цена
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="checkImages"> Изображения
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="checkOptions"> Размеры / цвета
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="checkSuppliers"> Поставщики
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="checkSuppliersReplace"> При добавлении заменять поставщиков
                    </label>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="checkOnlyUpdate"> Не добавлять новые товары
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="checkNotRestore" checked> Не восстанавливать удаленные товары
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="checkDelete"> Удаление
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label">
                        <input class="form-check-input" type="checkbox" name="checkRename"> Переименование
                    </label>
                </div>
            </div>
        </div>

        <h4>Сопоставление полей:</h4>
        @for ($i = 0; $i < count($header); $i++)
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="item-{{ $i }}">{{ $header[$i] }}</label>
                <div class="col-sm-9">
                    <select name="fields[]" id="item-{{ $i }}" class="form-control">
                        <option value="null">-- Выберите поле --</option>
                        @foreach ($fields as $name => $option)
                            <option value="{{ $name }}"
                                @if (!empty($header[$i]) && strpos($option['synonyms'], mb_strtolower($header[$i])) !== false)
                                    selected 
                                @endif
                            >{{ $option['name'] }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        @endfor
        <button type="submit" formaction="{{ route('admin.import.store', $import->id) }}" class="btn btn-primary">Импортировать</button>
        <button type="submit" formaction="{{ route('admin.import.check') }}" class="btn btn-info">Проверить наличие</button>
    </form>
@endsection