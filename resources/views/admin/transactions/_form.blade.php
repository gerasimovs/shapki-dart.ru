<div class="form-group">
    <label for="amount">Сумма транзакции</label>
    <input id="amount" type="number" class="form-control{{ $errors->has('amount') ? ' is-invalid' : '' }}" name="amount" value="{{ $transaction->amount ?? old('amount') }}" required autofocus>
    <small id="amount-help" class="form-text text-muted">Положительное число - пополнение, отрицательное - списание</small>
    @if ($errors->has('amount'))
        <span class="invalid-feedback">
            <strong>{{ $errors->first('amount') }}</strong>
        </span>
    @endif
</div>

<div class="form-group">
    <label for="description">Комментарий</label>
    <input id="description" type="text" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" value="{{ $transaction->description ?? old('description') }}">
    @if ($errors->has('description'))
        <span class="invalid-feedback">
            <strong>{{ $errors->first('description') }}</strong>
        </span>
    @endif
</div>
