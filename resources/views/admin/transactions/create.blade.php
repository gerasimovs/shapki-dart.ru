@extends('admin.index')

@section('content')
    <h3>Добавление транзакции</h3>
    <form action="{{ route('admin.users.transactions.store', $user->id) }}" method="POST">
        @include('admin.transactions._form')
        {{ csrf_field() }}
        <button type="submit" class="btn btn-primary">Добавить транзакцию</button>
    </form>
@endsection
