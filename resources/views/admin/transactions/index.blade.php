@extends('admin.index')

@section('content')
    <div class="media rounded border mb-3">
        <i class="material-icons" style="font-size: 8rem;">&#xE7FD;</i>
        <div class="card-body">
            <h5 class="card-title">{{ $user->name }}</h5>
            <p class="card-text">
                Телефон: {{ $user->phone ?? 'не указан' }}
                <br> 
                E-mail: {{ $user->email ?? 'не указан' }}
            </p>
        </div>
    </div>

    <a class="btn btn-sm btn-outline-secondary float-right" href="{{ route('admin.users.transactions.create', $user->id) }}">Создать</a>
    <h3>Транзакции</h3>

    @if (isset($transactions) && $transactions->count() > 0)
        <div class="table-responsive">
            <table class="table bg-light">
                <thead class="thead-light">
                    <tr>
                        <th>Дата</th>
                        <th>Сумма</th>
                        <th>Статус</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($transactions as $transaction)
                        <tr>
                            <td>@datetime($transaction->created_at)</td>
                            <td class="text-{{ $transaction->amount > 0 ? 'success' : 'danger'}}">
                                {{ abs($transaction->amount) }}
                            </td>
                            <td>
                                {{ $transaction->status }}
                                <div class="small">{{ $transaction->description }}</div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    @else
        <p class="big">Список операций пуст!</p>
    @endif
@endsection