@extends('admin.index')

@section('content')
    <h3>Создание новой группы</h3>
    <form action="{{ route('admin.roles.store') }}" method="POST">
        {{ csrf_field() }}
        @include('admin.roles.partials.fields-form')
        <button type="submit" class="btn btn-primary">Создать группу</button>
    </form>
@endsection