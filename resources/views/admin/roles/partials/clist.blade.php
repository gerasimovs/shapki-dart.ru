@foreach ($categories[$parent] as $category)
    <div class="form-check">
        <label>
            <input type="checkbox" name="categories[{{ $category->id }}][checked]" value="1"
                @if (isset($role) && $role->categories->contains('id', $category->id)) checked @endif
                >
            {{ $category->name }}
        </label>
        <textarea class="form-control" rows="1" name="categories[{{ $category->id }}][description]">@if (isset($role) && $role->categories->contains('id', $category->id)){{ $role->categories()->find($category->id)->pivot->description }}@endif</textarea>
        @if ($category->children_count)
            @include('admin.roles.partials.clist', ['categories' => $categories, 'parent' => $category->id])
        @endif
    </div>
@endforeach