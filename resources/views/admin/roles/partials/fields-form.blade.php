<div class="form-group">
    <label for="name">Название</label>
    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $role->name ?? old('name') }}" required autofocus>
    @if ($errors->has('name'))
        <span class="invalid-feedback">
            <strong>{{ $errors->first('name') }}</strong>
        </span>
    @endif
</div>
<div class="form-group">
    <label for="slug">Код</label>
    <input id="slug" type="text" class="form-control{{ $errors->has('slug') ? ' is-invalid' : '' }}" name="slug" value="{{ $role->slug ?? old('slug') }}" required>
    @if ($errors->has('slug'))
        <span class="invalid-feedback">
            <strong>{{ $errors->first('slug') }}</strong>
        </span>
    @endif
</div>
<div class="form-group">
    <label for="description">Описание</label>
    <input id="description" type="text" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" value="{{ $role->description ?? old('description') }}">
    @if ($errors->has('description'))
        <span class="invalid-feedback">
            <strong>{{ $errors->first('description') }}</strong>
        </span>
    @endif
</div>
<div class="form-group" id="categories_access">
    <p>Управление доступом</p>
    @include('admin.roles.partials.clist', ['categories' => $categoriesGroup, 'parent' => 0])
    <nav class="nav">
        <a href="#select_categories_access" class="nav-link" rel="categories_access">Выбрать все категории</a>
        <a href="#unselect_categories_access" class="nav-link" rel="categories_access">Отменить выбор</a>
    </nav>
</div>

@push('scripts')
    <script type="text/javascript">
    $(document).ready( function() {
        $("a[href='#select_categories_access']").click( function() {
           $("#" + $(this).attr('rel') + " input:checkbox:enabled").attr('checked', true);
            return false;
        });
        $("a[href='#unselect_categories_access']").click( function() {
             $("#" + $(this).attr('rel') + " input:checkbox:enabled").attr('checked', false);
            return false;
        });
    });</script>
@endpush