@extends('admin.index')

@section('content')
    <a class="btn btn-sm btn-outline-secondary float-right" href="{{ route('admin.roles.create') }}">Добавить</a>
    <h3>Группы пользователей</h3>
    <table class="table">
        <thead class="thead-light">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Название</th>
                <th scope="col">Код</th>
                <th scope="col">Описание</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($roles as $role)
                <tr>
                    <th scope="row">{{ $role->id }}</th>
                    <td>
                        <a href="{{ route('admin.roles.edit', $role->id) }}">{{ $role->name }}</a>
                    </td>
                    <td>{{ $role->slug }}</td>
                    <td>{{ $role->description }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>    
@endsection
