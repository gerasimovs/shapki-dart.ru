@extends('admin.index')

@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <h3>Редактирование группы {{ $role->name }}</h3>
    <form action="{{ route('admin.roles.update', $role->id) }}" method="POST">
        {{ csrf_field() }} {{ method_field('PUT') }}
        @include('admin.roles.partials.fields-form')
        <button type="submit" class="btn btn-primary">Сохранить изменения</button>
    </form>
@endsection