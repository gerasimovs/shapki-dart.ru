@foreach ($categories as $category)
    <tr>
        <td>
            <a href="{{ route('admin.categories.up', ['category' => $category->id, 'position' => 'top']) }}">top</a>
            <a href="{{ route('admin.categories.up', $category->id) }}">up</a>
            <a href="{{ route('admin.categories.down', $category->id) }}">down</a>
            <a href="{{ route('admin.categories.down', ['category' => $category->id, 'position' => 'bottom']) }}">bottom</a>
        <td>
            {{$prefix}} 
            <a href="{{ route('admin.categories.edit', $category->id) }}">{{ $category->name ?: 'без названия' }}</a>
            @if ($category->children->count()) <small>- <i>Родительская категория.</i></small> @endif
            <br>
            <small>
                Порядок сортировки: {{ $category->_lft }}.
                @if ($category->access) <i>Доступ ограничен.</i> @endif
                Товаров: {{$category->products->count()}}.
            </small>
        </td>
        <td>
            <form method="POST">
            <a class="btn btn-info" href="{{ route('admin.categories.show', $category->id) }}" data-tooltip="true" title="Просмотреть товары"><i class="material-icons">&#xE8A0;</i></a>

            <div class="btn-group">
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-tooltip="true" title="Сохранить...">
                    <i class="material-icons">&#xE161;</i>
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="{{ route('admin.categories.toWord', $category->id) }}">В формате Word</a>
                    <a class="dropdown-item" href="{{ route('admin.categories.toCsv', $category->id) }}">В формате CSV</a>
                </div>
            </div>
                {{ csrf_field() }}
                @if ($category->trashed())
                    {{ method_field('PUT') }}
                    <button class="btn btn-success" type="submit" formaction="{{ route('admin.categories.recovery', $category) }}"  data-tooltip="true" title="Восстановить"><i class="material-icons">&#xE8BA;</i></button>
                @else
                    {{ method_field('DELETE') }}
                    <button class="btn btn-danger" type="submit" formaction="{{ route('admin.categories.destroy', $category) }}" data-tooltip="true" title="Удалить"><i class="material-icons">&#xE872;</i></button>
                @endif
            </form>
        </td>
    </tr>
    @if ($category->children->count())
        @include('admin.categories._partials.list', ['categories' => $category->children, 'prefix' => $prefix . ' . ', 'level' => $level+1])
    @endif
@endforeach