@if (isset($categories[$item->id]))
    <optgroup label="{{ $item->name }}">
        <option value="{{ $item->id }}" @if (isset($category) && ($item->id == $category->category_id)) selected @endif>-- {{ $item->name }}</option>
        @foreach ($categories[$parent] as $item)
            @include('admin.categories._partials.coption', ['parent' => $item->id])
        @endforeach
    </optgroup> 
@else
    <option value="{{ $item->id }}" @if (isset($category) && ($item->id == $category->category_id)) selected @endif>{{ $item->name }}</option>
@endif