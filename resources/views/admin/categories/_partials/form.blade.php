<div class="form-group">
    <label for="name">Название</label>
    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $category->name ?? old('name') }}">
    @if ($errors->has('name'))
        <span class="invalid-feedback">
            <strong>{{ $errors->first('name') }}</strong>
        </span>
    @endif
</div>

<div class="form-group">
    <label for="description">Описание</label>
    <textarea id="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description">{{ $category->description ?? old('description') }}</textarea>
    @if ($errors->has('description'))
        <span class="invalid-feedback">
            <strong>{{ $errors->first('description') }}</strong>
        </span>
    @endif
</div>

<div class="form-group">
    <label>Родительская категория</label>
    <select name="category_id" class="form-control{{ $errors->has('category_id') ? ' is-invalid' : '' }}">
        <option value="">Выберите категорию</option>
        @foreach ($categories[''] as $item)
            @include('admin.categories._partials.coption', ['parent' => $item->id])
        @endforeach
    </select>
</div>

<div class="form-group">
    <label for="status">Цвет категории</label>
    <select class="form-control{{ $errors->has('status_id') ? ' is-invalid' : '' }}" id="input-status" name="status_id" required>
        @foreach ($category->getStatuses() as $statusId => $status)
            <option value="{{ $statusId }}" @if ($category->status_id == $statusId) selected @endif>@lang('bootstrap.text.' . $status)</option>
        @endforeach 
    </select>
    @if ($errors->has('status_id'))
        <span class="invalid-feedback">
            <strong>{{ $errors->first('status_id') }}</strong>
        </span>
    @endif
</div>
