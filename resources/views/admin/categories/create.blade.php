@extends('admin.index')

@section('content')
    <h3>Создание категории</h3>
    <form action="{{ route('admin.categories.store') }}" method="POST">
        @include('admin.categories._partials.form')
        {{ csrf_field() }} 
        <button type="submit" class="btn btn-primary">Сохранить изменения</button>
    </form>
@endsection