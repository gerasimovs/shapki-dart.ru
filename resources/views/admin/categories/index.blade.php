@extends('admin.index')

@section('content')
    <a class="btn btn-sm btn-outline-secondary float-right" href="{{ route('admin.categories.create') }}">Добавить</a>
    <h3>Все категории</h3>

    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.categories.index') }}">Активные</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('admin.categories.index') }}?delete=true">Архивные</a>
        </li>
    </ul>

    <table class="table">
        <thead class="thead-light">
            <tr>
                <th>#</th>
                <th>Название категории</th>
                <th>Действия</th>
            </tr>
        </thead>
        <tbody>
            @include('admin.categories._partials.list', ['categories' => $categories, 'prefix' => '', 'level' => 1])
        </tbody>
    </table>
@endsection