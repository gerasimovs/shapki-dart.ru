@extends('admin.index')

@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif

    <form action="{{ route('admin.categories.destroy', $category->id) }}" method="POST" class="float-right">
        {{ csrf_field() }}
        {{ method_field('DELETE') }}
        <button class="btn btn-sm btn-outline-secondary " type="submit"><i class="material-icons">&#xE872;</i> Удалить</button>
    </form>
    <h3>Редактирование категории <a href="{{ route('catalog.show', $category->id) }}">{{ $category->name }}</a></h3>
    <form action="{{ route('admin.categories.update', $category->id) }}" method="POST">
        @include('admin.categories._partials.form')
        {{ csrf_field() }} 
        {{ method_field('PUT') }}
        <button type="submit" class="btn btn-primary">Сохранить изменения</button>
    </form>
@endsection