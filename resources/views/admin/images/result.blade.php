@extends('admin.index')

@section('content')

    @if (count($images))
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th>Изображение</th>
                        <th>Загружаемый файл</th>
                        <th>Ссылка на загруженное изображение</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($images as $image)
                        <tr>
                            <td><img src="{{ $image->url }}" style="width: 5rem;"></td>
                            <td>{{ $image->upload_url }}</td>
                            <td>
                                <button class="btn btn-light copy-url" data-toggle="tooltip" data-placement="bottom" title="Копировать в буфер обмена">
                                    <i class="material-icons">&#xE14D;</i>
                                </button> 
                                <span class="image-url">{{ $image->url }}</span> 
                                @if ($image->products->count())
                                    <hr>
                                    @foreach ($image->products as $product)
                                        <a class="btn btn-secondary mr-2" href="{{ route('admin.products.edit', $product) }}">
                                        @if ($product->trashed()) <s> @endif
                                            {{ $product->vendor_code }}
                                        @if ($product->trashed()) </s> @endif
                                        </a>
                                    @endforeach
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    @else
        <p>Загруженные файлы отсутствуют</p>
    @endif

@endsection

@push('scripts')
    <script>
        let copyUrlBtns = document.querySelectorAll('.copy-url');  
        copyUrlBtns.forEach(function(entry) {
            entry.addEventListener('click', function(event) {  
                window.getSelection().removeAllRanges();
                
                let imageLink = this.nextElementSibling;  
                let range = document.createRange();  
                range.selectNode(imageLink);  
                window.getSelection().addRange(range);  

                try {  
                    let successful = document.execCommand('copy');  
                    let msg = successful ? 'successful' : 'unsuccessful';  
                    console.log('Copy url command was ' + msg + ': ' + range);  
                } catch(err) {  
                    console.log('Oops, unable to copy');  
                }  

                window.getSelection().removeAllRanges();  
            });
        });

        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
@endpush