@extends('admin.index')

@section('content')
    <a class="btn btn-sm btn-outline-secondary float-right ml-2" href="{{ route('admin.images.uploads') }}">Массовая загрузка</a>

    <h3>Изображения</h3>
    
    @if ($images->count())
        @include('partials.pagination', ['items' => $images, 'appends' => ['used' => Request::input('used'), 'table' => Request::input('table')]])
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link" href="{{ \URL::current() }}">Все</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ \URL::current() }}?used=false">Не используемые</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ \URL::current() }}?table=true">Таблицей</a>
            </li>
        </ul>
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th>Исходный файл</th>
                        <th>Ссылка</th>
                        <th>Дата загрузки</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($images as $image)
                        <tr>
                            <td>{{ $image->upload_url }}</td>
                            <td>{{ $image->url }}</td>
                            <td>@datetime($image->created_at)</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @include('partials.pagination', ['items' => $images])
    @else
        <p>Отсутствуют</p>
    @endif

@endsection

@push('scripts')
    <script>
        let copyUrlBtns = document.querySelectorAll('.copy-url');  
        copyUrlBtns.forEach(function(entry) {
            entry.addEventListener('click', function(event) {  
                window.getSelection().removeAllRanges();
                
                let imageLink = this.nextElementSibling;  
                let range = document.createRange();  
                range.selectNode(imageLink);  
                window.getSelection().addRange(range);  

                try {  
                    let successful = document.execCommand('copy');  
                    let msg = successful ? 'successful' : 'unsuccessful';  
                    console.log('Copy url command was ' + msg + ': ' + range);  
                } catch(err) {  
                    console.log('Oops, unable to copy');  
                }  

                window.getSelection().removeAllRanges();  
            });
        });

        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
@endpush