@extends('admin.index')

@section('content')
    <h3>Массовая загрузка изображений</h3>

    <form class="form-inline" method="POST" action="{{ route('admin.images.stores') }}" enctype="multipart/form-data" id="upload-form">
        <div class="form-group">
            <label for="images">Выберите изображения:</label>
            <input type="file" class="form-control-file" name="images[]" id="images" accept="image/*" required multiple>
        </div>
        @if ($errors->has('images'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('images') }}</strong>
            </span>
        @endif
        <button type="submit" class="btn btn-primary btn-sm">Загрузить</button>
        {{ csrf_field() }} 
    </form>

    <div class="images-container" id="upload-dropzone">Просто перетащите файлы сюда</div>
    <!-- Progress Bar -->
    <div class="progress" id="upload-progress">
        <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
            <span></span>
            <span class="sr-only">0% Complete</span>
        </div>
    </div>
    <!-- Upload Finished -->
    <div class="upload-finished list-group" id="upload-previews">
        <p class="upload-finished-title h5">Загружаемые файлы:</p>
    </div>

    <div id="template">
        <div class="d-flex justify-content-between mb-1">
            <!-- This is used as the file preview template -->
            <div>
                <span class="preview"><img data-dz-thumbnail></span>
            </div>
            <div>
                <p class="name" data-dz-name></p>
                <strong class="error text-danger" data-dz-errormessage></strong>
            </div>
            <div>
                <p class="size" data-dz-size></p>
                <div class="progress progress-striped active" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="0">
                    <div class="progress-bar progress-bar-success" style="width:0%;" data-dz-uploadprogress></div>
                </div>
            </div>
            <div>
                <button class="btn btn-primary start">
                    <i class="fas fa-upload"></i>
                    <span>Start</span>
                </button>
                <button data-dz-remove class="btn btn-warning cancel">
                    <i class="fas fa-ban"></i>
                    <span>Cancel</span>
                </button>
                <button data-dz-remove class="btn btn-danger delete">
                    <i class="fas fa-trash"></i>
                    <span>Delete</span>
                </button>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('js/images.manage.js') }}"></script>
@endpush