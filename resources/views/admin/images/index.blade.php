@extends('admin.index')

@section('content')
    <a class="btn btn-sm btn-outline-secondary float-right ml-2" href="{{ route('admin.images.uploads') }}">Массовая загрузка</a>

    <h3>Изображения</h3>
    
    @if ($images->count())
        @include('partials.pagination', ['items' => $images, 'appends' => ['used' => Request::input('used')]])
        <ul class="nav">
            <li class="nav-item">
                <a class="nav-link" href="{{ \URL::current() }}">Все</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ \URL::current() }}?used=false">Не используемые</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="{{ \URL::current() }}?table=true">Таблицей</a>
            </li>
        </ul>
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th>Изображение</th>
                        <th>Ссылка</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($images as $image)
                        <tr>
                            <td><img src="{{ $image->url }}" title="Загружено: {{ $image->upload_url }}" style="width: 15rem;" data-tooltip="true"></td>
                            <td>
                                <form class="d-inline form-delete" action="{{ route('admin.images.destroy', $image) }}" method="post">
                                    {{ csrf_field() }} {{ method_field('DELETE') }}
                                    <button class="btn btn-danger" type="submit" data-tooltip="true" title="Удалить">
                                        <i class="material-icons">&#xE872;</i>
                                    </button>
                                </form>
                                <button class="btn btn-light copy-url" data-tooltip="true" title="Копировать в буфер обмена">
                                    <i class="material-icons">&#xE14D;</i>
                                </button> 
                                <span class="image-url">{{ $image->url }}</span> 
                                <hr>
                                @if ($image->products->count())
                                    <p class="h6">Используется в товарах:</p>
                                    @foreach ($image->products as $product)
                                        <a class="btn btn-secondary mr-2" href="{{ route('admin.products.edit', $product) }}">
                                        @if ($product->trashed()) <s> @endif
                                            {{ $product->vendor_code }}
                                        @if ($product->trashed()) </s> @endif
                                        </a>
                                    @endforeach
                                @else
                                    <div class="alert alert-warning">Не используется</div>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @include('partials.pagination', ['items' => $images])
    @else
        <p>Отсутствуют</p>
    @endif

@endsection