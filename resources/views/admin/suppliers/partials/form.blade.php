<div class="form-group">
    <label for="name">Поставщик</label>
    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $supplier->name ?? old('name') }}" required autofocus>
    @if ($errors->has('name'))
        <span class="invalid-feedback">
            <strong>{{ $errors->first('name') }}</strong>
        </span>
    @endif
</div>

<div class="form-group">
    <label for="phone">Телефон</label>
    <input id="phone" type="tel" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ $supplier->phone ?? old('phone') }}">
    @if ($errors->has('phone'))
        <span class="invalid-feedback">
            <strong>{{ $errors->first('phone') }}</strong>
        </span>
    @endif
</div>

<div class="form-group">
    <label for="address">Местонахождение</label>
    <input id="address" type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" value="{{ $supplier->address ?? old('address') }}">
    @if ($errors->has('address'))
        <span class="invalid-feedback">
            <strong>{{ $errors->first('address') }}</strong>
        </span>
    @endif
</div>