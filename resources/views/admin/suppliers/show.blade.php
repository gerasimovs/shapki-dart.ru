@extends('admin.index')

@section('content')
    <div class="media rounded border mb-3">
        <i class="material-icons" style="font-size: 8rem;">&#xE7FD;</i>
        <div class="card-body">
            <h5 class="card-title">{{ $supplier->name }}</h5>
            <p class="card-text">
                Телефон: {{ $supplier->phone ?? 'не указан' }}
                <br> 
                Адрес: {{ $supplier->address ?? 'не указан' }}
            </p>
        </div>
    </div>
    @include('admin.products._partials.list')
@endsection
