@extends('admin.index')

@section('content')
    <h3>Добавление поставщика</h3>
    <form action="{{ route('admin.suppliers.store') }}" method="POST">
        @include('admin.suppliers.partials.form')
        {{ csrf_field() }}
        <button type="submit" class="btn btn-primary">Добавить поставщика</button>
    </form>
@endsection