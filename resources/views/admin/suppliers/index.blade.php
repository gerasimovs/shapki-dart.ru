@extends('admin.index')

@section('content')
    <a class="btn btn-sm btn-outline-secondary float-right" href="{{ route('admin.suppliers.create') }}">Создать</a>
    <h3>Поставщики</h3>
    <table class="table">
        <thead class="thead-light">
            <tr>
                <th scope="col">Поставщик</th>
                <th scope="col">Телефон</th>
                <th scope="col">Адрес</th>
                <th scope="col">#</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($suppliers as $supplier)
                <tr>
                    <td>
                        <a href="{{ route('admin.suppliers.edit', $supplier->id) }}">{{ $supplier->name }}</a><br>
                    </td>
                    <td>{{ $supplier->phone }}</td>
                    <td>{{ $supplier->address }}</td>
                    <td><a class="btn btn-info" href="{{ route('admin.suppliers.show', $supplier->id) }}" data-tooltip="true" title="Просмотреть"><i class="material-icons">&#xE8A0;</i></a></td>
                </tr>
            @endforeach
        </tbody>
    </table>    
@endsection
