@extends('admin.index')

@section('content')
    <h3>Редактирование поставщика <a href="{{ route('admin.suppliers.show', $supplier->id) }}">{{ $supplier->name }}</a></h3>
    <form action="{{ route('admin.suppliers.update', $supplier->id) }}" method="POST">
        @include('admin.suppliers.partials.form')
        {{ csrf_field() }} 
        {{ method_field('PUT') }}
        <button type="submit" class="btn btn-primary">Сохранить изменения</button>
    </form>
@endsection