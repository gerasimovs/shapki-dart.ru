@extends('admin.index')

@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    
    <h3>Пригласшение пользователя</h3>
    
    <form action="{{ route('admin.users.invites.store') }}" method="POST">
        {{ csrf_field() }}
        <div class="form-group">
            <label for="email">@lang('auth.label.email')</label>
            <input id="email" type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $role->email ?? old('email') }}" required autofocus>
            @if ($errors->has('email'))
                <span class="invalid-feedback">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group">
            <label for="role_id">Категория</label>
            <select class="form-control" id="role_id" name="role_id">
                <option>Выберите группу</option>
                @foreach ($roles as $rId => $rName)
                    <option value="{{$rId}}" @if ($rId == old('role_id')) selected @endif>{{ $rName }}</option>
                @endforeach
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Создать приглашение</button>
    </form>
@endsection