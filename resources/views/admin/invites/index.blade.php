@extends('admin.index')

@section('content')
    <a class="btn btn-sm btn-outline-secondary float-right" href="{{ route('admin.users.invites.create') }}">Пригласить</a>
    <h3>Приглашенные пользователи</h3>
    <table class="table">
        <thead class="thead-light">
            <tr>
                <th scope="col">E-mail</th>
                <th scope="col">Группа пользователя</th>
                <th scope="col">Кто пригласил</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($invites as $invite)
                <tr>
                    <td><a href="{{ route('admin.users.invites.edit', $invite->id) }}">{{ $invite->email }}</a></td>
                    <td>{{ $invite->role->name ?? 'Не выбрана' }}</td>
                    <td>{{ $invite->user->name }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>    
@endsection
