@extends('admin.index')

@section('content')
    <h3>Подготовка <a href="{{ route('admin.mailings.show', $mailing) }}">рассылки №{{ $mailing->id }} от&nbsp;@datetime($mailing->created_at)</a> к отправке</h3>
    <form method="POST" id="app">
    	<input type="hidden" value="users" name="entity">
    	<input type="hidden" value="mailings" name="type">
    	<input type="hidden" value="{{ $mailing->id }}" name="type_id">
	    <div class="form-group">
	        <label>Имя или адрес эл.почты пользователя:</label>
	        <input type="text" class="form-control" v-model.lazy="userdata" v-debounce="500" placeholder="Найти пользователей...">
	    </div>
	    <div class="mb-3">
	        <div class="chip" v-for="entity in entities" :key="entity.id">
	            <span class="closebtn" @click="detach(entity.id)">&times;</span>
	            <span v-html="entity.name"></span>
	        </div>
	        <div class="chip" v-for="result in results" :key="result.id">
	            <span class="closebtn" @click="attach(result.id)">+</span>
	            <span v-html="highlight(result.name)"></span>
	        </div>
	    </div>
    	</>
        {{ csrf_field() }}
        <button type="submit" formaction="{{ route('admin.mailings.send', $mailing->id) }}" class="btn btn-primary">Отправить</button>
    </form>
@endsection

@push('scripts')
    <script src="{{ asset('js/vue.js') }}"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="{{ asset('js/mailings.manage.js') }}"></script>
@endpush