@extends('admin.index')

@section('content')
    <h3>Редактирование <a href="{{ route('admin.mailings.show', $mailing->id) }}">рассылки №{{ $mailing->id }} от&nbsp;@datetime($mailing->created_at)</a></h3>
    <form method="POST">
        @include('admin.mailings.partials.form')
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <button type="submit" formaction="{{ route('admin.mailings.update', $mailing->id) }}" class="btn btn-primary">Сохранить изменения</button>
    </form>
@endsection