@component('components.forms.input', ['value' => $mailing->subject ?? '', 'name' => 'subject'])
    Тема письма
@endcomponent

@component('components.forms.input', ['value' => $mailing->greeting ?? '', 'name' => 'greeting'])
    Заголовок
@endcomponent

@component('components.forms.textarea', ['value' => $mailing->message ?? '', 'name' => 'message'])
    Сообщение
@endcomponent

<div id="app" data-mailing="{{ $mailing->id ?? 0 }}">

    <div class="form-group">
        <label>Прикрепить товары:</label>
        <input type="text" class="form-control" v-model.lazy="keywords" v-debounce="500" placeholder="Найти товары...">
    </div>

    <div class="mb-3" v-if="results">
        <div class="chip" v-for="result in results" :key="result.id">
            <span class="closebtn" @click="insertInMessage(result.id)">+</span>
            <span v-html="highlight(result.title)"></span>
        </div>
    </div>

</div>

@component('components.forms.textarea', ['value' => $mailing->salutation ?? '', 'name' => 'salutation'])
    Подпись
@endcomponent

@push('styles')
    <link href="{{ asset('css/summernote.css') }}" rel="stylesheet">
@endpush

@push('scripts')
    <script src="{{ asset('js/vue.js') }}"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script src="{{ asset('js/mailings.manage.js') }}"></script>
    <script src="{{ asset('js/summernote.js') }}"></script>
@endpush