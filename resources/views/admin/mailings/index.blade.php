@extends('admin.index')

@section('content')
    <a class="btn btn-sm btn-outline-secondary float-right ml-2" href="{{ route('admin.mailings.create') }}">Создать</a>

    <h3>Рассылки</h3>
    
    @if ($mailings->count())
        @include('partials.pagination', ['items' => $mailings])
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th>Рассылка</th>
                        <th>Тема рассылки</th>
                        <th>#</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($mailings as $mailing)
                        <tr>
                            <td><a href="{{ route('admin.mailings.edit', $mailing) }}">Рассылка №{{ $mailing->id }} <br>от&nbsp;@datetime($mailing->created_at)</a></td>
                            <td>{{ $mailing->subject }}</td>
                            <td><a href="{{ route('admin.mailings.prepare', $mailing) }}">Отправить</a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @include('partials.pagination', ['items' => $mailings])
    @else
        <p>Отсутствуют</p>
    @endif

@endsection