@extends('admin.index')

@section('content')
    <h3>Создание рассылки</h3>
    <form method="POST">
        @include('admin.mailings.partials.form')
        {{ csrf_field() }}
        <button type="submit" formaction="{{ route('admin.mailings.preview') }}" formtarget="_blank" class="btn btn-primary">Предпросмотр</button>
        <button type="submit" formaction="{{ route('admin.mailings.store') }}" class="btn btn-primary">Создать рассылку</button>
    </form>
@endsection