@extends('admin.index')

@section('content')
    <h3>Редактирование страницы <a href="{{ route('pages.show', $page) }}">{{ $page->name }}</a></h3>
    <form method="POST">
        @include('admin.pages.partials.form')
        {{ csrf_field() }}
        {{ method_field('PUT') }}
        <button type="submit" formaction="{{ route('admin.pages.update', $page) }}" class="btn btn-primary">Сохранить изменения</button>
    </form>
@endsection