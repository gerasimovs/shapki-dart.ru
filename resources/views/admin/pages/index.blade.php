@extends('admin.index')

@section('content')
    <a class="btn btn-sm btn-outline-secondary float-right ml-2" href="{{ route('admin.pages.create') }}">Создать</a>

    <h3>Страницы</h3>

    @if (!empty($pages))
        @include('partials.pagination', ['items' => $pages])
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-light">
                    <tr>
                        <th>Заголовок страницы</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($pages as $page)
                        <tr>
                            <td><a href="{{ route('admin.pages.edit', $page) }}">{{ $page->name }}</a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        @include('partials.pagination', ['items' => $pages])
    @else
        <p>Отсутствуют</p>
    @endif

@endsection