@extends('admin.index')

@section('content')
    <h3>Создание страницы</h3>
    <form method="POST">
        @include('admin.pages.partials.form')
        {{ csrf_field() }}
        <button type="submit" formaction="{{ route('admin.pages.store') }}" class="btn btn-primary">Создать страницу</button>
    </form>
@endsection