@component('components.forms.input', ['value' => $page->name ?? '', 'name' => 'name'])
    Заголовок
@endcomponent

@component('components.forms.textarea', ['value' => $page->content ?? '', 'name' => 'content', 'markdown' => true])
    Сообщение
@endcomponent

