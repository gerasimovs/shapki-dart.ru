<nav class="nav small">
    <div class="container d-flex flex-wrap">
        <span class="nav-link">Админка:</span>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="productsManage" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Товары
            </a>
            <div class="dropdown-menu" aria-labelledby="productsManage">
                <a class="dropdown-item" href="{{ route('admin.products.index') }}">Товары</a>
                <a class="dropdown-item" href="{{ route('admin.categories.index') }}">Категории</a>
                <a class="dropdown-item" href="{{ route('admin.suppliers.index') }}">Поставщики</a>
                <a class="dropdown-item" href="{{ route('admin.images.index') }}">Изображения</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{ route('admin.import.index') }}">Импорт товаров</a>
                <a class="dropdown-item" href="{{ route('admin.parsers.index') }}">Парсинг товаров</a>
                <a class="dropdown-item" href="{{ route('admin.helpers.index') }}">Помощники</a>
            </div>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="cartsManage" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Заказы
            </a>
            <div class="dropdown-menu" aria-labelledby="cartsManage">
                <a class="dropdown-item" href="{{ route('admin.orders.index') }}">Последние заказы</a>
                <a class="dropdown-item" href="{{ route('admin.carts.index') }}">Незавершенные заказы</a>
            </div>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="productsManage" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Пользователи
            </a>
            <div class="dropdown-menu" aria-labelledby="productsManage">
                <a class="dropdown-item" href="{{ route('admin.users.index') }}">Зарегистрированные пользователи</a>
                <a class="dropdown-item" href="{{ route('admin.users.invites.index') }}">Приглашенные пользователи</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="{{ route('admin.roles.index') }}">Группы пользователей</a>
            </div>
        </li>
        <li class="nav-item"><a class="nav-link" href="{{ route('admin.mailings.index') }}">Рассылки</a></li>
        <li class="nav-item"><a class="nav-link" href="{{ route('admin.pages.index') }}">Страницы</a></li>

    </div>
</nav>