<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->nullable();
            $table->unsignedInteger('totalQty')->nullable();
            $table->float('totalPrice')->nullable();
            $table->enum('status', [
                'Processing', 
                'Problem', 
                'PaymentDue', 
                'PickupAvailable', 
                'InTransit', 
                'Delivered', 
                'Cancelled', 
                'Returned'
            ]);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('offer_order', function (Blueprint $table) {
            $table->unsignedInteger('order_id');
            $table->foreign('order_id')->references('id')->on('orders');
            $table->unsignedInteger('offer_id');
            $table->foreign('offer_id')->references('id')->on('offers');
            $table->unsignedInteger('product_id');
            $table->string('vendor_code');
            $table->float('price');
            $table->string('color')->nullable();
            $table->string('size')->nullable();
            $table->unsignedInteger('qty');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_product');
        Schema::dropIfExists('orders');
    }
}
