<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('vendor_code')->unique();
            $table->string('name')->nullable();
            $table->float('price')->nullable();
            $table->text('description')->nullable();
            $table->boolean('available')->default(1); // TODO: Как использовать доступность товара?
            $table->unsignedInteger('category_id')->nullable()->default(null);
            $table->softDeletes();
            $table->timestamps();
        });

        DB::statement('ALTER TABLE products ADD FULLTEXT products(vendor_code, description)');

        Schema::create('product_product', function (Blueprint $table) {
            $table->unsignedInteger('product_id');
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->unsignedInteger('related_id');
            $table->foreign('related_id')->references('id')->on('products')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_product');
        Schema::table('products', function(Blueprint $table) {
            $table->dropIndex('products');
        });
        Schema::dropIfExists('products');
    }
}
