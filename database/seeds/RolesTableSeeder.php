<?php

use App\Permission;
use App\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            ['id' => 1, 'name' => 'Administrator', 'slug' => 'admin'],
            ['id' => 2, 'name' => 'Simple user', 'slug' => 'user'],
        ];

        foreach ($roles as $role) {
            Role::query()->create($role);
        }

        $permissions = [
            ['id' => 1,  'name' => 'user_management_access',],
            ['id' => 2,  'name' => 'user_management_create',],
            ['id' => 3,  'name' => 'user_management_edit',],
            ['id' => 4,  'name' => 'user_management_view',],
            ['id' => 5,  'name' => 'user_management_delete',],
            ['id' => 6,  'name' => 'permission_access',],
            ['id' => 7,  'name' => 'permission_create',],
            ['id' => 8,  'name' => 'permission_edit',],
            ['id' => 9,  'name' => 'permission_view',],
            ['id' => 10, 'name' => 'permission_delete',],
            ['id' => 11, 'name' => 'role_access',],
            ['id' => 12, 'name' => 'role_create',],
            ['id' => 13, 'name' => 'role_edit',],
            ['id' => 14, 'name' => 'role_view',],
            ['id' => 15, 'name' => 'role_delete',],
            ['id' => 16, 'name' => 'user_access',],
            ['id' => 17, 'name' => 'user_create',],
            ['id' => 18, 'name' => 'user_edit',],
            ['id' => 19, 'name' => 'user_view',],
            ['id' => 20, 'name' => 'user_delete',],
            ['id' => 21, 'name' => 'invitation_access',],
            ['id' => 22, 'name' => 'invitation_create',],
            ['id' => 23, 'name' => 'invitation_edit',],
            ['id' => 24, 'name' => 'invitation_view',],
            ['id' => 25, 'name' => 'invitation_delete',],
         // ['id' => 26, 'name' => 'event_access',],
         // ['id' => 27, 'name' => 'event_create',],
         // ['id' => 28, 'name' => 'event_edit',],
         // ['id' => 29, 'name' => 'event_view',],
         // ['id' => 30, 'name' => 'event_delete',],
        ];

        foreach ($permissions as $permission) {
            Permission::query()->create($permission);
        }

        $relationships = [
            1 => [
                'permission' => [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, /*26, 27, 28, 29, 30*/],
            ],
            2 => [
                'permission' => [21, 22, 23, 24, 25, /*26, 27, 28, 29, 30*/],
            ],
        ];

        foreach ($relationships as $role => $permissions) {
            $role = Role::query()->find($role);
            foreach ($permissions as $permission => $ids) {
                $role->{$permission}()->sync($ids);
            }
        }
    }
}
