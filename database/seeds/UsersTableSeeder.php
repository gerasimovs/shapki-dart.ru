<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$users = [
            [
            	'id' => 1, 
            	'name' => 'Sergey', 
            	'email' => 'mrteos@gmail.com', 
            	'password' => bcrypt('secret'),
            	'remember_token' => '',
                'role_id' => 1,
            ],
            [
                'id' => 2, 
                'name' => 'Natalia', 
                'email' => 'volkova_od@mail.ru', 
                'password' => bcrypt('secret'),
                'remember_token' => '',
                'role_id' => 2,
            ],
        ];

        foreach ($users as $user) {
            User::query()->create($user);
        }
    }
}
