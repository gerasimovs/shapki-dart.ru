<?php

namespace App\Events;

use App\Transaction;
use Illuminate\Queue\SerializesModels;

class TransactionCreated
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Transaction $transaction)
    {
        $transaction->account->recalculate();
    }

}
