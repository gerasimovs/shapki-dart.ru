<?php

namespace App\Events;

use App\Product;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Carbon;

class ProductCreating
{
    use SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Product $product)
    {
        $product->sorted_at = new Carbon;
    }

}
