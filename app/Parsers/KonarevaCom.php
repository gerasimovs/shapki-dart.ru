<?php

namespace App\Parsers;

use App\Parser;

class KonarevaCom extends Parser
{
    protected function getName()
    {
        $name = $this->content
            ->query(".//div[@class='product-info']/descendant::h1[1]");

        if ($name->length == 0) {
            return false;
        }

        $this->name = trim($name->item(0)->textContent);
    }

    protected function getVendorCode()
    {
        $vendorCode = $this->content
            ->query('.//meta[@itemprop="name"]/@content');

        if ($vendorCode->length == 0) {
            return false;
        }

        $this->vendor_code = trim($vendorCode->item(0)->textContent);
    }

    protected function getDescription()
    {
        $description = $this->content
            ->query('.//meta[@itemprop="description"]/@content');

        $featuresBlock = $this->content
            ->query('.//div[@class="ty-product-features-list"]/div');

        $features = array();
        foreach ($featuresBlock as $key => $value) {
            $features[] = collect($value->childNodes)->implode('textContent', ' ');
        }
        $features = $features ? implode('. ', $features) . '. ' : '';

        $this->description =  $features . $description->item(0)->textContent;
    }

    protected function getPrice()
    {
        $price = $this->content
            ->query('.//meta[@itemprop="price"]/@content');

        if ($price->length == 0) {
            return false;
        }

        $this->price = trim($price->item(0)->textContent);
    }

    protected function getOptions()
    {
        $this->options = collect(
            $this->content
                ->query('.//div[@class="option-image-block"]')
        )->map(function ($color) {
            if (stripos($color->textContent, 'декор')
                || stripos($color->textContent, 'вид сбоку')
                || stripos($color->textContent, 'вид сзади')) {
                return;
            }
            return ['color' => trim($color->textContent)];
        })->reject(function ($name) {
            return empty($name);
        });
    }

    protected function getImages()
    {
        $this->images = collect(
            $this->content
                ->query('.//div[contains(@class, "ty-product-img")]//a[contains(@class, "cm-image-previewer")]/@href')
        )->map(function ($node) {
            return $node->value;
        });
    }

    protected function getCards()
    {
        $this->cards = collect(
            $this->content
                ->query('id("pagination_contents")//a[@class="product-title"]/@href')
        )->map(function ($node) {
            return ['url' => $node->value];
        });
    }
}
