<?php

namespace App\Parsers;

use App\Parser as Model;

class Authentication extends Model
{
    public function __construct($content, $host)
    {
        $this->host = $host;
        $this->content = $content;
        $this->params = config('parser')[$host];
        if ($this->params['auth'] && $this->checkAuth()) {
            $this->authSite();
        }
    }

    private function checkAuth()
    {
        switch ($this->host) {
            case 'shapki-nsk.ru':
            case 'kupalniki-nsk.ru':
                return preg_match(
                    '/<a[^>]*?data-target=\"#loginModal\".*?>.*?<\/a>/s', 
                    $this->content
                );
                break;
            
            case 'www.100sp.ru':
                $dom = new \DOMDocument('1.0', 'UTF-8');
                @$dom->loadHTML(mb_convert_encoding($this->content, 'HTML-ENTITIES', 'UTF-8'));
                $this->content = new \DOMXPath($dom);

                return $this->content
                    ->query('id("user-menu")/*[@class="signin"]')
                    ->length;
                break;
            case 'avili-style.ru':
                return preg_match(
                    '/<div[^>]*?class=\"login-form\".*?>.*?<\/div>/s', 
                    $this->content
                );
                break;
            case 'nskshapki.ru':
                return strpos($this->content, '<a href="/user/login">Войдите</a>');
                break;
        }
    }

    private function authSite()
    {
        $this->url = $this->params['url'];
        $postData = $this->params['data'];
        $params = [
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => http_build_query($postData),
        ];

        $this->getContent($params);
    }
}
