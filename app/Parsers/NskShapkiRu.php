<?php

namespace App\Parsers;

class NskShapkiRu extends \App\Parser
{
    protected function getName()
    {
        $name = $this->content
            ->query('.//div[contains(./@class,"see-price-block")]/div[contains(./@class,"see-price")]/following-sibling::p');

        if ($name->length == 0) {
            $this->price = false;
            return false;
        }

        return $this->name = $name->item(0)->textContent;
    }

    public function getVendorCode()
    {
        $vendorCode = $this->content
            ->query(".//h1");

        if ($vendorCode->length == 0) {
            return false;
        }

        $this->vendor_code = trim($vendorCode->item(0)->textContent);
        return $this->vendor_code;
    }

    public function getDescription()
    {
        $features = [];
        $this->description = collect(
            $this->content
                ->query('.//div[contains(./@class,"param")]/table//tr')
        )->map(function ($node) {
            return trim($node->textContent, " \t\n\r\0\x0B\xC2\xA0");
        })->reject(function ($item) {
            return empty($item);
        })->implode('. ');

        return $this->description;
    }

    public function getPrice()
    {
        $price = $this->content
            ->query('.//div[contains(./@class,"see-price")]//li[contains(./@class,"price")]');

        if ($price->length == 0) {
            $this->price = false;
            return false;
        }

        preg_match('#([\s\d]+)#', $price->item(0)->textContent, $matches);
        $this->price = trim($matches[1] ?? 0);
    }

    public function getOptions()
    {
        $this->options = collect(
            $this->content->query(
                'id("color-select")//option'
            )
        )->map(function ($node) {
            return [
                'color' => trim($node->textContent)
            ];
        });

        return $this->options;
    }

    public function getImages()
    {
        $this->images = collect($this->content->query(
            'id("gallery_01")//a/@data-zoom-image'
        ))->map(function ($node) {
            return $this->url->component['scheme'] . '://' . $this->host . $node->textContent;
        });

        return $this->images;
    }

    public function getCards()
    {
        $urls = $this->content
            ->query('.//a[@class="product"]/@href');

        if ($urls->length == 0) {
            return false;
        }

        $this->cards = collect($urls)
            ->map(function ($item, $key) {
                return ['url' => $this->url->component['scheme'] . '://' . $this->host . $item->value];
            });
    }
}
