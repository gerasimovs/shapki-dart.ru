<?php

namespace App\Parsers;

use App\Parser;

class _100spRu extends Parser
{
    protected function getName()
    {
        $name = $this->content
            ->query("id('title')/@data-name");

        if ($name->length == 0) {
            return false;
        }

        $this->name = trim($name->item(0)->textContent);
    }

    protected function getVendorCode()
    {
        $vendorCode = $this->content
            ->query('.//*[@data-field="articul"]');

        if ($vendorCode->length == 0) {
            return false;
        }

        $this->vendor_code = trim($vendorCode->item(0)->textContent);
    }

    protected function getDescription()
    {
        $this->description = collect(
            $this->content
                ->query('.//*[@data-field="description"]')
        )->map(function ($node) {
            return trim($node->textContent, " \t\n\r\0\x0B\xC2\xA0");
        })->reject(function ($item) {
            return empty($item);
        })->implode(' ');
    }

    protected function getPrice()
    {
        $price = $this->content
            ->query("id('price')/@data-price|id('price')");

        if ($price->length == 0) {
            return false;
        }

        $this->price = trim($price->item(0)->textContent);
    }

    protected function getOptions()
    {
        $this->options = collect(
            $this->content
                ->query('.//*[@itemprop="offers"]//table//td[@data-field="oneSize"]')
        )->map(function ($color) {
            return ['color' => trim($color->textContent)];
        });
    }

    protected function getImages()
    {
        $this->images = collect(
            $this->content
                ->query('.//ul[@class="pictures-list"]//a/@href')
        )->map(function ($item, $key) {
            return $item->value;
        });
    }

    protected function getCards()
    {
        $this->cards = collect(
            $this->content
                ->query('id("sortable-list")//td[contains(@class, "title ")]/a/@href')
        )->map(function ($item, $key) {
            return ['url' => $this->url->component['scheme'] . '://' . $this->host . $item->value];
        });
    }
}
