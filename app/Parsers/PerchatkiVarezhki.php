<?php

namespace App\Parsers;

use App\Parser;

class PerchatkiVarezhki extends Parser
{
    public function __construct($url, $card = null)
    {
        parent::__construct($url, $card);
    }

    protected function getName()
    {
        $this->name = false;
        return $this->name;
    }

    protected function getVendorCode()
    {
        $vendorCode = $this->content
            ->query('id("p_list")/li/span');

        if ($vendorCode->length == 0) {
            $this->vendor_code = false;
            return false;
        }

        $this->vendor_code = trim($vendorCode->item(0)->textContent);
        return $this->vendor_code;
    }

    protected function getDescription()
    {
        $rows = $this->content
            ->query('.//*[@id="карточка товара"]//tr');

        $features = array();
        foreach ($rows as $row) {
            $rowData = [];
            foreach ($this->content->query('.//td', $row) as $k => $col) {
                $text = collect($col->childNodes)->map(function ($node) {
                    return trim($node->textContent, " \t\n\r\0\x0B\xC2\xA0");
                })->reject(function ($item) {
                    return empty($item);
                })->implode(', ');

                if ($k == 0) {
                    $text = mb_strtoupper(mb_substr($text, 0, 1)) . mb_substr($text, 1);
                }

                $rowData[] = $text;
            }
            $features[] = implode(': ', $rowData);
        }

        $additional = $this->getAdditionalDescription();
        if ($additional) {
            array_unshift($features, $additional);
        }

        $this->description = implode('. ', $features);

        return $this->description;
    }

    protected function getAdditionalDescription()
    {
        if ($this->card) {
            $xPath = new \DOMXPath($this->card->ownerDocument);
            $additional = $xPath->query('.//div[@class="t_note"]', $this->card);
            if ($additional->length > 0) {
                return trim($additional->item(0)->textContent);
            }
        }

        return false;
    }

    protected function getPrice()
    {
        $price = $this->content->query(
            'id("tovar_card2")//*[@class="price"]//b'
        );

        if ($price->length == 0) {
            $this->price = false;
            return false;
        }

        $this->price = trim($price->item(0)->textContent);

        return $this->price;
    }

    protected function getOptions()
    {
        $this->options = collect(
            $this->content->query(
                'id("tovar_card2")//*[@class="product_param"]//select//option'
            )
        )->map(function ($node) {
            return [
                'color' => trim($node->textContent)
            ];
        });

        return $this->options;
    }

    protected function getImages()
    {
        $this->images = collect($this->content->query(
            './/a[@class="highslide"]/@href|id("tovar_detail2")//img/@src'
        ))->map(function ($node) {
            $url = $node->textContent;

            if (strpos($url, 'thumb') > -1) {
                $url = '/d/' . (basename($url));
            }

            return $this->url->component['scheme'] . '://' . $this->host . $url;
        });

        return $this->images;
    }

    protected function getCards()
    {
        $this->cards = collect($this->content->query(
            './/*[@class="site-content-middle"]//*[@class="tovar2"]'
        ))->map(function ($node) {
            return [
                'block' => $node,
                'url' => $this->url->component['scheme'] . '://' 
                    . $this->host 
                    . $this->content->query('./h2/a/@href', $node)->item(0)->textContent
            ];
        });

        return $this->cards;
    }
}
