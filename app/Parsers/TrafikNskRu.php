<?php

namespace App\Parsers;

use App\Parser;

class TrafikNskRu extends Parser
{
    protected function getName()
    {
        $name = $this->content
            ->query(".//div[@class='product-info']/descendant::h1[1]");

        if ($name->length == 0) {
            return false;
        }

        $this->name = trim($name->item(0)->textContent);
    }

    protected function getVendorCode()
    {
        $vendorCode = $this->content
            ->query(".//div[@class='description']/span[contains(./text(),'Артикул:')]/following-sibling::text()");

        if ($vendorCode->length == 0) {
            return false;
        }

        $this->vendor_code = trim($vendorCode->item(0)->textContent);
    }

    protected function getDescription()
    {
        $this->description = collect($this->content
            ->query(".//div[@class='description']/*")
        )->map(function ($node) {
            return trim($node->textContent, " \t\n\r\0\x0B\xC2\xA0");
        })->reject(function ($item) {
            return empty($item);
        })->implode(' ');
    }

    protected function getPrice()
    {
        $price = $this->content
            ->query(".//div[@class='price']");

        if ($price->length == 0) {
            return false;
        }

        $this->price = trim(preg_replace(
            ['#[\t\n\r\0\x0B\xC2\xA0]#', '#\s{2,}#', '#.*?:.*?(\d+)\s?(\d+)?[^\d]*.*#'],
            ['', ' ', '$1$2'],
            $price->item(0)->textContent
        ));
    }

    protected function getOptions()
    {
        $colors = $this->content
            ->query(".//div[@class='options']/descendant::b[contains(./text(),'Цвет')]/following-sibling::select//option");

        $colors = collect($colors)->map(function ($item) {
            if ($item->textContent == ' --- Выберите --- ') return;
            $color = trim($item->textContent);
            if (preg_match('#\d+\-(.*)#', $color)) {
                $color = preg_replace('#\d+\-(.*)#', '$1', $color);
            }
            return $color;
        })->reject(function ($name) {
            return empty($name);
        });

        $sizes = array(0);
        $sizesBlock = $this->content
            ->query(".//div[@class='options']/descendant::b[contains(./text(),'Размер')]/following-sibling::select//option");

        if ($sizesBlock->length) {
            $sizes = collect($sizesBlock)->map(function ($item) {
                if ($item->textContent == ' --- Выберите --- ') return;
                return trim($item->textContent);
            })->reject(function ($name) {
                return empty($name);
            });
        }

        $options = array();
        foreach ($colors as $color) {
            foreach ($sizes as $size) {
                $options[] = array(
                    'color' => $color,
                    'size' => $size ? $size : false,
                );
            }
        }

        $this->options = collect($options)->sort([$this, 'sortEuroSize']);
    }

    protected function getImages()
    {
        $this->images = collect(
            $this->content
                ->query(".//div[@class='image-additional']//a/@href")
        )->map(function ($item, $key) {
            return $item->value;
        });
    }

    protected function getCards()
    {
        $this->cards = collect(
            $urls = $this->content
                ->query(".//div[@class='name']/a/@href")
        )->map(function ($item, $key) {
            return ['url' => $item->value];
        });
    }
}
