<?php

namespace App\Parsers;

class AviliStyleRu extends \App\Parser
{
    protected function getName()
    {
        $this->name = false;
        return $this->name;
    }

    public function getVendorCode()
    {
        $vendorCode = $this->content
            ->query(".//div[@class='pv-name']");

        if ($vendorCode->length == 0) {
            return false;
        }

        $this->vendor_code = trim($vendorCode->item(0)->textContent);
        return $this->vendor_code;
    }

    public function getDescription()
    {
        $features = [];
        $this->description = collect(
            $this->content
                ->query(".//div[@class='pv-info']//descendant::h4[contains(./text(),'Описание')]/following-sibling::*")
        )->map(function ($node) {
            return trim($node->textContent, " \t\n\r\0\x0B\xC2\xA0");
        })->reject(function ($item) {
            return empty($item);
        })->implode('. ');

        return $this->description;
    }

    public function getPrice()
    {
        $price = $this->content
            ->query('.//div[@class="pv-price"]');

        if ($price->length == 0) {
            $this->price = false;
            return false;
        }

        preg_match('#([\s\d]+)#', $price->item(0)->textContent, $matches);
        $this->price = trim($matches[1] ?? 0);
    }

    public function getOptions()
    {
        $this->options = collect(
            $this->content->query(
                './/div[@class="pv-sizes"]//div[@data-trigger="spinner"]/span'
            )
        )->map(function ($node) {
            return [
                'color' => trim($node->textContent)
            ];
        });

        return $this->options;
    }

    public function getImages()
    {
        $this->images = collect($this->content->query(
            './/div[@class="pv-photo-big"]//ul//li/a/img/@data-zoom-image'
        ))->map(function ($node) {
            $url = str_replace('-----', '/', rawurlencode(urldecode(str_replace('/', '-----', $node->textContent))));
            return $this->url->component['scheme'] . '://' . $this->host . $url;
        });

        return $this->images;
    }

    public function getCards()
    {
        $urls = $this->content
            ->query('.//div[contains(./@class,"product-item")]/div[@class="product-footer"]/div[@class="product-name"]/a/@href');

        if ($urls->length == 0) {
            return false;
        }

        $this->cards = collect($urls)
            ->map(function ($item, $key) {
                return ['url' => $this->url->component['scheme'] . '://' . $this->host . $item->value];
            });
    }
}
