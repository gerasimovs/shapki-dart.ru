<?php

namespace App\Parsers;

use App\Parser;

class KupalnikNskRu extends Parser
{
    protected function getName()
    {
        return false;
    }

    protected function getVendorCode()
    {
        $vendorCode = $this->content
            ->query(".//div[@class='product-info']/descendant::h1[1]");

        if ($vendorCode->length == 0) {
            return false;
        }

        $this->vendor_code = trim($vendorCode->item(0)->textContent);
        return $this->vendor_code;
    }

    protected function getDescription()
    {
        $features = [];
        $this->description = collect(
            $this->content
                ->query(".//div[@class='description']/descendant::strong[contains(./text(),'Характеристики')]/following-sibling::div")
        )->map(function ($node) {
            return trim($node->textContent, " \t\n\r\0\x0B\xC2\xA0");
        })->reject(function ($item) {
            return empty($item);
        })->implode('. ');

        return $this->description;
    }

    protected function getPrice()
    {
        $price = $this->content
            ->query(".//div[@class='price']");

        if ($price->length == 0) {
            $this->price = false;
            return false;
        }

        $this->price = trim(preg_replace(['#[\t\n\r\0\x0B\xC2\xA0]#', '#\s{2,}#', '#.*?:.*?(\d+)[^\d]*.*#'], ['', ' ', '$1'], $price->item(0)->textContent));
    }

    protected function getOptions()
    {
        $colorsBlock = $this->content
            ->query(".//div[@id='shapki-options']//div[@class='option-small-slide']");

        $options = array();
        foreach (@$colorsBlock as $colors) {
            $color = trim($this->content->query(".//div[@class='row']//div[@class='col-xs-12']", $colors)->item(0)->textContent);
            $sizesBlock = $this->content->query(".//div[@class='row size-row']", $colors)->item(0);
            $sizes = array(0);
            if ($sizesBlock) {
                $sizes = collect($this->content->query(".//button", $sizesBlock))
                    ->map(function ($size) {
                        return trim($size->textContent);
                    })->reject(function ($size) {
                        return empty($size);
                    });
            }
            foreach ($sizes as $size) {
                $options[] = [
                    'color' => $color,
                    'size' => $size ? $size : false,
                ];
            }
        }

        $this->options = collect($options)->sort([$this, 'sortEuroSize']);
        return $this->options;
    }

    protected function getImages()
    {
        $withMain = (bool) request('with_main');

        $images = array();
        $imagesList = collect();

        if ($withMain) {
            $imagesList = $imagesList->merge($this->content
                ->query('id("main-product-image")|.//div[@class="image-additional"]//a')
            );
        }

        $imagesList = $imagesList->merge($this->content
            ->query('.//div[contains(@class, "option-item")]//a[@class="thumb-link"]')
        );

        if ($imagesList->count() > 0) {
            foreach ($imagesList as $image) {

                if ($image->nodeName == 'img') {
                    $urlNode = $this->content->query('./@src', $image);
                } else {
                    $urlNode = $this->content->query('./@href', $image);
                }

                if ($urlNode->length == 0) {
                    continue;
                }

                $imageUrl = preg_replace(
                    "/(.*?)\/cache(\/.*?)-\d{3,4}x\d{3,4}_\d{1}(\.jpg|\.jpeg|\.png|\.gif)/i",
                    "$1$2$3",
                    filter_var($urlNode->item(0)->textContent, FILTER_VALIDATE_URL)
                );

                if (empty($imageUrl) || strpos($imageUrl, 'image/cache/data') > -1) {
                    continue;
                }

                $imageArray = [$imageUrl];

                if ($image->nodeName == 'img') {
                    $titleNode = $this->content->query('./@title', $image);
                } else {
                    $titleNode = $this->content->query('./img/@title', $image);
                }

                if ($titleNode->length > 0) {
                    $imageArray[] = $titleNode->item(0)->textContent;
                }

                $images[] = implode('#', $imageArray);
            }
        }

        $this->images = $images;
        return $this->images;
    }

    protected function getCards()
    {
        $urls = $this->content
            ->query(".//div[@class='item']//div[@class='name']/a/@href");

        if ($urls->length == 0) {
            return false;
        }

        $this->cards = collect($urls)
            ->map(function ($item, $key) {
                return ['url' => $item->value];
            });
    }
}
