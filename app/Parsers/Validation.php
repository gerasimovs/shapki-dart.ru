<?php

namespace App\Parsers;

use App\Parser as Model;

class Validation extends Model
{
    public function __construct($host)
    {
        $this->rules = config('parser');
        $this->host = $host;

        $this->validation();
    }

    public function validation()
    {
        if (!array_key_exists($this->host, $this->rules)) {
            throw new \Exception('URL не поддерживается');
        }
    }
}
