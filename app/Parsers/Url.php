<?php

namespace App\Parsers;

class Url
{
    public $class;
    public $host;
    public $url;
    public $component;

    public function __construct($url)
    {
        $this->component = parse_url($url);
        $this->url = $this->modify($url);
    }

    public function modify($url)
    {
        $rules = config('parser');
        $host = $this->component['host'];

        if (isset($rules[$host]['host'])) {
            $url = str_replace($this->component['host'], $rules[$host]['host'], $url);
        }

        if (isset($rules[$host]['class'])) {
            $this->class = $rules[$host]['class'];
        }

        return $url;
    }

    public function __toString()
    {
        return $this->url;
    }
}
