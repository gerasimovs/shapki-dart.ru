<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shipper extends Model
{
    //
    public function products()
    {
        return $this->belongsToMany(Product::class)->withTrashed();
    }
}
