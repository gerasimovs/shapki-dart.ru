<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Option extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    public function products()
    {
        return $this->belongsToMany(Product::class, 'offers')
            ->as('offer')
            ->withPivot('id', 'status')
        ;
    }

    static public function firsOrCreateManyFromArrayName(array $options)
    {
        $optionsResult = array();

        foreach ($options as $option) {
            $optionsResult[] = self::firstOrCreate(['name' => $option]);
        }
 
        return collect($optionsResult);
    }

    /*static public function syncWithChangeStatus(\App\Product $product, array $options)
    {
        foreach ($product->options as $option) {
            $option->offer->status = 0;
            $option->offer->save();
        }

        $product->options()->syncWithoutDetaching(collect($options));

        foreach ($product->options()->whereIn('options.id', $options)->get() as $option) {
            $option->offer->status = 1;
            $option->offer->save();
        }

        return $product;
    }*/
}
