<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mailing extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'mailings';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'subject', 'greeting', 'message', 'salutation', 
    ];

    /**
    * Получить товары, привязанные к рассылке.
    */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
