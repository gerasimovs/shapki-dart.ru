<?php

namespace App\Presenters;

use App\Properties\ProductProperty;

class ProductPresenter extends AbstractPresenter
{
    /**
     * @return string
     */
    public function presentRoute(): string
    {
        return route('products.show', $this->entity->id);
    }

    public function presentSku()
    {
        $skuAlias = array_search($this->entity->unit_id, ProductProperty::SKU);
        return $skuAlias;
    }

    /**
     * @param mixed $value
     * @return string
     */
    public function presentUpdatedAt($value): string
    {
        return $this->getDatesPresenter($value);
    }

    /**
     * @param mixed $value
     * @return string
     */
    public function presentCreatedAt($value): string
    {
        return $this->getDatesPresenter($value);
    }

    /**
     * @param mixed $value
     * @return string
     */
    public function presentDeletedAt($value): string
    {
        return $this->getDatesPresenter($value);
    }
}
