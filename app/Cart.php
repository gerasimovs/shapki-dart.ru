<?php

namespace App;

use Illuminate\Support\Facades\Session;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    public $items;
    public $totalQty;

    public function __construct($cart = null)
    {
        if (Session::has('cart')) {
            $cart = Session::get('cart');
            $this->items = $cart->items;
            $this->totalQty = $cart->totalQty;
        }
    }

    public function add(Offer $offer, int $count)
    {
        $oldOffer = $this->offers->find($offer);
        $count += $oldOffer ? $oldOffer->pivot->count : 0;
        $this->offers()->sync([$offer->id => ['count' => $count]], false);

        return $this;
    }

    public function remove(Offer $offer, $qty = 1)
    {
        $count = (int) $this->offers->find($offer)->pivot->count;
        $count--;
        if ($count > 0 && $qty != 'all') {
            $this->offers()->sync([$offer->id => ['count' => $count]], false);
        } else {
            $this->offers()->detach([$offer->id]);
        }

        return $this;
    }

    public function save_cart()
    {
        if ($this->totalQty) {
            Session::put('cart', $this);
        } else {
            $this->delete_cart();
        }
    }

    public function delete_cart()
    {
        Session::forget('cart');
    }


    /**
     * Get the user that owns the cart.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
    * Get the offers for the cart.
    */
    public function offers()
    {
        return $this->belongsToMany(Offer::class)
            ->withPivot('count');
    }

    /**
     * Get count of offers in the cart.
     *
     * @return string
     */
    public function getTotalQtyAttribute()
    {
        return $this->offers->sum('pivot.count');
    }

}
