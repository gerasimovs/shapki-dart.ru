<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UploadImage extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'url'
    ];

    public function image()
    {
        return $this
            ->belongsTo(Image::class)
            ->withTrashed();
    }

    public function getUploadDir()
    {
        return substr($this->hash, 0, 2) . '/' . substr($this->hash, 2, 2);
    }
}
