<?php 

namespace App\Helpers;

class Blade
{
    public static function snippets($expression, $test)
    {
        $count = preg_match_all('#\[\[(.*?)\]\]#', $expression, $snippets);

        if ($count) {
       		$result = array();
        	foreach ($snippets[1] as $key => $value) {
        		if (strpos($value, ':') > -1) {
        			$params = explode(':', $value);
        			if ($params[0] == 'Product') {
        				$product = \App\Product::withTrashed()
        					->with('images:url')
        					->select('id', 'vendor_code')
        					->find($params[1]);

        				if ($product) {
        					$matchKey = $snippets[0][$key];
                            $imageUrl = $product->images()->value('url');
        					$result[$matchKey] = '<a href="' . route('products.show', $product->id) . '"><image src="' . $imageUrl . '" alt="' . $product->vendor_code . '" style="max-width: 98%;"></a>';
        				}
        			}
        		}
        	}
        }

        $expression = strtr($expression, $result);

        return $expression;
    }
}