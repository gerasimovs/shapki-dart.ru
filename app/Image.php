<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Image extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'hash', 'url',
    ];

    public function products()
    {
        return $this
            ->belongsToMany(Product::class)
            ->withTrashed();
    }

    public function upload()
    {
        return $this
            ->hasMany(UploadImage::class);
    }

    public function getUploadDir()
    {
        return substr($this->hash, 0, 2) . '/' . substr($this->hash, 2, 2);
    }
}
