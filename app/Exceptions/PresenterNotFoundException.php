<?php

namespace App\Exceptions;

use Exception;

class PresenterNotFoundException extends Exception
{
    /**
     * Create a new presenter not found exception.
     *
     * @param string      $class
     * @param string|null $message
     * @param int         $code
     * @param Exception   $previous
     */
    public function __construct($class, $message = null, $code = 0, Exception $previous = null)
    {
        if (!$message) {
            $message = $class.' not found. Please set the $presenter property to your presenter path.';
        }

        parent::__construct($message, $code, $previous);
    }
}
