<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;

class BladeServiceProvider extends ServiceProvider
{
    /**
     * Регистрация привязок в контейнере.
     *
     * @return void
     */
    public function boot()
    {
        Blade::directive('datetime', function($expression) {
            return "<?php echo with({$expression})->format('d.m.Y'); ?>";
        });

        Blade::directive('route', function($expression) {
            return "<?php echo route({$expression}); ?>";
        });

        Blade::directive('money', function($expression) {
            return "<?php echo number_format({$expression}, 0, ',', ' '); ?>";
        });

        Blade::directive('snippets', function($expression) {
            return '<?php 

            $expression = '. $expression . ';

            $count = preg_match_all(\'#\[\[(.*?)\]\]#\', $expression, $snippets);
            $result = array();

            if ($count) {
                foreach ($snippets[1] as $key => $value) {
                    if (strpos($value, \':\') > -1) {
                        $params = explode(\':\', $value);
                        if ($params[0] == \'Product\') {
                            $product = \App\Product::withTrashed()
                                ->with(\'images:url\')
                                ->select(\'id\', \'vendor_code\')
                                ->find($params[1]);

                            if ($product) {
                                $matchKey = $snippets[0][$key];
                                $imageUrl = $product->images()->value(\'url\');
                                $imageCid = $message->embed($imageUrl);
                                $result[$matchKey] = \'<a href="\' . route(\'products.show\', $product->id) . \'"><image src="\' . $imageCid . \'" alt="\' . $product->vendor_code . \'" style="max-width: 98%;"></a>\';
                            }
                        }
                    }
                }
            }

            echo $expression = strtr($expression, $result); ?>';
            /* return "<?php echo \App\Helpers\Blade::snippets({$expression}); ?>";*/
        });

        //Blade::component('components.markdown', 'markdown');
    }

    /**
     * Регистрация сервис-провайдера.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}