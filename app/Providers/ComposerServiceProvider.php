<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Регистрация привязок в контейнере.
     *
     * @return void
     */
    public function boot()
    {
        View::composer(
            ['categories._list', 'partials.mainmenu'],
            \App\Http\ViewComposers\CategoriesComposer::class
        );

        View::composer(
            ['home.dashboard', 'partials.topbar'],
            \App\Http\ViewComposers\UserInformersComposer::class
        );
    }

    /**
     * Регистрация сервис-провайдера.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}