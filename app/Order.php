<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $casts = [
        'order_id' => 'integer',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function offers()
    {
        return $this->belongsToMany(Offer::class)
            ->withPivot('product_id', 'option_id', 'vendor_code', 'price', 'color', 'comment', 'qty', 'confirm');
    }

    public function getStatuses()
    {
        return [
            'Processing' => 'Processing',
            'Problem' => 'Problem',
            'PaymentDue' => 'PaymentDue',
            'PickupAvailable' => 'PickupAvailable',
            'InTransit' => 'InTransit',
            'Delivered' => 'Delivered',
            'Cancelled' => 'Cancelled',
            'Returned' => 'Returned',
        ];
    }
}
