<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class UserRegistered extends Notification
{
    use Queueable;

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->success()
            ->subject('Регистрация на сайте  ' . config('app.name'))
            ->greeting('Добрый день!')
            ->line('Спасибо за регистрацию на нашем сайте. Доступ к товарам будет активирована после одобрения администратором сайта.')
            ->action('Перейти на сайт', route('home'))
            ->salutation('С уважением,  ' . config('app.name'))
        ;
    }
}
