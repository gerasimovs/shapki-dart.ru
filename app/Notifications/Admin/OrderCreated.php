<?php

namespace App\Notifications\Admin;

use App\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;

class OrderCreated extends Notification
{
    use Queueable;

    private $order;

    /**
     * Create a new notification instance.
     *
     * @param  \App\Order  $order
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
        $this->url = route('admin.orders.show', $order);
        $this->chat_id = config('services.telegram-bot-api.chat_id');
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            'mail',
            //TelegramChannel::class,
        ];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->success()
            ->subject('📦 Заказ #' . $this->order->id . ' - ' . config('app.name'))
            ->greeting('Уважаемый модератор!')
            ->line('Пользователь ' . $this->order->user->name . ' (' . $this->order->user->email . ') оформил заказ.')
            ->line('Сумма заказа: ' . $this->order->totalPrice . ' руб.')
            ->line('Всего единиц: ' . $this->order->totalQty . ' шт.')
            ->action('Посмотреть детали заказа', $this->url)
            ->salutation('С уважением,  ' . config('app.name'))
        ;
    }

    /**
     * Get the Telegram of the notification
     *
     * @param  mixed  $notifiable
     * @return \NotificationChannels\Telegram\TelegramMessage
     **/
    public function toTelegram($notifiable)
    {
        return TelegramMessage::create()
            ->to($this->chat_id)
            ->content("Оформлен новый заказ #{$this->order->id}. \nПользователь: {$this->order->user->name} ({$this->order->user->email}). \nСумма заказа: {$this->order->totalPrice} руб. \nВсего единиц: {$this->order->totalQty} шт.")
            ->button('Посмотреть детали заказа', $this->url)
        ;
    }


}
