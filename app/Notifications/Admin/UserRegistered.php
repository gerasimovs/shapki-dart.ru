<?php

namespace App\Notifications\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use NotificationChannels\SmscRu\SmscRuMessage;
use NotificationChannels\SmscRu\SmscRuChannel;
use NotificationChannels\Telegram\TelegramChannel;
use NotificationChannels\Telegram\TelegramMessage;
use Illuminate\Notifications\Messages\MailMessage;

class UserRegistered extends Notification
{
    use Queueable;

    private $user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
        $this->url = route('admin.users.edit', $this->user->id);
        $this->chat_id = config('services.telegram-bot-api.chat_id');
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [
            'mail',
            // SmscRuChannel::class,
            // TelegramChannel::class,
        ];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->error()
            ->subject('Новый пользователь на сайте  ' . env('APP_NAME'))
            ->greeting('Уважаемый модератор!')
            ->line('На сайте зарегистрировался новый пользователь.')
            ->line('Имя пользователя: ' . $this->user->name)
            ->line('Адрес e-mail: ' . $this->user->email)
            ->action('Установить права', $this->url)
            ->salutation('С уважением,  ' . env('APP_NAME'))
        ;
    }

    public function toSmscRu($notifiable)
    {
        return SmscRuMessage::create("User {$this->user->email} is registred!");
    }

    public function toTelegram($notifiable)
    {
        return TelegramMessage::create()
            ->to($this->chat_id)
            ->content("Пользователь {$this->user->email} зарегистрировался на сайте!")
            ->button('Установить права', $this->url);
    }

}
