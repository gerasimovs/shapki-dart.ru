<?php

namespace App\Notifications\Admin;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

/**
 * ProductParsed class
 * @property string $message
 */
class ParserComplete extends Notification
{
    use Queueable;

    /**
     * @var string  $message
     */
    public $message;

    /**
     * @var string  $status
     */
    public $status;

    /**
     * @var array  $data
     */
    public $data;

    /**
     * Create a new notification instance.
     *
     * @param  string $message
     * @param  string $status
     * @param  array $data
     * @return void
     */
    public function __construct($message, $status = 'success', $data = [])
    {
        $this->message = $message;
        $this->status = $status;
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return $this->status
            ? ['database']
            : [];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'status' => $this->status,
            'message' => $this->message,
            'is_html' => true,
            'data' => $this->data,
        ];
    }
}
