<?php

namespace App\Traits;

use App\Exceptions\PresenterNotFoundException;
use App\Presenters\AbstractPresenter;

trait Presentable
{
    /**
     * View presenter instance
     *
     * @var AbstractPresenter
     */
    protected $presenterInstance;

    /**
     * Prepare a new or cached presenter instance
     *
     * @return AbstractPresenter
     * @throws PresenterNotFoundException
     */
    public function present()
    {
        $presenterClassName = $this->presenter ?? 'App\\Presenters\\' . class_basename($this) . 'Presenter';

        if (!class_exists($presenterClassName, true)) {
            throw new PresenterNotFoundException($presenterClassName);
        }

        if (!$this->presenterInstance) {
            $this->presenterInstance = new $presenterClassName($this);
        }

        return $this->presenterInstance;
    }
}
