<?php

namespace App\Listeners;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserEventSubscriber
{
    /**
     * Handle user logout events.
     */
    public function onUserRegistered($event) 
    {
        $event->user->notify(new \App\Notifications\UserRegistered());
        \App\Role::with('users')
            ->where('slug', 'moderator')
            ->first()
            ->users
            ->each(function($user) use ($event) {
                try {
                    $user->notify(new \App\Notifications\Admin\UserRegistered($event->user));
                } catch (\NotificationChannels\SmscRu\Exceptions\CouldNotSendNotification $e) {
                    $e->getMessage();
                }
            });

        $invite = \App\Invite::where('email', $event->user->email)->first();
        if ($invite) {
            $event->user->role_id = $invite->role_id;
            $event->user->save();
            $invite->delete();
        }
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'Illuminate\Auth\Events\Registered',
            'App\Listeners\UserEventSubscriber@onUserRegistered'
        );
    }
}
