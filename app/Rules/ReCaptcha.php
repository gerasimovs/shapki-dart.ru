<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ReCaptcha implements Rule
{
    protected const VERIFY_URL = 'https://www.google.com/recaptcha/api/siteverify';

    protected $errors = [];

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $result = $this->getRecaptchaResult($value);

        if (isset($result['error-codes']) && $result['error-codes'] !== []) {
            $this->errors = $result['error-codes'];

            return false;
        }

        if ($result['score'] < config('recaptcha.score')) {
            $this->errors[] = __("recaptcha.score-threshold-not-met");

            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        $errors = array_map(function ($error) {
            return __("recaptcha.{$error}");
        }, $this->errors);

        return implode(' ', array_filter($errors));
    }

    private function getRecaptchaResult($token)
    {
        $recaptchaData = array(
            'secret' => config('recaptcha.secret_key'),
            'response' => $token,
            'remoteip' => request()->ip(),
        );

        $recaptchaQuery = http_build_query($recaptchaData);

        $options = array(
            \CURLOPT_URL => self::VERIFY_URL,
            \CURLOPT_POST => true,
            \CURLOPT_RETURNTRANSFER => true,
            \CURLOPT_POSTFIELDS => $recaptchaQuery,
            \CURLOPT_HTTPHEADER => array(
                'Content-Type: application/x-www-form-urlencoded; charset=utf-8',
                'Content-Length: ' . strlen($recaptchaQuery)
            ),
        );

        $curl = curl_init();
        curl_setopt_array($curl, $options);

        $recaptchaResult = json_decode(curl_exec($curl), true);
        curl_close($curl);

        return $recaptchaResult;
    }
}
