<?php

namespace App;

use Exception;

class Crm
{
    private $webHook;
    public $company;

    private static $methods = [];

    public function __construct()
    {
        $this->webHook = config('crm.webhook');
        $this->company = config('crm.mycompany');
    }

    public function request(string $method, array $params = [])
    {
        $urlRequest = $this->webHook . $method;
        $curlOptions = array(
            CURLOPT_TIMEOUT => 70,
            CURLOPT_HEADER => false,
            CURLOPT_POST => true,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_CONNECTTIMEOUT => 65,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_POSTFIELDS => http_build_query($params),
            CURLOPT_RETURNTRANSFER => true,
        );

        $curl = curl_init($urlRequest);
        curl_setopt_array($curl, $curlOptions);

        $curlResult = curl_exec($curl);

        if (false === $curlResult) {
            $curlErrorNumber = curl_errno($curl);
            $errorMsg = sprintf(
                'При выполнении cURL-запроса произошла ошибка (код: %s): %s' . PHP_EOL,
                curl_errno($curl),
                curl_error($curl)
            );
            curl_close($curl);
            throw new Exception($errorMsg);
        }

        $requestInfo = curl_getinfo($curl);
        curl_close($curl);

        switch ($requestInfo['http_code']) {
            case 403:
                $errorMsg = 'Доступ к порталу запрещен, запрос прерван';
                throw new Exception($errorMsg);
                break;

            case 502:
                $errorMsg = 'Проблема с доступом к порталу';
                throw new Exception($errorMsg);
                break;
        }

        if ('' === $curlResult) {
            $errorMsg = 'Пустой ответ с портала';
            throw new Exception($errorMsg);
        }

        $jsonResult = json_decode($curlResult, true);
        unset($curlResult);

        $jsonErrorCode = json_last_error();
        if (null === $jsonResult && (JSON_ERROR_NONE !== $jsonErrorCode)) {
            $errorMsg = sprintf('Ошибка при выполнении функции json_decode (код: %s)', $jsonErrorCode);
            throw new Exception($errorMsg);
        }

        return $jsonResult;
    }

    public function __get($method)
    {
        if (!isset(static::$methods[$method])) {
            $class = 'App\\Crm\\' . $method;
            static::$methods[$method] = new $class();
        }

        return static::$methods[$method];
    }

}
