<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    const UPDATED_AT = null;

    protected $fillable = ['amount', 'description'];

    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => \App\Events\TransactionCreated::class,
    ];

    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    public function getStatusAttribute()
    {
        return $this->amount > 0
            ? __('transaction.receipt')
            : __('transaction.write_off')
        ;
    }
}
