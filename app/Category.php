<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Kalnoy\Nestedset\NodeTrait;

class Category extends Model
{
    use SoftDeletes;
    use NodeTrait;

    public $statuses = [
        0 => 'primary',
        1 => 'secondary',
        2 => 'success',
        3 => 'danger',
        4 => 'warning',
        5 => 'info',
        6 => 'light',
        7 => 'dark',
        8 => 'body',
        9 => 'muted',
        10 => 'white',
        11 => 'black-50',
        12 => 'white-50',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'category_id', 
    ];

    /**
    * Получить товары в категории.
    */
    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function additionalProducts()
    {
        return $this->belongsToMany(Product::class);
    }

    public function trashedProducts()
    {
        return $this->hasMany(Product::class)->withTrashed();
    }


    /**
    * Получить родительскую категорию.
    */
    public function category()
    {
        return $this->belongsTo(self::class);
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class)
            ->withPivot('description')
        ;
    }

    /**
     * Get category status.
     *
     * @return string
     */
    public function getStatusAttribute()
    {
        return $this->statuses[$this->status_id];
    }

    public function getParentIdName()
    {
        return 'category_id';
    }

    public function setCategoryIdAttribute($value)
    {
        $this->setParentIdAttribute($value);
    }

    public function toTop()
    {
        $this->insertBeforeNode(self::whereIsRoot()->defaultOrder()->first());
    }

    public function toBottom()
    {
        $this->insertAfterNode(self::whereIsRoot()->reversed()->first());
    }


    public function getStatuses()
    {
        return $this->statuses;
    }
}
