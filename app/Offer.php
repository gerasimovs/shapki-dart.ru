<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id', 'option_id', 'status', 
    ];
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    

    public function product()
    {
        return $this
            ->belongsTo(\App\Product::class)
            ->withTrashed();
    }

    public function option()
    {
        return $this->belongsTo(Option::class);
    }

    /**
     * Get the cart that owns the offers.
     */
    public function cart()
    {
        return $this->belongsTo(Cart::class);
    }

    // public function size()
    // {
    //     return $this->belongsTo(Size::class);
    // }
}
