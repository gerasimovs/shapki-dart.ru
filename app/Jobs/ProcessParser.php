<?php

namespace App\Jobs;

use App\ProductSupplier;
use App\Properties\ParserProperty;
use App\Services\ParserService;
use App\Services\ProductService;
use App\Notifications\Admin\ParserComplete;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessParser extends JobAbstract implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 5;

    /**
     * @var ProductSupplier $parser
     */
    protected $parser;

    /**
     * Create a new job instance.
     *
     * @param  ProductSupplier  $parser
     * @return void
     */
    public function __construct(ProductSupplier $parser)
    {
        $this->parser = $parser;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $product = $this->getParser()->product;
        $route = route('admin.products.edit', $product);
        $isConsole = app()->runningInConsole();

        try {
            $data = app(ParserService::class)->getData($this->getParser()->url);
            if (!empty($data->errors)) {
                throw $data->errors;
            }

            $result = app(ProductService::class)->setOptions($this->getParser()->product, $data->options);

            $message = [
                0 => "Парсинг для товара <a href=\"{$route}\">{$product->vendor_code}</a> выполнен"
            ];

            if (!empty($result['attached']) || !empty($result['detached'])) {
                $message[1] = 'success';
            }

            if ($isConsole) {
                $this->getUser()->notify(new ParserComplete(...$message));
            } else {
                $this->setStatusMessage(...$message);
            }

            if ($product->trashed()) {
                $message = [
                    0 => "Товар <a href=\"{$route}\">{$product->vendor_code}</a> появился в продаже!",
                    1 => 'primary',
                ];

                if ($isConsole) {
                    $this->getUser()->notify(new ParserComplete(...$message));
                } else {
                    $this->setStatusMessage(...$message);
                }

                $this->getParser()->update([
                    'status' => ParserProperty::STATUS['arrival'],
                ]);
            }
            
            $oldPrice = $this->getParser()->price;
            $this->getParser()->price = (float) $data->price;
            if ($this->getParser()->isDirty()) {
                $message = [
                    0 => "Закупочная цена товара <a href=\"{$route}\">{$product->vendor_code}</a> изменена с {$oldPrice} на {$data->price}",
                    1 => 'info',
                ];
                if ($isConsole) {
                    $this->getUser()->notify(new ParserComplete(
                        $message,
                        'info'
                    ));
                } else {
                    $this->setStatusMessage(...$message);
                }
            }
        } catch (\Exception $e) {
            $message = [
                0 => "Ошибка при парсинге товара <a href=\"{$route}\">{$product->vendor_code}</a>: {$e->getMessage()}",
                1 => 'danger',
                2 => [
                    'file' => $e->getFile(),
                    'line' => $e->getLine(),
                    'code' => $e->getCode(),
                ]
            ];

            if ($isConsole) {
                if (!$product->trashed() && $e->getCode() == 404) {
                    $this->getUser()->notify(new ParserComplete(...$message));
                }
            } else {
                $this->setStatusMessage(...$message);
            }

            if ($e->getCode() == 404) {
                $this->getParser()->update([
                    'status' => ParserProperty::STATUS['missing'],
                ]);
            }
        }

        if (!$product->touch()) {
            $this->setStatusMessage(
                'Ошибка при сохранении товара',
                'danger'
            );
        }
        
        return $this->getStatusMessages();
    }

    /**
     * @return ProductSupplier
     */
    public function getParser()
    {
        return $this->parser;
    }

    /**
     * @return \App\User
     */
    public function getUser()
    {
        static $user;

        if (!$user) {
            $user = \App\User::query()->where('email', 'mrteos@gmail.com')->first();
        }

        return $user;
    } 
}
