<?php

namespace App\Jobs;

use App\Jobs\ProcessParser;
use App\Services\ParserService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class CheckUnparsedProduct
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $parsers;

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $parsers = app(ParserService::class)->withoutUpdate()->take(250)->get();

        $parsers->each(function ($parser) {
            if (!$this->hasParser($parser)) {
                $this->parsers->push($parser);
                ProcessParser::dispatch($parser);
                sleep(5);
            }
        });
    }

    /**
     * @param \App\Parser $parser 
     * @return boolean
     */
    public function hasParser($parser)
    {
        return $this
            ->getParsers()
            ->contains($parser);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getParsers()
    {
        if ($this->parsers === null) {
            $this->parsers = \App\Job::query()
            ->where(
                'payload->data->commandName',
                ProcessParser::class
            )
            ->get(['payload'])
            ->map(function ($job) {
                $command = unserialize($job->payload['data']['command']);
                if (method_exists($command, 'getParser')) {
                    return $command->getParser();
                } else {
                    return false;
                }

            })->filter();
        }

        return $this->parsers;
    }
}
