<?php

namespace App\Jobs;

use Illuminate\Support\HtmlString;

class JobAbstract
{
    protected $statusMessages = [];

    public function setStatusMessage($message, $type = 'info', $params = [])
    {
        $this->statusMessages[$type][] = [
            'message' => new HtmlString($message),
            'params' => $params,
        ];
    }

    public function getStatusMessages()
    {
        return $this->statusMessages;
    }

}
