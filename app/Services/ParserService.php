<?php

namespace App\Services;

use App\Parser;
use App\ProductSupplier;

/**
 * ParserService class
 * 
 * @package App
 */
class ParserService
{
    const PERIOD_NEED_UPDATED = 2;

    /**
     * @param string $externalLink 
     * @return Parser
     */
    public function getData($externalLink)
    {
        $parser = new Parser($externalLink);

        if (!empty($parser->errors)) {
            return $parser;
        }
        return $parser->parseOne();
    }

    /**
     * @param \Carbon\Carbon|null $date 
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function withoutUpdate(\Carbon\Carbon $date = null)
    {
        if ($date === null) {
            $date = now()->subDays(self::PERIOD_NEED_UPDATED);
        }

        return ProductSupplier::query()
            ->select(['product_supplier.*', 'products.updated_at as product_updated_at'])
            ->join('products', 'products.id', 'product_supplier.product_id')
            ->whereHas('product', function ($query) use ($date) {
                $query->withTrashed()
                    ->where('updated_at', '<', $date);
            })
            ->oldest('products.updated_at')
            ->hasExternalUrl();
    }
}
