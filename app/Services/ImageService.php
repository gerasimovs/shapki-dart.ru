<?php

namespace App\Services;

use App\Image;
use App\UploadImage;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

/**
 * ImageService class
 * 
 * @package App
 */
class ImageService
{
    /**
     * @var Storage $storage
     */
    public $storage;

    /**
     * ImageService constuctor
     */
    public function __construct()
    {
        $this->storage = Storage::disk(config('filesystems.cloud'));
    }

    /**
     * @param string $url Image URL
     * @return bool|Image 
     */
    public function firstOrCreate($url)
    {
        // Check image URL
        if (!filter_var($url, FILTER_VALIDATE_URL, FILTER_FLAG_PATH_REQUIRED)) {
            return false;
        }

        // Check if the image has been loaded before
        $model = UploadImage::query()->with('image')->where('url', $url)->first()->image
            ?? Image::query()->where('url', $url)->first();

        if ($model) {
            return $model;
        }

        // Check if the image is uploaded to the cloud
        $urlComponents = parse_url($url);
        if ($this->isCloud($urlComponents['host'])) {
            $hash = $this->getHashFromCloudPath($urlComponents['path']);
            $imageByHash = $this->getImageWhereHash($hash);

            if ($imageByHash) {
                return $imageByHash;
            }

            return $this->saveImage(compact('url', 'hash'));
        }

        // Upload image
        $image = $this->uploadFromUrl($url);
        if ($image) {
            $uploadImage = UploadImage::query()->with('image')->firstOrNew(compact('url'));
            $uploadImage->image()->associate($image);
            $uploadImage->save();
        }

        return $image;
    }

    /**
     * @param string $hash 
     * @return bool|Image
     */
    public function getImageWhereHash($hash)
    {
        return Image::query()->where('hash', $hash)->first();
    }

    /**
     * @param string $url 
     * @return Image
     */
    public function uploadFromUrl($url)
    {
        /** @todo Add queue for upload and image resize */
        set_time_limit(0);

        $tempFile = tempnam(sys_get_temp_dir(), 'tempImage');
        if (!copy($url, $tempFile)) {
            return false;
        }

        return $this->uploadImage(
            new File($tempFile),
            $url
        );
    }

    /**
     * @param \Symfony\Component\HttpFoundation\File\File $file 
     * @param string|null $url 
     * @return Image
     */
    public function uploadImage(\Symfony\Component\HttpFoundation\File\File $file, string $url = null)
    {
        set_time_limit(0);
        $hash = sha1_file($file);

        $imageByHash = $this->getImageWhereHash($hash);
        if ($imageByHash) {
            return $imageByHash;
        }

        $urlChunked = str_split($hash, 2);
        $fileName = array_pop($urlChunked) . '.' . $file->extension();
        $path = implode('/', $urlChunked);

        $storagePath = $this->storage->putFileAs($path, $file, $fileName);

        return $this->saveImage([
            'hash' => $hash,
            'url' => $this->storage->url($storagePath),
            'upload_url' => $url,
        ]);
    }

    /**
     * @param array $attributes 
     * @return Image
     */
    public function saveImage($attributes)
    {
        $attributes = \Illuminate\Support\Arr::only($attributes, ['url', 'hash', 'upload_url']);

        $image = new Image;
        $image->url = $attributes['url'];
        $image->hash = $attributes['hash'];
        $image->upload_url = $attributes['upload_url'] ?? null;
        $image->save();

        return $image;
    }

    /**
     * @param Image $image 
     * @param bool $force 
     * @return bool
     */
    public function deleteImage(Image $image, $force = false)
    {
        return $force ? $image->forceDelete() : $image->delete();
    }

    /**
     * @param string $host 
     * @return bool
     */
    public function isCloud(string $host)
    {
        /** @todo Check if the image is uploaded to the cloud */
        return $host == $this->storage->getAdapter()->getClient()->getEndpoint()->getHost();
    }

    /**
     * @param string $path 
     * @return string
     */
    public function getHashFromCloudPath(string $path)
    {
        $bucket = $this->storage->getAdapter()->getBucket();

        $startExt = strpos($path, '.');
        if ($startExt > -1) {
            $path = substr($path, 0, $startExt);
        }

        $hash = explode('/', trim($path, '/'));
        unset($hash[array_search($bucket, $hash)]);

        return implode($hash);
    }
}
