<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Import extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'imports';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'headers', 'file', 'upload_file', 'type',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'headers' => 'array',
    ];

    /**
     * Get the import's status.
     *
     * @param  string  $value
     * @return string
     */
    public function getStatusAttribute()
    {
        switch ($this->status_id) {
            case '1':
                return '----';
                break;
            
            default:
                return 'Не завершен';
                break;
        }
    }
}
