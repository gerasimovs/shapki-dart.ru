<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use \Illuminate\Database\Eloquent\SoftDeletes;
    use \Illuminate\Notifications\Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'company', 'city'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Атрибуты, которые должны быть преобразованы в даты.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function invites()
    {
        return $this->hasMany(Invite::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    public function mailings()
    {
        return $this->belongsToMany(Mailing::class);
    }

    public function routeNotificationForSmscru()
    {
        return $this->phone;
    }

    /**
     * Get the cart record associated with the user.
     */
    public function cart()
    {
        return $this->hasOne(Cart::class);
    }

    public function account()
    {
        return $this->hasOne(Account::class);
    }

    /**
     * Check user has administrative rights
     *
     * @return bool
     */
    public function isAdmin()
    {
        return $this->id == 1 || $this->id == 2;
    }
}
