<?php

namespace App\Mail;

use App\Mailing;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProductsNew extends Mailable
{
    use Queueable, SerializesModels;

    public $greeting;
    public $text;
    public $salutation;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Mailing $mailing)
    {
        $this->subject = $mailing->subject;
        $this->greeting = $mailing->greeting;
        $this->text = $mailing->message;
        $this->salutation = $mailing->salutation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $view = $this->view('emails.products.new');
        return $view;

    }
}
