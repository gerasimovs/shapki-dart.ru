<?php

namespace App;

use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;
    use Searchable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'vendor_code', 'name', 'description', 'price', 
        'category_id', 'unit_id',
        'sorted_at',
    ];

    /**
     * Атрибуты, которые должны быть преобразованы к базовым типам.
     *
     * @var array
     */
    protected $casts = [
        'price' => 'integer',
        'sorted_at' => 'datetime',
        'deleted_at' => 'datetime',
    ];

    private $units = [
        1 => 'piece',
        2 => 'set',
        3 => 'pack',
        4 => 'size_range',
    ];

    private $statuses = [
        1 => 'in_stock',
        2 => 'out_of_stock',
        3 => 'pre_order',
    ];

    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'creating' => \App\Events\ProductCreating::class,
    ];

    /**
     * Получить категории данного товара.
     */
    public function category()
    {
        return $this->belongsTo(Category::class)->withTrashed();
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }

    /**
     * Получить сопутствующие товары.
     */
    public function related()
    {
        return $this->belongsToMany(self::class, 'product_product', 'product_id', 'related_id');
    }
    public function relatedBy()
    {
        return $this->belongsToMany(self::class, 'product_product', 'related_id', 'product_id');
    }

    public function images()
    {
        return $this->belongsToMany(Image::class)
            ->withPivot('status', 'sort', 'description')
            ->oldest('image_product.sort')
            ->oldest('id')
        ;
    }

    public function image()
    {
        return $this->hasOne(ImageProduct::class)
        ;
    }

    public function options()
    {
        return $this->belongsToMany(Option::class, 'offers')
            ->as('offer')
            ->withPivot('id', 'status')
            ->wherePivot('status', 1)
        ;
    }

    public function offers()
    {
        return $this->belongsToMany(Offer::class);
    }

    public function suppliers()
    {
        return $this->belongsToMany(Supplier::class);
    }

    public function mailings()
    {
        return $this->belongsToMany(Mailing::class);
    }

    /**
     * Заготовка запроса доступных товаров.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeAvailable($query)
    {
        return $query->where('available', '!=', 0);
    }
    
    /**
     * @return string
     */
    public function getIsNewAttribute()
    {
        return $this->created_at->diffInDays(now()) <= 7;
    }

    public function getUnitAttribute()
    {
        return $this->units[$this->unit_id];
    }

    public function getAvailabilityAttribute()
    {
        return $this->statuses[$this->status_id];
    }

    /* Product's helpers */
    
    public function getUnits()
    {
        return $this->units;
    }

    public function getStatuses()
    {
        return $this->statuses;
    }
}
