<?php

namespace App;

use App\Parsers\Authentication;
use App\Parsers\Validation;
use App\Parsers\Url;
// use Symfony\Component\DomCrawler\Crawler;
// use Illuminate\Database\Eloquent\Model;

class Parser //extends Model
{
    public $host;
    public $content;
    public $card;

    public $url;
    public $vendor_code;
    public $name;
    public $description;
    public $price;
    public $options = [];
    public $images = [];

    public $cards = [];

    public function __construct($url, $card = null)
    {
        try {
            $this->url = new Url($url);
            $this->init();
            $this->getContent();
            $this->getDom();
            $this->card = $card;
        } catch (\Exception $e) {
            $this->errors = $e->getMessage();
        }
    }

    public function init()
    {
        $this->host = $this->url->component['host'];
    }

    public function parseOne()
    {
        if ($this->url->class && !($this instanceof $this->url->class)) {
            $parser = new $this->url->class((string) $this->url, $this->card);
        } else {
            $parser = $this;
        }

        // $parser->init();
        // $parser->getContent();
        // $parser->getDom();

        $parser->getName();
        $parser->getVendorCode();
        $parser->getDescription();
        $parser->getPrice();
        $parser->getOptions();
        $parser->getImages();

        return $parser;
    }

    public function parseMany()
    {
        $result = array();

        if ($this->url->class && !($this instanceof $this->url->class)) {
            $parser = new $this->url->class((string) $this->url, $this->card);
        } else {
            $parser = $this;
        }

        // $parser->init();
        // $parser->getContent();
        // $parser->getDom();

        $parser->getCards();
        $results = collect([]);
        $cards = $parser->cards;
        foreach ($cards as $key => $card) {
            set_time_limit(0);
            $parser = new $parser($card['url'], $card['block'] ?? null);
            $results = $results->push($parser->parseOne());
        }

        return $results;
    }

    protected function getContent($params = [])
    {
        $options = [
            CURLOPT_TIMEOUT => 90,
            CURLOPT_HEADER => false,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_URL => $this->url,
            CURLOPT_USERAGENT => getenv('HTTP_USER_AGENT') 
                ?: 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36',
            CURLOPT_COOKIEFILE => storage_path('app/parsers/cookies.txt'),
            CURLOPT_COOKIEJAR => storage_path('app/parsers/cookies.txt'),
            CURLOPT_RETURNTRANSFER => true,
        ] + $params;

        $ch = curl_init();
        curl_setopt_array($ch, $options);

        $content = curl_exec($ch);
        $error = curl_error($ch);
        $info = curl_getinfo($ch);

        curl_close($ch);

        if(!$content) {
            throw new \Exception($error);
        }

        new Authentication($content, $this->host);

        switch ($httpCode = $info['http_code']) {
            case 200:
                break;
            case 404:
                throw new \Exception('Страница ' . $info['url'] . ' не доступна', $httpCode);
                break;
            default:
                throw new \Exception('Неожиданный код ответа HTTP: ' . $httpCode, $httpCode);
        }

        $this->content = mb_convert_encoding($content, 'HTML-ENTITIES', 'UTF-8');
        return $this;
    }

    protected function getDom()
    {
        $dom = new \DOMDocument('1.0', 'UTF-8');
        @$dom->loadHTML($this->content);
        $this->content = new \DOMXPath($dom);
        return $this;
    }

    public function sortEuroSize($a, $b)
    {
        $sizeA = $a['size'] ? strtolower($a['size']) : 999;
        $sizeB = $b['size'] ? strtolower($b['size']) : 999;

        $euroSizes = array('4xs', '3xs', '2xs', 'xs', 's', 'm', 'l', 'xl', '2xl', '3xl', '4xl', '5xl', '6xl', '7xl', '8xl', '9xl', '10xl', '11xl', '12xl');

        if ($sizeA == $sizeB) { return 0; }

        if (in_array($sizeA, $euroSizes) && in_array($sizeB, $euroSizes)) {
            return array_search($sizeA, $euroSizes) < array_search($sizeB, $euroSizes) ? -1 : 1;
        }
        return $sizeA < $sizeB ? -1 : 1;
    }
}
