<?php

namespace App\Crm;

use App\Crm;

trait Crud
{
    private static $crm = null;
    private static $methodGroup = null;

    public function getClient()
    {
        if (!isset(static::$crm)) {
            static::$crm = new Crm;
        }

        return static::$crm;
    }

    public function getMethodGroup()
    {
        if (!isset(static::$methodGroup)) {
            $reflect = new \ReflectionClass(static::class);
            static::$methodGroup = strtolower($reflect->getShortName());
        }

        return static::$methodGroup;
    }

    public function list($order = array(), $filter = array(), $select = array(), $start = 0)
    {
        $method = sprintf('crm.%s.list', $this->getMethodGroup());
        $result = $this->getClient()->request(
            $method,
            array(
                'order'  => $order,
                'filter' => $filter,
                'select' => $select,
                'start'  => $start
            )
        );

        return $result;
    }

    public function fields()
    {
        $method = sprintf('crm.%s.fields', $this->getMethodGroup());
        $result = $this->getClient()->request(
            $method
        );

        return $result;
    }

    public function add($fields = [])
    {
        $method = sprintf('crm.%s.add', $this->getMethodGroup());
        $result = $this->getClient()->request(
            $method,
            array('fields' => $fields)
        );

        return $result;
    }

    public function get($id)
    {
        $method = sprintf('crm.%s.get', $this->getMethodGroup());
        $result = $this->getClient()->request(
            $method,
            ['id' => $id]
        );

        return $result;
    }

    public function update($id, $fields)
    {
        $method = sprintf('crm.%s.update', $this->getMethodGroup());
        $result = $this->getClient()->request(
            $method,
            [
                'id' => $id,
                'fields' => $fields
            ]
        );

        return $result;
    }

    public function delete($id)
    {
        $method = sprintf('crm.%s.delete', $this->getMethodGroup());
        $result = $this->getClient()->request(
            $method,
            ['id' => $id]
        );

        return $result;
    }

}
