<?php

namespace App\Crm;

use App\Crm;
use App\Crm\Crud;

class Invoice
{
    use Crud;

    public function getExternalLink($id)
    {
        $result = $this->getClient()->request(
            'crm.invoice.getexternallink',
            array(
                'id' => $id
            )
        );
        
        return $result;
    }
}
