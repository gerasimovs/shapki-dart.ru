<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    protected $viewIndex = 'admin.categories.index';
    protected $viewShow = 'admin.products.index';
    protected $viewCreate = 'admin.categories.create';
    protected $viewEdit = 'admin.categories.edit';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::withDepth()->defaultOrder();

        if (request('delete') == 'true') {
            $categories = $categories->onlyTrashed();
        }

        $categories = $categories->get()->toTree();

        return view($this->viewIndex, compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::defaultOrder()->get()->groupBy('category_id');
        $category = new Category;

        return view('admin.categories.create', compact('categories', 'category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = $this->fill($request, new Category());

        return redirect()->route('admin.categories.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        $products = $category->trashedProducts()->with('category', 'images');

        if (request('sorted')) {
            $products = $products->latest(request('sorted'));
        } else {
            $products = $products->latest('sorted_at');
        }

        $products = $products->paginate(96);

        $categories = Category::defaultOrder()->get()->groupBy('category_id');

        return view($this->viewShow, compact('products', 'categories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        return view($this->viewEdit, [
            'category' => $category,
            'categories' => Category::defaultOrder()->get()->groupBy('category_id'),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        $this->fill($request, $category);

        return redirect()->route('admin.categories.edit', $category->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        $category->delete();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function recovery(Category $category)
    {
        $category->restore();
        return redirect()->back();
    }

    private function fill(Request $request, Category $category)
    {
        $category->name = $request->name;
        $category->description = $request->description;
        $category->category_id = $request->category_id;
        $category->status_id = $request->status_id;
        $category->save();

        return $category;
    }

    public function up(Request $request, Category $category)
    {
        if ($request->has('position') && $request->position == 'top') {
            if ($category->isRoot()) {
                $category->toTop();
            } else {
                $category->prependToNode($category->parent)->save();
            }
        } else {
            $category->up();
        }

        return redirect()->back();
    }

    public function down(Request $request, Category $category)
    {
        if ($request->has('position') && $request->position == 'bottom') {
            if ($category->isRoot()) {
                $category->toBottom();
            } else {
                $category->appendToNode($category->parent)->save();
            }
        } else {
            $category->down();
        }

        return redirect()->back();
    }
}
