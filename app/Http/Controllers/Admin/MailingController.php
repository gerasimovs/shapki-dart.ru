<?php

namespace App\Http\Controllers\Admin;

use App\Mailing;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class MailingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mailings = Mailing::paginate(request('count') ?? 96);
        return view('admin.mailings.index', compact('mailings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.mailings.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $mailing = new Mailing();
        $this->update($request, $mailing);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Mailing  $mailing
     * @return \Illuminate\Http\Response
     */
    public function show(Mailing $mailing)
    {
        $view = new \App\Mail\ProductsNew($mailing);
        return $view->render();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Mailing  $mailing
     * @return \Illuminate\Http\Response
     */
    public function edit(Mailing $mailing)
    {
        return view('admin.mailings.edit', compact('mailing'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mailing  $mailing
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Mailing $mailing)
    {
        $mailing = $this->fill($request, $mailing);
        $mailing->save();

        if ($request->products) {
            switch ($request->products) {
                case 'latest':
                    $products = Product::latest()->take($request->products_latest ?: 12)->get();
                    break;
                
                case 'selected':
                    # code...
                    break;
            }

            $mailing->products()->sync($products->pluck('id'), false);
        }

        return redirect()->route('admin.mailings.edit', $mailing);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Mailing  $mailing
     * @return \Illuminate\Http\Response
     */
    public function destroy(Mailing $mailing)
    {
        //
    }

    public function preview(Request $request)
    {
        $mailing = new Mailing;
        $mailing = $this->fill($request, $mailing);
        return $this->show($mailing);
    }

    public function prepare(Request $request, Mailing $mailing)
    {
        return view('admin.mailings.prepare', compact('mailing'));
    }

    public function send(Request $request, Mailing $mailing)
    {
        $users = $mailing->users;
        $error = null;

        try {
            foreach ($users as $user) {
                Mail::to($user)->send(new \App\Mail\ProductsNew($mailing));
            }
        } catch (\Exception $e) {
            return redirect()->route('admin.mailings.prepare', $mailing->id)->withErrors($error);
        }

        return redirect()->route('admin.mailings.index')->with('status', 'Рассылка отправлена');
    }

    /* Helpers */

    private function fill(Request $request, Mailing $mailing)
    {
        $mailing->subject = $request->subject;
        $mailing->greeting = $request->greeting;
        $mailing->message = $request->message;
        $mailing->salutation = $request->salutation;

        return $mailing;
    }
}
