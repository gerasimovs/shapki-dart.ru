<?php

namespace App\Http\Controllers\Admin;

use App\Image;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ImageController extends Controller
{
    /**
     * undocumented class variable
     *
     * @var string
     **/
    private $indexView = 'admin.images.index';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $images = Image::with('products');
        $perPage = request('count') ?? 96;

        if (request('used') == 'false') {
            $images = $images->has('products', 0);
        }

        if (request('table') == 'true') {
            $this->indexView = 'admin.images.table';
        }

        $images = $images->orderBy('id', 'desc')->paginate($perPage);
        return view($this->indexView, compact('images'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function show(Image $image)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function edit(Image $image)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Image $image)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Image  $image
     * @return \Illuminate\Http\Response
     */
    public function destroy(Image $image)
    {
        $image->delete();
        return back();
    }


    public function uploads(Request $request)
    {
        return view('admin.images.uploads');
    }

    public function stores(Request $request)
    {
        $files = $request->file('images') ?? [$request->file('image')] ?? [];
        $localHost = parse_url(config('app.url'))['host'];
        $disk = 'images';

        $images = array();
        foreach ($files as $file) {
            $hash = sha1_file($file->getRealPath());
            $image = Image::firstOrNew(['hash' => $hash]);
            if ($image->exists) {
                $url = parse_url($image->url);
                if (isset($url['host']) && $url['host'] != $localHost) {
                    $extension = $file->extension();
                    $path = substr($hash, 0, 2) . '/' . substr($hash, 2, 2);
                    $url = $hash . '.' . $extension;
                    $url = $file->storeAs($path, $url, $disk);
                    $image->upload_url = $image->url;
                    $image->url = config('app.url') . '/images/' . $url;
                    $image->save();
                } elseif (!isset($url['host'])) {
                    $image->url = config('app.url') . $image->url;
                    $image->save();
                }
                $image->upload_url = $file->getClientOriginalName();
            } else {
                $extension = $file->extension();
                $path = substr($hash, 0, 2) . '/' . substr($hash, 2, 2);
                $url = $hash . '.' . $extension;
                $url = $file->storeAs($path, $url, $disk);
                $image->url = config('app.url') . '/images/' . $url;
                $image->upload_url = $file->getClientOriginalName();
                $image->save();
            }

            $images[] = $image;
        }

        return view('admin.images.resault', compact('images'));

        /*$disk = Storage::disk('images');

        foreach ($request->file('file-list') as $file) {

            $filename = Filename::first();

            $disk->putFile('', $file);

            Image::create([
                'filename' => $filename->name,
                'title' => $file->getClientOriginalName(),
                'extension' => $file->guessClientExtension(),
                'size' => $file->getClientSize(),
                'mime' => $file->getClientMimeType(),
                'hash' => md5_file($file->getRealPath()),
            ]);

            $filename->delete();
        }*/
    }
}
