<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

class NotificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        $notifications = $request->user()
            ->unreadNotifications()
            ->when($request->get('filter') === 'available', function ($query) {
                $query->where('data->status', 'primary');
            })
            ->when($request->get('filter') === 'no_available', function ($query) {
                $query->where('data->status', 'danger');
            })
            ->when($request->get('filter') === 'price', function ($query) {
                $query->where('data->status', 'info');
            })
            ->paginate(96);

        return view('admin.notifications.index')
            ->with('notifications', $notifications);
    }
}
