<?php

namespace App\Http\Controllers\Admin;

use App\Invite;
use App\Role;
use App\Notifications\InviteCreated;
use App\Http\Requests\EmailForRegistration;
use Illuminate\Http\Request;

class InviteController extends \App\Http\Controllers\Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invites = Invite::with('user')->get();
        return view('admin.invites.index', compact('invites'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected function create()
    {
        $roles = Role::pluck('name', 'id');
        return view('admin.invites.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function store(EmailForRegistration $request)
    {
        $invite = $request->user()->invites()->create([
            'email' => $request->email,
            'role_id' => $request->role_id,
            'token' => str_random(),
        ]);

        return back()
            ->with('status', 'Приглашение для пользователя ' . $invite->email . ' добавлено');;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Invite  $invite
     * @return \Illuminate\Http\Response
     */
    public function edit(Invite $invite)
    {
        $roles = Role::pluck('name', 'id');
        return view('admin.invites.edit', compact('invite', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Invite  $invite
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Invite $invite)
    {
        $invite->role_id = $request->role_id;
        $invite->save();

        return back()
            ->with('status', 'Приглашение для пользователя ' . $invite->email . ' изменено');
    }

    public function send(Invite $invite)
    {
        $invite->notify(new InviteCreated());

        return back()
            ->with('status', 'Приглашение для пользователя ' . $invite->email . ' отправлено');
    }

}
