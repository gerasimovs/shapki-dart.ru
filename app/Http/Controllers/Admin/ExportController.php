<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExportController extends Controller
{
    public function toWord(int $category)
    {
        $category = \App\Category::select('id', 'name')
            ->with([
                'products' => function ($query) {
                    $query->select('id', 'category_id', 'vendor_code', 'price')
                        ->orderBy('created_at', 'desc')
                        ->with('options');
                }, 
            ])->find($category)
            /*->toArray()*/;

        header("Content-Description: File Transfer");
        header('Content-Disposition: attachment; filename=' . str_slug($category->name) . '.docx');
        header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Expires: 0');
            
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();

        $section->addTitle($category->name);

        $table = $section->addTable();
        foreach ($category->products as $product) {
            $table->addRow();
            $table->addCell()->addText($product->vendor_code);
            $image = $product->images()->value('url');
            if ($image) {
                $table->addCell()->addImage($product->images->value('url'));
            } else {
                $table->addCell();
            }
        }

        // Saving the document as OOXML file...
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');

        // Download the produced file automatically
        $xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $xmlWriter->save('php://output');
        exit;
    }

    public function categoryToCsv(Category $category)
    {
        $category->load(['products' => function ($query) {
            $query->with('images', 'options')->latest('id');
        }]);

        header('Content-Description: File Transfer');
        header('Content-Disposition: attachment; filename=' . str_slug($category->name) . '.csv'); 
        header('Content-Type: text/csv'); 
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Expires: 0');

        $out = fopen('php://output', 'w');
        fputs($out, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
        fputcsv($out, [
            'Коллекция',
            'Артикул',
            'Название',
            'Подробнее',
            'Цена',
            'Размеры',
            'Картинка',
        ], ';');
        foreach ($category->products as $product) {
            $images = $product->images->pluck('url')->toArray();
            $line = [
                $category->name,
                $product->vendor_code,
                $product->name,
                $product->description,
                (int) $product->price,
                trim($product->options->implode('name', ', '), ', '),
            ];

            fputcsv($out, array_merge($line, $images), ';');
        }

        if ($category->children) {
           foreach ($category->children as $children) {
               foreach ($children->products as $product) {
                    $images = $product->images->pluck('url')->toArray();
                    $line = [
                        $children->name,
                        $product->vendor_code,
                        $product->name,
                        $product->description,
                        (int) $product->price,
                        trim($product->options->implode('name', ', '), ', '),
                    ];

                    fputcsv($out, array_merge($line, $images), ';');
                }
            }
        }

        fclose($out);
        exit;
    }

    public function productsToCsv(Request $request)
    {
        header('Content-Description: File Transfer');
        header('Content-Disposition: attachment; filename=products.csv'); 
        header('Content-Type: text/csv'); 
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Expires: 0');

        $out = fopen('php://output', 'w');
        fputs($out, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
        fputcsv($out, [
            'Коллекция',
            'Артикул',
            'Название',
            'Подробнее',
            'Цена',
            'Размеры',
            'Картинка',
        ], ';');

        foreach ($request->products ?? [] as $productId) {
            $product = \App\Product::find($productId);
            $images = $product->images->pluck('url')->toArray();
            $line = [
                $product->category->name,
                $product->vendor_code,
                $product->name,
                $product->description,
                (int) $product->price,
                trim($product->options->implode('name', ', '), ', '),
            ];

            fputcsv($out, array_merge($line, $images), ';');
        }

        fclose($out);
        exit;
    }

    public function productsToWord(Request $request)
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();

        $table = $section->addTable();
        foreach ($request->products ?? [] as $key => $productId) {
            $product = \App\Product::withTrashed()->with('images')->find($productId);
            $table->addRow();
            $table->addCell()->addText($product->vendor_code);
            foreach ($product->images as $index => $image) {
                if ($index > 0) {
                    $table->addRow();
                    $table->addCell();
                }

                $table->addCell()->addImage(
                    $image->url,
                    ['width' => 360]
                );
            }
        }
        // Saving the document as OOXML file...
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');

        // Download the produced file automatically
        $xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');

        // Render
        header("Content-Description: File Transfer");
        header('Content-Disposition: attachment; filename=products.docx');
        header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Expires: 0');

        $xmlWriter->save('php://output');
        exit;
    }

}
