<?php

namespace App\Http\Controllers\Admin;

use Storage;
use App\Import;
use App\Http\Requests\FileRequired;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ImportController extends Controller
{
    private $handle;
    private $fields;
    private $errors = [];
    private $success = [];

    public function __construct()
    {
    }

    public function index()
    {
        $imports = Import::latest('id')->paginate(request('count') ?? 12);

        return view('admin.import.index', compact('imports'));
    }

    public function show(Import $import)
    {
        $fields = config('import.imported.' . $import->type);

        return view('admin.import.parse', [
            'import' => $import,
            'header' => $import->headers,
            'file' => $import->file,
            'fields' => $fields,
            'imported' => $import->type,
        ]);
    }

    public function parse(FileRequired $request)
    {
        /* Setting options for parsing */
        $this->length = config('import.csv.length');
        $this->delimiter = $request->input('delimiter') ?? config('import.csv.delimiter');
        $this->enclosure = $request->input('enclosure') ?? config('import.csv.enclosure');
        $this->escape = $request->input('escape') ?? config('import.csv.escape');
        $type = $request->input('fields') ?? 'products';
        $fields = config('import.imported.' . $type);
        /* @todo Вывод ошибок и ограничение в форме на количество символов */

        /* Retrieving data from a file */
        $upload_file = $request->file('file')->getClientOriginalName();
        $filePath = $request->file('file')->getRealPath();
        $handle = fopen($filePath, 'r');

        $data = array();
        while (($row = $this->getCsv($handle)) !== false) {
            if (false == array_filter($row)) {
               continue;
            }
            $data[] = $row;
        }

        fclose($handle);

        /* Data сonversion to UTF-8 */
        $this->in_charset = config('import.csv.in_charset');
        $this->convertUtf8($data);

        /* Get data headers */
        if ($request->has('header')) {
            $headers = array_shift($data);
        } else {
            $headers = $data[0];
        }

        /* Get data in reverse order */
        if ($request->has('sorting')) {
            $data = array_reverse($data);
        }

        /* Save file for next request */
        $fileData = json_encode($data);
        $file = 'json/' . time() . '_data.json';
        $newPath = Storage::disk('local')->put($file, $fileData);

        /* Save model Import */
        $import = Import::create(compact(
            'headers', 'file', 'upload_file', 'type'
        ));

        /* Redirect to show */
        return redirect()->route('admin.imports.show', $import->id);
    }

    public function store(Request $request, Import $import)
    {
        $imported = $request->input('imported');
        
        switch ($imported) {
            case 'order':
                $this->ordersImport($request);
                break;
            default:
                $this->productsImport($request);
                break;
        }

        return redirect()
            ->route('admin.import.finish')
            ->with('danger', $this->errors)
            ->with('success', $this->success);
    }

    public function check(Request $request)
    {
        $file = Storage::disk('local')->get($request->input('file'));
        $productsData = json_decode($file);

        $products = array();
        $notExists = array();

        $fields = $request->input('fields');
        foreach ($productsData as $row) {
            $productData = $this->userArrayCombine($fields, $row);
            $productVendorCode = $productData['vendor_code'];

            $product = \App\Product::where('vendor_code', $productVendorCode)->withTrashed()->first();

            if (is_null($product)) {
                $notExists[] = 'Товар с артикулом ' . $productVendorCode . ' не существует';
            } else {
                $products[] = $product;
            }

        }

        return redirect()
            ->route('admin.import.finish')
            ->with('danger', $notExists)
            ->with('products', $products);
    }

    private function productsImport($request)
    {
        /* Get json and decode */
        $file = Storage::disk('local')->get($request->input('file'));
        $products = json_decode($file);

        $fields = $request->input('fields');
        foreach ($products as $row) {
            $productData = $this->userArrayCombine($fields, $row);

            if ($request->checkDelete) {
                \App\Product::where('vendor_code', $productData['vendor_code'])->delete();
                $this->success[] = 'Товар ' . $productData['vendor_code'] . ' удален';
                continue;
            }

            if ($request->checkRename) {
                $old_product = \App\Product::withTrashed()->where('vendor_code', $productData['vendor_code'])->first();
                $new_product = \App\Product::withTrashed()->where('vendor_code', $productData['new_vendor_code'])->first();
                if ($new_product) {
                    $this->errors[] = 'Товар с артикулом ' . $productData['new_vendor_code'] . ' уже существует';
                    continue;
                }
                if (!$old_product) {
                    $this->errors[] = 'Товар с артикулом ' . $productData['vendor_code'] . ' не существует';
                    continue;
                }
                $old_product->vendor_code = $productData['new_vendor_code'];
                $old_product->save();
                continue;
            }

            $newData = array();


            $newData['name'] = $productData['name'] ?? null;
            $newData['description'] = $productData['description'] ?? null;

            // Добавление продукта

            $productVendorCode = $productData['vendor_code'];
            $product = \App\Product::where('vendor_code', $productVendorCode)->withTrashed()->first();

            if ($product == false) {
                if ($request->checkOnlyUpdate) {
                    $this->errors[] = 'Товар с артикулом ' . $productVendorCode . ' не существует';
                    continue;
                } else {
                    $product = new \App\Product();
                    $isNew = true;
                }
            } else {
                $isNew = false;
            }

            if ($product->trashed()) {
                if ($request->checkNotRestore) {
                    $this->errors[] = 'Товар с артикулом ' . $productVendorCode . ' не восстановлен';
                    continue;
                } else {
                    $product->restore(); 
                }
            }

            if ($isNew) {
                $product->vendor_code = $productVendorCode;
            }

            if ($isNew || $request->checkDescription) {
                $product->fill($newData);
            }

            if ($isNew || $request->checkPrice) {
                $product->price = $productData['price'] ?? null;
            }

            if ($isNew || $request->checkCategory) {
                $product->category_id = isset($productData['category']) 
                    ? $this->getCategoryId($productData['category']) 
                    : null;
            }

            if (!empty($productData['unit_id']) && array_key_exists($productData['unit_id'], $product->getUnits())) {
                $product->unit_id = $productData['unit_id'];
            }

            $product->updated_at = now();
            $product->save();

            if($isNew) {
                $this->success[] = 'Товар с артикулом ' . $productVendorCode . ' добавлен';
            } else {
                if ($request->checkOnlyUpdate) {
                    $this->success[] = 'Товар с артикулом ' . $productVendorCode . ' обновлен';
                } else {
                    $this->errors[] = 'Товар с артикулом ' . $productVendorCode . ' уже существует';
                }
            }

            if (isset($productData['categories'])) {
                $this->setCategories($product, $productData['categories']);
            }

            if (($isNew || $request->checkOptions) && isset($productData['options'])) {
                $this->setOptions($product, $productData['options']);
            }

            if (($isNew || $request->checkImages) && isset($productData['images'])) {
                $this->setImages($product, $productData['images']);
            }

            if (($isNew || $request->checkSuppliers) && isset($productData['suppliers_name'])) {
                $this->setSuppliers($product, [
                    'name' => $productData['suppliers_name'],
                    'url' => $productData['suppliers_url'] ?? null,
                    'price' => $productData['suppliers_price'] ?? null,
                ], $request->has('checkSuppliersReplace'));
            }
        }
    }

    private function ordersImport($request)
    {
        $file = Storage::disk('local')->get($request->input('file'));
        $data = json_decode($file);
        $fields = $request->input('fields');

        $offers = array();
        $totalQty = 0;
        $totalPrice = 0;
        foreach ($data as $row) {
            $productData = $this->userArrayCombine($fields, $row);
            $product = $this->getProduct($productData, false);
            $option = \App\Option::firstOrCreate(['name' => $productData['options']]);
            $offer = \App\Offer::firstOrCreate([
                'product_id' => $product->id,
                'option_id' => $option->id,
            ], ['status' => false]);
            $price = intval($productData['price'] ?? $product->price);
            $quantity = intval($productData['quantity'] ?? 1);

            if (array_key_exists($offer->id, $offers)) {
                $offers[$offer->id]['qty'] = $offers[$offer->id]['qty']  + $quantity;
            } else {
                $offers[$offer->id] = [
                    'product_id' => $product->id,
                    'option_id' => $option->id,
                    'vendor_code' => $productData['vendor_code'],
                    'price' => $price,
                    'color' => $productData['options'],
                    'qty' => $quantity,
                ];
            }

            $totalQty += $quantity;
            $totalPrice += ($price * $quantity);
        }

        $order = new \App\Order;
        $order->user_id = $request->user()->id;
        $order->totalQty = $totalQty;
        $order->totalPrice = $totalPrice;

        $order->save();
        $order->offers()->sync($offers);
        
        $this->success[] = 'Заказ импортирован';
    }

    public function finish()
    {
        return view('admin.import.result', [
            'products' => session('products') ?? [],
            'danger' => session('danger') ?? [],
            'success' => session('success') ?? [],
        ]);
    }


    /*
     * TODO: Вынести в модель
     */

    private function setOptions(\App\Product $product, $options, $delimiter = ',')
    {
        $ids = array();
        $data = is_array($options) ? $options : explode($delimiter, $options);
        foreach ($data as $name) {
            if (empty($name)) { continue; }
            
            $option = \App\Option::firstOrCreate(['name' => trim($name)]);
            $ids[] = $option->id;
        }

        $product->options()->syncWithoutDetaching($ids);

        $product->options->each(function($option) use($ids) {
            $option->offer->status = in_array($option->id, $ids);
            $option->offer->save();
        });   
    }

    private function setImages(\App\Product $product, $images, $delimiter = ',')
    {
        $imagesIds = array();
        $localHost = parse_url(config('app.url'))['host'];
        $disk = 'images';

        $sort = 1;

        $imagesData = is_array($images) ? $images : explode($delimiter, $images);
        foreach ($imagesData as $imageFields) {
            $fields = explode('#', $imageFields, 2);

            $image = app(\App\Services\ImageService::class)->firstOrCreate($fields[0]);

            if (!$image) {
                $this->errors[] = 'Файл ' . $fields[0] . ' не удалось скопировать';
                continue;
            }

            if (array_key_exists($image->id, $imagesIds)) {
                continue;
            }

            $imagesIds[$image->id] = [
                'sort' => $sort++,
                'description' => $fields[1] ?? null,
            ];

            $this->success[] = 'Файл ' . $fields[0] . ' добавлен';
        }

        $product->images()->sync($imagesIds);
    }

    private function getCategoryId($name, $restore = true)
    {
        $name = trim($name);

        if (empty($name)) {
            return null;
        }

        $category = \App\Category::where('name', $name)->withTrashed()->first() ?? new \App\Category;

        if ($category->exists == false) {
            $category->name = $name;
            $category->save();
        } elseif ($category->trashed() && $restore) { 
            $category->restore(); 
        }

        return $category->id;
    }

    private function setCategories(\App\Product $product, $categories, $delimiter = '|')
    {
        $data = is_array($categories) ? $categories : explode($delimiter, $categories);

        $ids = collect($data)->map(function ($name) {
                if (empty($name)) {
                    return null;
                }
                return $this->getCategoryId($name);
            })->reject(function ($category) { 
                return empty($category);
            });

        $product->categories()->sync($ids->toArray());
    }

    private function setSuppliers(\App\Product $product, $data, $detaching)
    {
        $delimiter = ',';
        $suppliersName = is_array($data['name']) ? $data['name'] : explode($delimiter, $data['name']);
        unset($data['name']);

        $ids = collect($suppliersName)->map(function ($name) {
            $name = trim($name);
            return empty($name) 
                ? null 
                : \App\Supplier::select('id')->firstOrCreate(['name' => $name]);
        })->reject(function ($supplier) { 
            return empty($supplier);
        })->mapWithKeys(function ($supplier) use ($data) { 
            if (array_filter($data)) {
                return [$supplier->id => $data];
            } else {
                return [$supplier->id];
            }
        })->toArray();

        $product->suppliers()->sync($ids, $detaching);
    }

    private function getProduct($data, $restore = true)
    {
        $product = \App\Product::where('vendor_code', $data['vendor_code'])->withTrashed()->first() ?? new \App\Product;
        
        if ($product->trashed() && $restore) { 
            $product->restore();
        }

        if ($product->exists == false || request('checkDescription')) {
            $product->vendor_code = $data['vendor_code'];
            $product->name = $data['name'] ?? null;
            $product->price = $data['price'] ?? 0;
            $product->description = $data['description'] ?? null;
            $product->category_id = $this->getCategoryId($data['category'] ?? 0, $restore);
            //$product->deleted_at = now();
        }

        $product->save();

        return $product;
    }





    /*
     * TODO: Вынести в сервисы
     */

    private function convertUtf8(array &$array)
    {
        array_walk($array, function (&$entry) {
            if (is_array($entry)) {
                $this->convertUtf8($entry);
            } else {
                $entry = trim(iconv($this->in_charset, 'UTF-8', $entry));
            }
        });
    }


    private function getCsv(&$handle)
    {
        return fgetcsv($handle, $this->length, $this->delimiter, $this->enclosure, $this->escape);
    }


    private function userArrayCombine(array $keys, array $values)
    {
        $result = array();
        foreach ($keys as $i => $k) {
            $result[$k][] = $values[$i];
        }

        array_walk($result, function(&$value) {
            if(count($value) == 1) {
                $value = array_pop($value);
            } else {
                $value = array_filter($value, function ($value) {
                    return !empty($value);
                });
            }
        });

        return $result;
    }

}
