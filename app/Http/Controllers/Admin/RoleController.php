<?php

namespace App\Http\Controllers\Admin;

use App\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();
        return view('admin.roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categoriesGroup = \App\Category::withCount('children')->orderBy('sort')->get()->groupBy('category_id');
        return view('admin.roles.create', compact('categoriesGroup'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $role = new Role;
        $this->update($request, $role);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        $role->load(['categories' => function($query) {
            $query->select('id');
        }]);
        $categoriesGroup = \App\Category::withCount('children')->orderBy('sort')->get()->groupBy('category_id');
        return view('admin.roles.edit', compact('role', 'categoriesGroup'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        $role->name = $request->name;
        $role->slug = $request->slug;
        $role->description = $request->description;
        $role->save();

        $categories = $this->categoriesFilter($request->categories);
        $role->categories()->sync($categories);

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        //
    }

    private function categoriesFilter($categories)
    {
        $filtredCategories = [];

        foreach ($categories as $key => $value) {
            if (isset($value['checked']) && $value['checked'] == '1') {
                $filtredCategories[$key] = ['description' => $value['description']];
            }
        }

        return $filtredCategories;
    }
}
