<?php

namespace App\Http\Controllers\Admin;

use App\Category;
use Illuminate\Http\Request;

class HelperController
{

    public function index()
    {
        return view('admin.helpers.index');
    }


    public function combine(Request $request)
    {
        $combine = array();
        $sizes = explode(',', $request->sizes);
        $colors = explode(',', $request->colors);

        foreach ($sizes as $size) {
            foreach ($colors as $color) {
                $combine[] = trim($size) . ' ' . $color;
            }
        }

        return implode(', ', $combine);
    }

    public function fromWord(Request $request)
    {

        $reader = \PhpOffice\PhpWord\IOFactory::createReader('Word2007');
        $docFile = $request->file('word')->getRealPath();
        $phpDoc = $reader->load($docFile);

        $rows = array();
        foreach ($phpDoc->getSections() as $section) {
            foreach ($section->getElements() as $element) {
                if($element instanceof \PhpOffice\PhpWord\Element\Table){
                    $rows = array_merge($rows, $element->getRows());
                }
            }
        }

        echo "<table>";
        foreach ($rows as $key => $row) {
            echo "<tr>";
            $cells = $row->getCells();

            $productData = array();
            $productOptions = array();
            $productPrice = '';
            $productModel = '';
            foreach($cells[0]->getElements() as $element) {
                $elementsText = ''; 
                
                if($element instanceof \PhpOffice\PhpWord\Element\TextRun) {
                    foreach($element->getElements() as $text) {
                        $elementsText .= $text->getText();
                    }
                }
                else if($element instanceof \PhpOffice\PhpWord\Element\Text) {
                    $elementsText .= $element->getText();
                }
                else if($element instanceof \PhpOffice\PhpWord\Element\ListItem) {
                    $productOptions[] = trim($element->getText(), ' .');
                }

                $elementsText = trim($elementsText, ' .');

                if (empty($elementsText)) {
                    continue;
                }

                if (strpos($elementsText, 'Цена:') === 0) {
                    $productPrice = $elementsText;
                    continue;
                }

                if (strpos($elementsText, 'Модель:') === 0) {
                    $productModel = $elementsText;
                    continue;
                }

                if (strpos($elementsText, 'Описание:') === 0) {
                    $elementsText = trim(mb_substr($elementsText, 9));
                }

                $productData[] = $elementsText;
            }

            echo "<td>";
            echo trim(str_replace('Модель:', '', $productModel), ' .');
            echo "</td>";
            echo "<td>";
            echo str_replace('Цвета:', '', implode('. ', $productData));
            echo "</td>";
            echo "<td>";
            echo implode(', ', $productOptions);
            echo "</td>";
            echo "<td>";
            echo str_replace(array('Цена: ', ' руб'), '', $productPrice);
            echo "</td>";


            // $productImage = $cells[1]->getElements();

            echo "</tr>";
        }
        echo "</table>";
    }
}