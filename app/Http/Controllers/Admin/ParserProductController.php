<?php

namespace App\Http\Controllers\Admin;

use App\ProductSupplier;
use App\Jobs\ProcessParser;
use App\Services\ParserService;
use App\Services\ProductService;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;

class ParserProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $parsers = ProductSupplier::query()
            ->select(['product_supplier.*', 'products.updated_at as product_updated_at'])
            ->join('products', 'products.id', 'product_supplier.product_id')
            ->with('product')
            ->has('product')
            ->hasExternalUrl()
            ->oldest('products.updated_at')
            ->paginate(request('count', 96));

        return view('admin.products.parsers.index', compact('parsers'));
    }

    /**
     * Display the specified resource.
     *
     * @param  Request          $request
     * @param  ProductSupplier  $parser
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, ProductSupplier $parser)
    {
        return view('admin.products.parsers.edit', compact('parser'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request          $request
     * @param  ProductSupplier  $parser
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductSupplier $parser)
    {
        if ($request->has('parser.url') && $url = filter_var($request->input('parser.url'), FILTER_VALIDATE_URL, FILTER_FLAG_PATH_REQUIRED)) {
            if ($url != $parser->url) {
                $parser->url = $url;
                $this->setStatusMessage("Ссылка на парсер для товара <a href=\"{$parser->product->present()->route}\">{$parser->product->vendor_code}</a> обновлена");
            }
        }

        if ($request->has('parser.price')) {
            $oldPrice = $parser->price;
            if ($oldPrice != $parser->price) {
                $parser->price = (float) $request->input('parser.price');
                $this->setStatusMessage("Закупочная цена товара <a href=\"{$parser->product->present()->route}\">{$parser->product->vendor_code}</a> изменена с {$oldPrice} на {$parser->price}");
            }
        }

        $parser->save();

        return redirect()->route('admin.products.parsers.edit', $parser)
            ->with('status', $this->getStatusMessages());
    }

    /**
     * @param  ProductSupplier  $parser
     * @return \Illuminate\Http\Response
     */
    public function parse(ProductSupplier $parser, ProcessParser $job)
    {
        $statuses = $job->dispatchNow($parser);

        $response = redirect()->back();

        if (isset($statuses['danger'])) {
            $response = $response->with('danger', Arr::pluck($statuses['danger'], 'message'));
            unset($statuses['danger']);
        }

        if (!empty($statuses)) {
            $withStatus = [];

            foreach ($statuses as $status) {
                $withStatus = array_merge($withStatus, array_column($status, 'message'));
            }

            $response = $response->with('status', $withStatus);
        }

        return $response;
    }
}
