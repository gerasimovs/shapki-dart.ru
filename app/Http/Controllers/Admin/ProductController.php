<?php

namespace App\Http\Controllers\Admin;

use Storage;
use App\Product;
use App\Option;
use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Http\File;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $perPage = (int) $request->count ?: 96;

        $products = Product::with('category', 'images')->latest('sorted_at');
        $header = 'Все товары';
        
        if (request('photo') == 'false') {
            $products = $products->has('images', 0);
            $header = 'Товары без фотографий';
        }

        if (request('category') == 'false') {
            $products = $products->whereHas('category', function($query)
            {
                $query->onlyTrashed();
            });
            $header = 'Товары без категорий';
        }

        if ( request('delete', false) ) {
            if (request('delete') == 'true') {
                $products = $products->onlyTrashed();
                $header = 'Архивные товары';
            }
        } else {
            $products = $products->withTrashed();
        }

        if (request('supplier') == 'false') {
            $products = $products->has('suppliers', 0);
            $header = 'Товары без поставщиков';
        }

        $products = $products->paginate($perPage);
        $header .= ' (' . $products->total() . ' шт.)';

        return view('admin.products.index', [
            'header' => $header,
            'products' => $products,
            'categories' => Category::orderBy('sort')->get()->groupBy('category_id')
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Product $product = null)
    {
        if (is_null($product)) {
            $product = new Product;
        }

        $categories = Category::orderBy('sort')->get()->groupBy('category_id');

        return view('admin.products.create', compact('product', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(\App\Http\Requests\Admin\StoreProductsRequest $request)
    {
        return $this->update($request, new Product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $product->load(['images' => function ($query) {
            return $query->orderBy('image_product.sort','ASC');
        }]);
        $product->load('options', 'category', 'suppliers', 'related');

        return view('admin.products.edit', [
            'product' => $product,
            'categories' => Category::orderBy('sort')->get()->groupBy('category_id')
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        $errors = [];

        $product->vendor_code = $request->vendor_code;
        $product->name = $request->name;
        $product->price = $request->price;
        $product->description = $request->description;
        $product->unit_id = $request->unit_id;
        $product->status_id = $request->status_id;
        $product->category_id = $request->category_id;

        $product->save();

        /* Категории */
        
        if (isset($request->categories)) {
            $product->categories()->sync(array_filter($request->categories));
        }

        /* Опции */
        $options = array();

        if ($request->has('options_name') && $options = array_filter($request->options_name)) {
            $options = collect($options)->map(function ($name) {
                    return Option::firstOrCreate(['name' => mb_strtolower(trim($name))]);
                })->pluck('id')->toArray();

            $product->options()->syncWithoutDetaching($options);
        }

        $options = array_merge($options, $request->options ?? []);
        $product->options->each(function($option) use ($options) {
            $option->offer->status = in_array($option->id, $options);
            $option->offer->save();
        });

        /* Изображения */
        $images = $request->images ?? [];
        $disk = 'images';
        $localHost = parse_url(config('app.url'))['host'];

        if ($request->images_url) {
            foreach ($request->images_url as $upload_url) {
                set_time_limit(0);
                if (!$upload_url) {
                    continue;
                }
                $image = \App\Image::where('url', $upload_url)->orWhere('upload_url', $upload_url)->first();
                
                if ($image && parse_url($image->url)['host'] == $localHost) { 
                    $images[] = $image->id;
                    continue;
                }

                $tempFile = tempnam(sys_get_temp_dir(), 'tempImage');
                if (copy($upload_url, $tempFile)) {
                    $hash = sha1_file($tempFile);

                    if (!$image) {
                        $image = \App\Image::firstOrCreate(['hash' => $hash], ['url' => $upload_url]);
                    }

                    $file = new File($tempFile);
                    $extension = $file->extension();
                    $path = substr($image->hash, 0, 2) . '/' . substr($image->hash, 2, 2) . '/';
                    $url = $image->hash . '.' . $extension;
                    
                    $image->url = config('app.url') . '/images/' . Storage::disk($disk)->putFileAs($path, $file, $url);
                    $image->upload_url = $upload_url;
                    $image->save();
                    
                    $images[$image->id] = [
                        'sort' => count($images) + 1,
                    ];
                } else {
                    $errors[] = 'Файл ' . $upload_url . ' не удалось загрузить';
                }
            }
        }

        $product->images()->sync($images);

        /* Сопутствующие товары */
        if ($request->related_code) {
            $related = array();
            foreach ($request->related_code as $vendor_code) {
                $relatedId = \App\Product::where('vendor_code', $vendor_code)->value('id');
                if ($relatedId) {
                    $related[] = $relatedId;
                }
            }
            $product->related()->sync(array_merge($related, $request->related['id'] ?? []));
        }

        /* Поставщики */
        if ($request->suppliers) {
            $product->suppliers()->sync($request->suppliers);
        }
        

        return redirect()->route('admin.products.edit', $product)->withErrors($errors);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Product $product)
    {
        $product->delete();

        return redirect()->route('admin.products.edit', $product->id)->with('status', 'Товар ' . $product->vendor_code . ' удален!');
    }

    /**
     * Restore the specified resource to storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function restore(Request $request, Product $product)
    {
        $product->restore();

        return back()->with('status', 'Товар ' . $product->vendor_code . ' восстановлен!');;
    }



    protected function search(Request $request)
    {
        $products = Product::withTrashed()->whereRaw(
            'MATCH(vendor_code, name, description) AGAINST(? IN BOOLEAN MODE)',
            [$request->text])->orWhere('vendor_code', 'like', '%' . $request->text . '%')->withCount('images', 'options')->paginate($request->count ?? 96);
        $products->load('images');

        $categories = Category::orderBy('sort')->get()->groupBy('category_id');
        
        return view('admin.products.index', compact('products', 'categories'));
    }

    protected function moving(Request $request)
    {
        if ($request->products && $request->category_id) {
            foreach ($request->products as $productId) {
                Product::withTrashed()
                    ->find($productId)
                    ->category()
                    ->associate($request->category_id)
                    ->save()
                ;
            }
        }
        return back()->with('status', 'Товары перемещены');
    }

    protected function sorting(Product $product)
    {
        $product->sorted_at = now();
        $product->save();

        return back();
    }

    public function get(Request $request)
    {
        return $request->all();
    }

}
