<?php

namespace App\Http\Controllers\Admin;

use App\Crm;
use App\Order;
use App\Http\Controllers\Controller;

class CrmController extends Controller
{

    public function deal(Order $order)
    {
        return __METHOD__;
    }

    public function invoice(Order $order)
    {
        $crm = new Crm;
        
        // dd($crm->Invoice->fields()['result']);
        $ufOrderId = config('crm.user_fields.orderid_in_invoice');

        $invoices = $crm->Invoice->list(
            [],
            [$ufOrderId => $order->id],
            ['ID', $ufOrderId, 'UF_COMPANY_ID']
        );

        $products = array();

        foreach ($order->offers as $offer) {
            if (isset($products[$offer->product_id])) {
                $products[$offer->product_id]['QUANTITY'] += $offer->pivot->qty;
            } else {
                $products[$offer->product_id] = [
                    'PRICE' => (int) $offer->pivot->price,
                    'PRODUCT_NAME' => $offer->pivot->vendor_code,
                    'QUANTITY' => $offer->pivot->qty,
                    'VAT_RATE' => 'Без НДС',
                    'VAT_INCLUDED' => 'Y',
                    // "ID" => array:3 [▶]
                    // "PRICE" => array:4 [▶]
                    "PRODUCT_ID" => $offer->product_id,
                    // "VAT_RATE" => array:4 [▶]
                    // "VAT_INCLUDED" => array:4 [▶]
                    // "CATALOG_XML_ID" => array:3 [▶]
                    // "PRODUCT_XML_ID" => array:3 [▶]
                ];
            }
        }

        if ($invoices['total'] == 0) {
            $companies = $crm->Company->list(
                [],
                ['EMAIL' => $order->user->email],
                ['ID', 'EMAIL']
            );

            if ($companies['total'] == 0) {

                $fields = [
                    'TITLE' => $order->user ?: $order->user->name,
                    'COMPANY_TYPE' => 'CUSTOMER',
                    'INDUSTRY' => 'OTHER',
                    'CURRENCY_ID' => 'RUB',
                    'OPENED' => 'Y',
                    'EMAIL' => [
                        [
                            'TYPE' => 'WORK',
                            'VALUE' => $order->user->email
                        ]
                    ],
                    'PHONE' => [
                        [
                            'TYPE' => 'WORK',
                            'VALUE' => $order->user->phone
                        ]
                    ],
                ];

                $companyId = $crm->Company->add($fields)['result'];
            } else {

                $companyId = current($companies['result'])['ID'];
            }

            $fields = [
                // 'ACCOUNT_NUMBER' => $order->id,
                'ORDER_TOPIC' => 'Заказ #' . $order->id,
                'STATUS_ID' => 'N',
                'PRODUCT_ROWS' => $products,
                'UF_MYCOMPANY_ID' => $crm->company,
                'PERSON_TYPE_ID' => 2,
                'UF_COMPANY_ID' => $companyId,
                'PAY_SYSTEM_ID' => 1,
                'OPENED' => 'Y',
                $ufOrderId => $order->id,
            ];

            $invoiceId = $crm->Invoice->add($fields)['result'];
        } else {
            $invoiceId = current($invoices['result'])['ID'];
            $fields = [
                'PRODUCT_ROWS' => $products,
            ];

            $crm->Invoice->update(
                $invoiceId, 
                $fields
            );
        }

        $externalLink = $crm->Invoice->getExternalLink($invoiceId)['result'];
        return redirect()->to($externalLink);
    }
}
