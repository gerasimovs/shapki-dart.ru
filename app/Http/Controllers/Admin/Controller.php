<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller as BaseController;
use Illuminate\Http\Request;

class Controller extends BaseController
{


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $reflect = new \ReflectionClass($this);
        $modelClass = '\\App\\' . preg_replace('/Controller$/', '', $reflect->getShortName());
        if (class_exists($modelClass)) {
            $model = new $modelClass;
            $this->update($request, $model);
        }
    }

}
