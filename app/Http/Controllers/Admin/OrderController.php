<?php

namespace App\Http\Controllers\Admin;

use App\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::with('user')->orderBy('id', 'desc')->get();
        return view('admin.orders.index', compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        $totalQty = $order->totalQty;
        $totalPrice = $order->totalPrice;
        $offersGroup = collect($order->offers)->groupBy('product_id');
        return view('admin.orders.show', compact('offersGroup', 'order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        return view('admin.orders.edit', compact('order'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        $order->user_id = $request->user_id;
        $order->status = $request->status;
        $order->save();

        $order->offers()->sync($request->offers, false);

        return back()->with('status', 'Изменения сохранены');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }

    public function toWord(Order $order)
    {
        $order->load('offers.product.suppliers');

        $offers = $order->offers->map(function($offer) {
            $offer->supplier = $offer->product->suppliers->count() 
                ? $offer->product->suppliers->first()->name 
                : 'Без поставщика';
            return $offer;
        });

        $offersGroup = $offers->groupBy([
            'supplier',
            function ($item) {
                return $item['product_id'];
            },
        ], $preserveKeys = true);

        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $phpWord->setDefaultFontName('Calibri ');
        $phpWord->setDefaultFontSize(11);

        $sectionStyle = array(
            'marginTop' => '567',
            'marginLeft' => '567',
            'marginRight' => '567',
            'marginBottom' => '567',
        );
        $section = $phpWord->addSection($sectionStyle);

        $title = 'Заказ №' . $order->id . ' пользователя ' . $order->user->name;
        $section->addTitle($title, 1);

        // Create a new table style
        $tableStyle = new \PhpOffice\PhpWord\Style\Table;
        $tableStyle->setBorderColor('999999');
        $tableStyle->setBorderSize(6);
        $tableStyle->setUnit(\PhpOffice\PhpWord\Style\Table::WIDTH_PERCENT);
        $tableStyle->setWidth(100 * 50);
        $tableStyle->setCellMargin(80);

        foreach ($offersGroup as $supplier => $supplierOffers) {
            $section->addTitle($supplier, 2);
            $table = $section->addTable($tableStyle);
            $productIteration = 1;
            foreach ($supplierOffers as $offers) {
                if ($productIteration % 4 === 1) { $table->addRow(); }
                $offerIteration = 1;
                $offerCount = $offers->count();
                $cell = $table->addCell();
                foreach ($offers as $offer) {
                    if ($offerIteration == 1) {
                        $image = $offer->product->images()->value('url');
                        if ($image) {
                            $cell->addImage($offer->product->images()->value('url'), [
                                'width' => 130,
                            ]);
                        }
                        $cell->addText($offer->product->vendor_code);
                        $cell->addText($offer->product->description);
                    }
                    $cell->addText($offer->option->name . ' - ' . $offer->pivot->qty);

                    if ($offerIteration == $offerCount) {
                        $offerTotalQty = $offers->sum('pivot.qty');
                        $offerPrice = (int) $offer->pivot->price;
                        $offerTotalPrice = $offerTotalQty * $offerPrice;
                        $cell->addText($offerTotalQty . ' шт. * ' . $offerPrice . ' = ' . $offerTotalPrice);
                    }
                    $offerIteration++;
                }
                $productIteration++;
            }
        }

        // Saving the document as OOXML file...
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');

        header('Content-Description: File Transfer');
        header('Content-Disposition: attachment; filename=order.docx');
        header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Expires: 0');
            
        // Download the produced file automatically
        $xmlWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        $xmlWriter->save('php://output');
        exit;
    }

    public function toCsv(Order $order)
    {
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=order.csv"); 
        header("Content-Type: text/csv"); 
        $out = fopen('php://output', 'w');
        fputs($out, $bom =( chr(0xEF) . chr(0xBB) . chr(0xBF) ));
        $offersGroup = collect($order->offers)->groupBy('product_id');
        fputcsv($out, [
            'Модель', 
            'Цвет', 
            'Количество', 
            'Цена',
            'Итого',
        ], ';');
        foreach ($offersGroup as $offers) {
            foreach ($offers as $offer) {
                fputcsv($out, [
                    $offer->pivot->vendor_code,
                    $offer->option->name,
                    $offer->pivot->qty,
                    (int) $offer->pivot->price,
                    $offer->pivot->qty * $offer->pivot->price,
                ], ';');
            }
        }
        fputcsv($out, [
            'Итого', 
            '', 
            $order->totalQty, 
            '', 
            $order->totalPrice,
        ], ';');
        fclose($out);
    }

    public function toOrderForm(Order $order)
    {
        $order->load('offers.product.suppliers');

        $offers = $order->offers->map(function($offer) {
            $offer->supplier = $offer->product->suppliers->count() 
                ? $offer->product->suppliers->first()->name 
                : 'Без поставщика';
            return $offer;
        });

        $offersGroup = $offers->groupBy([
            'supplier',
            function ($item) {
                return $item['product_id'];
            },
        ], $preserveKeys = true);

        return view('admin.orders.blank', compact('offersGroup', 'order'));
    }
}
