<?php

namespace App\Http\Controllers\Admin;

use App\Parser;
use App\Http\Requests\Admin\ParserLink as Request;
use App\Http\Controllers\Controller;

class ParserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.parsers.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $parser = new Parser($request->url);

        if (!empty($parser->errors)) {
            return redirect()
                ->route('admin.parsers.index')
                ->withInput($request->input())
                ->withErrors($parser->errors);
        }

        $parsers = collect([$parser->parseOne()]);
        
        return view('admin.parsers.show', compact('parser', 'parsers'));
    }

    /**
     * Mass store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function massStore(Request $request)
    {
        $parser = new Parser($request->url);

        if (!empty($parser->errors)) {
            return redirect()
                ->route('admin.parsers.index')
                ->withInput($request->input())
                ->withErrors($parser->errors);
        }

        $parsers = $parser->parseMany();
        
        return view('admin.parsers.show', compact('parser', 'parsers'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Parser  $parser
     * @return \Illuminate\Http\Response
     */
    public function show($parser)
    {
        return view('admin.parsers.show');
    }
}
