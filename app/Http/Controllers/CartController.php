<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Offer;
use App\Order;
use Illuminate\Http\Request;

class CartController extends Controller
{

    public function index(Request $request)
    {
        $offers = \Auth::user()->cart ? \Auth::user()->cart->offers : null;
        $offersGroup = $offers ? $offers->groupBy('product_id') : null;

        $totalQty = $offersGroup ? $offers->sum('pivot.count') : 0;
        $totalPrice = $offersGroup ? $offers->sum(function ($offer) {
            return $offer->pivot->count * $offer->product->price;
        }) : 0;

        return view('cart.index', compact('offersGroup', 'totalQty', 'totalPrice'));
    }

    public function add(Request $request, Offer $offer)
    {
        $cart = $this->getCart();
        
        $count = request('count');
        $count = ($count > 0) ? (int) $count : 1;

        $cart->add($offer, $count);
        $cart->save();

        $status = 'Товар ' . $offer->product->vendor_code . ' - ' . $offer->option->name . ' (' . $count . ' шт.) добавлен в корзину!';

        return back()->with('status', $status);
    }

    public function addToCart(Request $request)
    {
        $cart = $this->getCart();

        $count = (int) $request->count ?: 1;
        $offer = Offer::findOrFail($request->offer);

        $cart->add($offer, $count);
        $cart->touch();

        return ['total_qty' => $cart->total_qty + $count];
    }

    public function remove(Offer $offer)
    {
        $cart = \Auth::user()->cart;
        if (is_null($cart) || is_null($cart->offers->find($offer))) {
            $status = 'Товар ' . $offer->product->vendor_code . ' - ' . $offer->option->name . ' отсутствует в корзине!';
            return back()->with('status', $status);
        }

        $cart->remove($offer);
        $cart->touch();

        $status = 'Товар ' . $offer->product->vendor_code . ' - ' . $offer->option->name . ' (1 шт.) удален из корзины!';
        return back()->with('status', $status);
    }

    public function removeAll(Offer $offer)
    {
        $cart = \Auth::user()->cart;
        if (is_null($cart) || is_null($cart->offers->find($offer))) {
            $status = 'Товар ' . $offer->product->vendor_code . ' - ' . $offer->option->name . ' отсутствует в корзине!';
            return back()->with('status', $status);
        }

        $cart->remove($offer, 'all');
        $cart->touch();

        $status = 'Товар ' . $offer->product->vendor_code . ' - ' . $offer->option->name . ' удален из корзины!';
        return back()->with('status', $status);
    }

    public function clearCart()
    {
        $cart = \Auth::user()->cart;
        if (is_null($cart) || is_null($cart->offers)) {
            $status = 'Товары отсутствуют в корзине!';
            return back()->with('status', $status);
        }

        $cart->delete();

        return back();
    }

    public function saveOrder(Request $request)
    {
        $cart = \Auth::user()->cart;
        if (is_null($cart) || is_null($cart->offers)) {
            $status = 'Товары отсутствуют в корзине!';
            return back()->with('status', $status);
        }

        $offers = $cart->offers;
        $order = new Order;
        $order->user_id = $request->user()->id;
        $order->totalQty = $offers->sum('pivot.count');
        $order->totalPrice = $offers->sum(function ($offer) {
            return $offer->pivot->count * $offer->product->price;
        });

        if ($request->comment) {
            $order->comment = $request->comment;
        }

        $order_offers = [];
        foreach ($offers as $offer) {
            $order_offers[$offer->id] = [
                'product_id' => $offer->product->id,
                'option_id' => $offer->option->id,
                'vendor_code' => $offer->product->vendor_code,
                'price' => $offer->product->price,
                'color' => $offer->option->name,
                //'size' => $offer->size,
                'qty' => $offer->pivot->count,
            ];
        }

        \Illuminate\Support\Facades\DB::transaction(function() use ($order, $order_offers) {
            $order->save();
            $order->offers()->sync($order_offers);
        });

        $cart->delete();

        try {
            $request->user()->notify(new \App\Notifications\OrderCreated($order));
            \App\User::find(2)->notify(new \App\Notifications\Admin\OrderCreated($order));
        } catch (\Exception $e) {
            $error = $e->getMessage();
        }
        
        return redirect()->route('user.dashboard')->with('status', 'Заказ оформлен!');
    }

    private function getCart()
    {
        if (\Auth::check() == false) {
            abort(403, 'Unauthorized action.');
        }

        return \Auth::user()->cart ?? \Auth::user()->cart()->save(new Cart);
    }
}
