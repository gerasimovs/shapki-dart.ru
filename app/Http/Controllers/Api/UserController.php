<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserController extends Controller
{
    private $relationship = null;
    private $relationshipId = null;

    public function __construct(Request $request)
    {
        $this->relationship = $request->has('type') ? $request->type : null;
        $this->relationshipId = $request->has('type_id') ? $request->type_id : null;
    }

    public function search(Request $request)
    {
        $models = User::select(['id', 'name', 'email'])
            ->where('name', 'like', '%' . $request->text . '%')
            ->orWhere('email', 'like', '%' . $request->text . '%')
            ->get();

        return response()->json($models);
    }

    public function attach(Request $request)
    {
        $model = User::find($request->id);
        $model->{$this->relationship}()
            ->sync($this->relationshipId, false);

        return $this->get($request);
    }
    
    public function detach(Request $request)
    {
        $model = User::find($request->id);
        $model->{$this->relationship}()
            ->detach($this->relationshipId);

        return $this->get($request);
    }

    public function get(Request $request)
    {
        $users = User::whereHas($this->relationship, function ($query) {
            $query->where('id', $this->relationshipId);
        })->get(['id', 'name', 'email']);

        return response()->json($users);
    }
}
