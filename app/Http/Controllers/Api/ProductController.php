<?php

namespace App\Http\Controllers\Api;

use App\Product;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function search(Request $request)
    {
        $products = Product::whereRaw('MATCH(vendor_code, name, description) AGAINST(? IN BOOLEAN MODE)', [$request->text])
            ->with('images:url')
            ->select(['id', 'vendor_code as title'])
            ->take(10)
            ->get();

        return response()->json($products->keyBy('id'));
    }

    public function attach(Request $request)
    {
        $relationship = $request->type;
        $relationshipId = $request->type_id;

        $product = Product::find($request->id);
        $product->$relationship()->sync($relationshipId, false);

        return $this->get($request);
    }
    
    public function detach(Request $request)
    {
        $relationship = $request->type;
        $relationshipId = $request->type_id;

        $product = Product::find($request->id);
        $product->$relationship()->detach($relationshipId);

        return $this->get($request);
    }

    public function get(Request $request)
    {
        $relationship = $request->type;
        $relationshipId = $request->type_id;

        $products = Product::with('images:url')->whereHas($relationship, function ($query) use ($relationshipId) {
            $query->where('id', $relationshipId);
        })->get(['id', 'vendor_code as title']);

        return response()->json($products->keyBy('id'));
    }
}
