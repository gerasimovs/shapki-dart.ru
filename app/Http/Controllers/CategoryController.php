<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    protected $viewIndex = 'categories.index';
    protected $viewShow = 'categories.show';
    protected $viewSearch = 'categories.search';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected function index(Request $request)
    {
        $perPage = (int) $request->count ?: 12;
        /*$products = [];*/

        /*if (\Auth::check() && \Auth::user()->role) {
            $categoriesIds = \Auth::user()->role->categories()->pluck('id');*/
            $products = Product::withCount('images', 'options')
                /*->whereIn('category_id', $categoriesIds)*/
                ->latest('sorted_at')
                ->latest('id')
                ->paginate($perPage)
            ;
        /*}*/

        return view($this->viewIndex, compact('products'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    protected function show(Request $request, Category $category)
    {
        $categories = $category->children;
        
        $perPage = (int) $request->count ?: 12;
        
        $products = Product::withCount('images', 'options')
            ->whereHas('categories', function($query) use ($category) {
                $query->where('id', $category->id);
            })->orWhere('category_id', $category->id)
            ->latest('sorted_at')
            ->latest('id')
            ->paginate($perPage);

        /*if (\Auth::check() && \Auth::user()->role) {
            $roleId = \Auth::user()->role->id;
            $description = $category->roles()->find($roleId)->pivot->description;
            if ($description) {
                $category->description = $category->description 
                    ? $category->description . ' ' . $description
                    : $description
                ;
            }
        }*/

        return view($this->viewShow, compact('category', 'products', 'categories'));
    }
}
