<?php

namespace App\Http\Controllers;

use Storage;
use Illuminate\Http\Request;
use Illuminate\Http\File;
use Illuminate\Notifications\Messages\MailMessage;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('index', 'test');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home.index');
    }

    public function dashboard(Request $request)
    {
        $orders = $request->user()->orders()->orderBy('id', 'desc') ->get();
        return view('home.dashboard', compact('orders'));
    }

    public function test()
    {

        // $images = \App\Image::withTrashed()->doesntHave('upload')->get();
        // $count = $images->count();
        // foreach ($images as $i => $image) {
        //     if (is_null($image->upload_url)) {
        //         $upload_url = $image->url;
        //     } else {
        //         $upload_url = $image->upload_url;
        //     }
        //     $imageUpload = \App\UploadImage::firstOrCreate(['url' => $upload_url]);
        //     $imageUpload->image_id = $image->id;
        //     $imageUpload->save();
        //     dump($i . ' from ' . $count);
        // }

        // $order = \App\Order::first();
        // \Auth::user()->notify(new \App\Notifications\Admin\OrderCreated($order));
        // dd('ok');

        // $supplier = 23;

        // $categories = \App\Category::whereIn('id', [23, 24, 105, 19, 20, 21, 106, 107, 34, 99, 4, 97, 98, 50, 67, 80])
        //     ->has('products')
        //     ->with(['products' => function($query) {
        //         $query->withTrashed();
        //     }])
        //     ->get();

        // foreach ($categories as $item) {
        //     $item->products->each(function($product) use ($supplier) {
        //         $product->suppliers()->sync($supplier, false);
        //     });
        // }

        sleep(3);
        return 'Функция выполнена';
    }


    public function rename()
    {
        $localHost = parse_url(config('app.url'))['host'];
        $disk = 'images';

        $images = \App\Image::where('url', 'not like', '%shapki-dart%')
            ->get();
        dump($images->count());
        foreach ($images as $image) {
            $url = parse_url($image->url);
            if (isset($url['host']) && $url['host'] != $localHost) {
                $tempImage = tempnam(sys_get_temp_dir(), 'tempImage');
                @$copy = copy($image->url, $tempImage);
                if ($copy) {
                    $file = new File($tempImage);
                    $extension = $file->extension();
                    $path = substr($image->hash, 0, 2) . '/' . substr($image->hash, 2, 2);
                    $url = $image->hash . '.' . $extension;
                    $upload = \App\UploadImage::firstOrNew(['url' => $image->url]);
                    $image->url = config('app.url') . '/images/' . Storage::disk($disk)->putFileAs($path, $file, $url);
                    $image->save();
                    $upload->image_id = $image->id;
                    $upload->save();
                } else {
                    $image->url = 'http://shapki-dart.ru/images/no_photo.png';
                    $image->save();
                }
            } elseif (!isset($url['host'])) {
                $image->url = config('app.url') . $image->url;
                $image->save();
            }
        }
    }

    public function parseCalendars($json)
    {
        $data = json_decode($json);

        /* Тип календаря */
        $types = collect($data->types)->pluck('title', 'slag')->toArray();
        
        /* Категории календарей */
        $menu = array();
        foreach ($data->menu as $menuItem) {
            $menu[$menuItem->slag] = array(
                'title' => $menuItem->title,
                'desc' => $menuItem->desc
            );
            if (isset($menuItem->items)) {
                foreach ($menuItem->items as $menuSubItem) {
                    if ($menuSubItem->id == 117) {
                        continue;
                    }

                    $menu[$menuSubItem->slag] = array(
                        'title' => $menuItem->title . ' ' . $menuSubItem->title,
                        'desc' => $menuSubItem->desc,
                        'parent' => $menuSubItem->parent_slag
                    );

                    if (isset($menuSubItem->items)) {
                        foreach ($menuSubItem->items as $menuSubSubItems) {
                            $menu[$menuSubSubItems->slag] = array(
                                'title' => $menu[$menuSubItem->slag]['title'] . ', ' . $menuSubSubItems->title,
                                'desc' => $menuSubSubItems->desc,
                                'parent' => $menuSubSubItems->parent_slag
                            );
                        }
                    }
                }
            }
        }

        /* Список календарей */
        foreach ($data->list as $productData) {
            if (!isset($menu[$productData->category])) {
                continue;
            }

            $product = \App\Product::firstOrNew(['vendor_code' => $productData->title]);
            if ($product->exists) {
                continue;
            }

            $category = \App\Category::firstOrCreate(['name' => $menu[$productData->category]['title']], ['category_id' => 2]);
            $product->category_id = $category->id;
            $product->description = strip_tags($menu[$productData->category]['desc']);
            $product->price = $productData->price;
            $product->save();

            $imageUrl = 'http://www.top-calendar.ru/' . $productData->img_big;

            $image = \App\Image::firstOrCreate(['url' => $imageUrl], ['hash' => '']);

            $product->images()->sync([$image->id]);

        }
    }
}
