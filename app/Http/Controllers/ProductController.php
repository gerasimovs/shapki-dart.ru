<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    protected $viewShow = 'products.show';
    protected $viewSearch = 'products.search';

    /**
     * Display the product.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    protected function show(Product $product)
    {
        $product->load(['options' => function ($query) {
                $query->where('status', 1);
            }]);

        $product->load(['images' => function ($query) {
            return $query->orderBy('image_product.sort','ASC');
        }]);
        
        $product->load('related');
        
        return view($this->viewShow, compact('product'));
    }

    /**
     * Display the products by search params.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    protected function search(Request $request)
    {
        /*if (\Auth::check() == false || (\Auth::check() && \Auth::user()->role) == false) {
            abort(403, 'Unauthorized action.');
        }*/
        
        $perPage = (int) $request->count ?: 12;
        /*$categoriesIds = \Auth::user()->role->categories()->pluck('id');*/

        $products = Product::whereRaw(
                'MATCH(vendor_code, name, description) AGAINST(? IN BOOLEAN MODE)',
                [$request->text]
            )
            /*->whereIn('category_id', $categoriesIds)*/
            ->withCount('images', 'options')
            ->with('images')
            ->paginate($perPage);

        return view($this->viewSearch, compact('products'))->withInput($request->all());
    }

}
