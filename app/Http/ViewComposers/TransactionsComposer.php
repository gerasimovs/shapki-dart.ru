<?php

namespace App\Http\ViewComposers;

use Auth;
use Illuminate\View\View;

class TransactionsComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $transactions = Auth::check() && Auth::user()->account
            ? Auth::user()->account->transactions()->paginate(100) 
            : collect()
        ;

        $view->with('transactions', $transactions);
    }
}
