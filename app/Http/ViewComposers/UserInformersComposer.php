<?php

namespace App\Http\ViewComposers;

use Auth;
use Illuminate\View\View;

class UserInformersComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        if (Auth::check() == false) {
            return;
        }

        static $userInformers;

        if (!is_array($userInformers)) {
            $user = Auth::user();
            $userInformers = [
                'notifications' => $user->unreadNotifications()->count(),
                'orders' => $user->orders()->count(),
                'cart' => $user->cart 
                    ? $user->cart->total_quantity
                    : null,
            ];

            if ($account = Auth::user()->account) {
                $userInformers += [
                    'balance'       => $account->balance,
                    'transactions'  => $account->transactions()->count(),
                ];
            }
        }

        $view->with('userInformers', $userInformers);
    }
}
