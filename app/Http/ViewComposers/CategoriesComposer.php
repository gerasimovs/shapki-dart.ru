<?php

namespace App\Http\ViewComposers;

use App\Category;
use Illuminate\Support\Facades\Cache;
use Illuminate\View\View;

class CategoriesComposer
{
    protected static $categories;

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        if (!self::$categories) {
            self::$categories = /*Cache::remember('categories', now()->addHour(), function () {
                return*/ Category::query()
                    ->withCount('products', 'children')
                    ->withDepth()
                    ->defaultOrder()
                    ->get();
            /*});*/
        }

        $current = request()->route('category');
        self::$categories->each(function ($category) use ($current) {
            $category->selected = $current && ($category->isAncestorOf($current) || $category->is($current));
            $category->is_show = $category->children_count || $category->products_count;
        });

        $view->with('categories', self::$categories->toTree());
    }
}
