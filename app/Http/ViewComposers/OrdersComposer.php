<?php

namespace App\Http\ViewComposers;

use Auth;
use Illuminate\View\View;

class OrdersComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $orders = Auth::check()
            ? Auth::user()->orders()->latest()->paginate(100) 
            : collect()
        ;

        $view->with('orders', $orders);
    }
}
