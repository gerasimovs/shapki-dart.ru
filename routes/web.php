<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'CategoryController@index')->name('catalog');
Route::get('category/{category}', 'CategoryController@show')->name('catalog.show');
Route::get('product/{product}', 'ProductController@show')->name('products.show');
Route::get('search', 'ProductController@search')->name('products.search');

Route::group(['middleware' => 'auth'], function () {
    Route::group(['prefix' => 'user', 'as' => 'user.'], function () {
        Route::get('/', 'HomeController@dashboard')->name('dashboard');
        Route::get('notifications', 'HomeController@notifications')->name('notifications.index');
        Route::get('orders', 'OrderController@index')->name('orders.index');
        Route::get('orders/{order}', 'OrderController@show')->name('orders.show');
    });
});


Route::group(['prefix' => 'cart', 'as' => 'cart.'], function () {
    Route::get('/', 'CartController@index')->name('index');
    Route::post('add-to-cart', 'CartController@addToCart')->name('add-to-cart');
    Route::get('add/{offer}', 'CartController@add')->name('add');
    Route::get('remove/{offer}', 'CartController@remove')->name('remove');
    Route::get('delete/{offer}', 'CartController@removeAll')->name('delete');
    Route::get('clear', 'CartController@clearCart')->name('clear');
    Route::post('order', 'CartController@saveOrder')->name('order');
});

Route::any('test', 'HomeController@test');

Route::redirect('home', '/')->name('home');
Route::view('about', 'pages.about')->name('about');
Route::view('rules', 'pages.rules')->name('rules');
Route::view('contacts', 'pages.contacts')->name('contacts');
Route::view('feedback', 'pages.feedback')->name('feedback');

Route::get('catalog/{path?}', function($path = '/') {
    return redirect($path);
})->where('path', '.*');;

Route::get('{page}', 'PageController@show')->name('pages.show');
