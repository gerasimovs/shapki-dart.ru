<?php
/*
 * 'middleware' => 'role:admin',
 * 'prefix' => 'admin', 
 * 'namespace' => 'Admin',
 */

use \App\Http\Controllers\Admin\CrmController;
use Illuminate\Support\Facades\Route;

Route::bind('product', function ($id) {
	if (in_array('admin', Route::current()->middleware())) {
		return App\Product::withTrashed()->findOrFail($id);
	} else {
		return App\Product::findOrFail($id);
	}
});

Route::bind('category', function ($id) {
    if (in_array('admin', Route::current()->middleware())) {
        return App\Category::withTrashed()->findOrFail($id);
    } else {
        return App\Category::findOrFail($id);
    }
});

Route::get('/', function() { return view('admin.index'); })->name('dashboard');
Route::get('notifications', 'NotificationController@index')->name('notifications.index');
Route::resource('pages', 'PageController');

/* Users */

Route::group(['prefix' => 'users', 'as' => 'users.'], function () {
    Route::post('{user}/auth', 'UserController@auth')->name('auth');
    Route::post('{user}/access', 'UserController@setCategoriesAccess')->name('access');
    Route::resource('invites', 'InviteController');
    Route::post('invites/{invite}/send', 'InviteController@send')->name('invites.send');
    Route::resource('{user}/transactions', 'TransactionController');
});
Route::resource('users', 'UserController');
Route::resource('roles', 'RoleController');

/* Categories */

Route::group(['prefix' => 'categories', 'as' => 'categories.'], function () {
    Route::get('{category}/up', 'CategoryController@up')->name('up');
    Route::get('{category}/down', 'CategoryController@down')->name('down');
    Route::put('{category}/recovery', 'CategoryController@recovery')->name('recovery');
    Route::get('{category}/exportToWord', 'ExportController@toWord')->name('toWord');
    Route::get('{category}/exportToCsv', 'ExportController@categoryToCsv')->name('toCsv');
});
Route::resource('categories', 'CategoryController');

/* Products*/

Route::group(['prefix' => 'products', 'as' => 'products.'], function () {
    Route::get('search', 'ProductController@search')->name('search');
    Route::get('create/{product}', 'ProductController@create')->name('createAs');
    Route::get('{product}/sort', 'ProductController@sorting')->name('sorting');
    Route::post('move', 'ProductController@moving')->name('moving');
    Route::post('to-category', 'ProductController@moving')->name('toCategory');
    Route::post('add-supplier', 'ProductController@addSupplier')->name('addSupplier');
    Route::post('exportToWord', 'ExportController@productsToWord')->name('toWord');
    Route::post('exportToBlock', 'ExportController@productsToBlock')->name('toBlock');
    Route::post('exportToCsv', 'ExportController@productsToCsv')->name('toCsv');
    Route::post('sortToTop', 'ProductController@sortToTop')->name('toTop');
    Route::post('{product}/restore', 'ProductController@restore')->name('restore');

    Route::post('parsers/{parser}', 'ParserProductController@parse')->name('parsers.parse');
    Route::resource('parsers', 'ParserProductController')->only(['index', 'edit', 'update']);
});
Route::resource('products', 'ProductController');
Route::resource('brands', 'BrandController');

/* Orders */

Route::group(['prefix' => 'orders', 'as' => 'orders.'], function () {
    Route::get('{order}/blank', 'OrderController@toOrderForm')->name('blank');
    Route::get('{order}/csv', 'OrderController@toCsv')->name('toCsv');
    Route::get('{order}/doc', 'OrderController@toWord')->name('toWord');
    Route::get('{order}/crm', 'CrmController@invoice')->name('toCrm');
});
Route::resource('orders', 'OrderController');
Route::resource('carts', 'CartController');
Route::resource('suppliers', 'SupplierController');

Route::group(['prefix' => 'images', 'as' => 'images.'], function () {
    Route::get('uploads', 'ImageController@uploads')->name('uploads');
    Route::post('uploads', 'ImageController@stores')->name('stores');
});
Route::resource('images', 'ImageController');

/* Utils */

Route::prefix('import')->group(function () {
    Route::get('/', 'ImportController@index')->name('import.index');
    Route::post('/', 'ImportController@parse')->name('import.parse');
    Route::post('/{import}/check', 'ImportController@check')->name('import.check');
    Route::get('/{import}/check', 'ImportController@checked')->name('import.checked'); 
    Route::get('/finish', 'ImportController@finish')->name('import.finish'); 
    Route::get('/{import}', 'ImportController@show')->name('imports.show');
    Route::post('/{import}', 'ImportController@store')->name('import.store'); 
});

Route::post('/parsers/mass', 'ParserController@massStore')->name('parsers.store.mass');
Route::resource('parsers', 'ParserController')
    ->only('index', 'store', 'show');

Route::group(['prefix' => 'mailings', 'as' => 'mailings.'], function () {
    Route::post('/preview', 'MailingController@preview')->name('preview');
    Route::get('/{mailing}/prepare', 'MailingController@prepare')->name('prepare');
    Route::post('/{mailing}/send', 'MailingController@send')->name('send');
});
Route::resource('mailings', 'MailingController');

Route::get('helper/logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index')->name('helpers.logs');
Route::post('helper/combine', 'HelperController@combine')->name('helpers.combine');
Route::post('helper/word', 'HelperController@fromWord')->name('helpers.fromWord');
Route::get('helper', 'HelperController@index')->name('helpers.index');
