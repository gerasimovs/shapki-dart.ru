<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/', function () {
    return ['status' => 'ok'];
});
//Route::group(['middleware' => ['auth']], function () {
    Route::post('add-to-cart', 'CartController@addToCart')->name('add-to-cart');
//});

Route::get('products.get',    'Api\\ProductController@get');
Route::get('products.search', 'Api\\ProductController@search');
Route::get('products.attach', 'Api\\ProductController@attach');
Route::get('products.detach', 'Api\\ProductController@detach');

Route::get('users.get',       'Api\\UserController@get');
Route::get('users.search',    'Api\\UserController@search');
Route::get('users.attach',    'Api\\UserController@attach');
Route::get('users.detach',    'Api\\UserController@detach');

