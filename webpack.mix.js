const mix = require('laravel-mix');
const path = require('path');
const glob = require('glob-all');

const assets = './resources/';

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .options({
        purifyCss: {
            moduleExtensions: ['php', 'vue', 'js'],
            paths: glob.sync([
                path.join(__dirname, 'resources/**/*.blade.php'),
                path.join(__dirname, 'resources/**/*.vue'),
                path.join(__dirname, 'resources/**/*.js')
            ]),
            purifyOptions: {
                whitelist: [
                    'active', '*dropdown-content*', '*select-dropdown*', '*select-wrapper*', 'material-icons', '*material-icons.mi*', 'toast', 'progress*',
                    '*waves-light', '*type=checkbox*', '*switch*', 'loader*', '*chip*', '*indicators*', '*ymaps*', 'sidenav-overlay'
                ],
                rejected: true
            }
        }
    })

    .sass(assets + 'sass/app.scss', 'public/css')
    .sass(assets + 'sass/spa.scss', 'public/css')
    .js(assets + 'js/app.js', 'public/js')
    .js(assets + 'js/spa.js', 'public/js')
    .extract()
    
    .version()
    .sourceMaps();