<?php

return [

    'www.100sp.ru' => [
        'class' => \App\Parsers\_100spRu::class,
        'auth' => true,
        'url' => 'https://www.100sp.ru/login.php',
        'data' => [
            'email' => env('PARSER_100SP_LOGIN'),
            'password' => env('PARSER_100SP_PASSWORD'),
            'save_my_login' => 'on',
            'Submit' => 'Войти',
        ],
    ],

    'kupalniki-nsk.ru' => [
        'class' => \App\Parsers\KupalnikNskRu::class,
        'auth' => true,
        'url' => 'https://kupalniki-nsk.ru/login/',
        'data' => [
            'email' => env('PARSER_SHAPKINSK_LOGIN'),
            'password' => env('PARSER_SHAPKINSK_PASSWORD'),
        ],
    ],

    'shapki-nsk.ru' => [
        'class' => \App\Parsers\ShapkiNskRu::class,
        'auth' => true,
        'url' => 'https://shapki-nsk.ru/login/',
        'data' => [
            'email' => env('PARSER_SHAPKINSK_LOGIN'),
            'password' => env('PARSER_SHAPKINSK_PASSWORD'),
        ],
    ],

    'trafik-nsk.ru' => [
        'class' => \App\Parsers\TrafikNskRu::class,
        'auth' => false,
    ],

    'konareva.com' => [
        'class' => \App\Parsers\KonarevaCom::class,
        'auth' => false,
    ],

    'перчатки-варежки.рф' => [
        'class' => \App\Parsers\PerchatkiVarezhki::class,
        'host' => 'xn----7sbbhmehlfng4cogy2f.xn--p1ai',
        'auth' => false,
    ],

    'xn----7sbbhmehlfng4cogy2f.xn--p1ai' => [
        'class' => \App\Parsers\PerchatkiVarezhki::class,
        'auth' => false,
    ],

    'avili-style.ru' => [
        'class' => \App\Parsers\AviliStyleRu::class,
        'auth' => true,
        'url' => 'http://avili-style.ru/auth/?login=yes',
        'data' => [
            'AUTH_FORM' => 'Y',
            'TYPE' => 'AUTH',
            'backurl' => '/auth/',
            'USER_LOGIN' => env('PARSER_AVILI_LOGIN'),
            'USER_PASSWORD' => env('PARSER_AVILI_PASSWORD'),
            'USER_REMEMBER' => 'Y',
            'Login' => 'Войти',
        ],
    ],

    'nskshapki.ru' => [
        'class' => \App\Parsers\NskShapkiRu::class,
        'url' => 'https://nskshapki.ru/user/login/returnUrl/%252Fuser%252Findex',
        'auth' => true,
        'data' => [
            'LoginForm' => [
                'username' => env('PARSER_NSKSHAPKI_LOGIN'),
                'password' => env('PARSER_NSKSHAPKI_PASSWORD'),
            ],
            'yt0' => 'Войти',
        ]
    ]

];
