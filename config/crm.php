<?php

return [
    'webhook' => env('BITRIX24_WEBHOOK'),
    'mycompany' => env('BITRIX24_COMPANY'),
    'user_fields' => [
        'orderid_in_invoice' => env('BITRIX24_UF_INVOICEID'),
    ],
];
