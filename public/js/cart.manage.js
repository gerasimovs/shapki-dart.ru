document.addEventListener('DOMContentLoaded', function(){
    'use strict';

    let token = document.querySelector('meta[name="csrf-token"]').content;
    let action = 'http://shapki-dart.ru/cart/add-to-cart';

    function buttonAddSpinner(button) {
        button.disabled = true;
        button.innerHTML = '<i class="fas fa-spinner fa-pulse"></i>  добавление...';
    }

    function buttonAddSuccess(button) {
        button.classList.add('text-success');
        button.innerHTML = '<i class="fas fa-check"></i>  добавлено';
    }

    function buttonAddError(button) {
        button.classList.add('text-danger');
        button.innerHTML = '<i class="fas fa-times"></i> ошибка';
    }

    function buttonResetDefault(button, cbutton) {
        $(button).popover('dispose');
        button.parentNode.replaceChild(cbutton, button);
    }

    function cartSetCount(count) {
        let cart = document.querySelector('.cart-button');
        let badge = cart.querySelector('.badge');
        if (cart.parentNode.style.display == 'none') {
            cart.parentNode.style.display = 'initial';
        }
        badge.textContent = count;
    }

    function addToCart(form) {
        let timer = 1500;
        let button = form.querySelector('button');
        let buttonClone = button.cloneNode(true);
        buttonAddSpinner(button);
        fetch(action, { 
            method: 'POST',
            credentials: 'include',
            headers: {
                'X-CSRF-Token': token,
            },
            body: new FormData(form),
        }).then(function(response) {
            if (response.status !== 200) {
                throw new Error(response.status);
            } else {
                return response.json();
            }
        }).then(function(json) {
            cartSetCount(json.total_qty);
            buttonAddSuccess(button);
        }).catch(function(err) {
            if (err.message == 403) {
                timer = 2500;
                $(button).popover({
                    content: 'Функция доступна только зарегистрированным пользователям',
                    placement: 'bottom',
                    trigger: 'manual'
                })
                .popover('show');
            }
            buttonAddError(button);
        }).then(function() {
            setTimeout(buttonResetDefault, timer, button, buttonClone);
            form.reset();
        });

    }

    $('form.add-to-cart').on('submit', function() {
        if (typeof window['fetch'] == 'function') {
            event.preventDefault();
            addToCart(this);
        }
    });


});
