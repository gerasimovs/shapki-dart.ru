function setImageOrder(value) {
    let image = $(this).closest('.image-card');
    let order = $(this).closest('.order-group').find('.order-current');
    let newOrder = +order.val()+value;

    image.css('order', newOrder);
    order.val(newOrder);
}

function createInputBlock(name) {
    let input = document.createElement('input');
    input.type = 'text';
    input.name = name;
    input.className = 'form-control form-control-sm';
    return input;
}

function insertInputBlockBefore(inputName, selector) {
    let newInput = createInputBlock(inputName);
    let addBlock = document.querySelector(selector);
    addBlock.parentNode.insertBefore(newInput, addBlock); 
}

function appendProductsChecked(formData) {
    document.querySelectorAll('input[type="checkbox"]:checked').forEach(function(productChecked) {
        formData.append('products[]', productChecked.value);
    });
}

function productDelete(form) {
    let button = form.querySelector('.product-delete');
    button.innerHTML = '<i class="fas fa-spinner fa-pulse"></i>';

    fetch(button.getAttribute('formaction'), { 
        method: 'POST',
        credentials: 'include',
        headers: {
            'X-CSRF-Token': token,
            'X-Requested-With': 'XMLHttpRequest',
        },
        body: new FormData(form),
    }).then(function(response) {
        if (response.status !== 200) {
            throw new Error(response.status);
        } else {
            return response.text();
        }
    }).then(function(text) {
        let doc = document.implementation.createHTMLDocument("products-list");
        let productTr = button.closest('tr');
        $('[data-tooltip="true"]', $(productTr)).tooltip('dispose');
        doc.documentElement.innerHTML = text;
        let productNew = doc.querySelector('#' + productTr.getAttribute('id'));
        productTr.parentNode.replaceChild(productNew, productTr);
        $('[data-tooltip="true"]', $(productNew)).tooltip();
    }).catch(function(err) {
        console.log(err);
    })
}

function saveProducts(action, method) {
    let formData = new FormData;
    appendProductsChecked(formData);

    fetch(action, {
        method: method,
        credentials: 'include',
        headers: {
            'X-CSRF-Token': token,
            'X-Requested-With': 'XMLHttpRequest',
        },
        body: formData
    }).then(function(response) {
        if (response.status !== 200) {
            throw new Error(response.status);
        } else {
            let contentDispositionHeader = response.headers.get('Content-Disposition');
            if (contentDispositionHeader !== null) {
                let contentDispositionHeaderResult = contentDispositionHeader.split(';')[1].trim().split('=')[1];
                filename = contentDispositionHeaderResult.replace(/"/g, '');
            }
            return response.blob();
        }
    }).then(function(blob) {
        let downloadlink = document.createElement('a');
        downloadlink.href = window.URL.createObjectURL(blob);
        downloadlink.download = filename;
        if (window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveBlob(blob, filename);
        } else {
            downloadlink.click();
        }
    }).catch(function(err) {
        console.log(err);
    });
}

function productRestore(form) {
    let button = form.querySelector('.product-restore');
    button.innerHTML = '<i class="fas fa-spinner fa-pulse"></i>';

    fetch(button.getAttribute('formaction'), { 
        method: 'POST',
        credentials: 'include',
        headers: {
            'X-CSRF-Token': token,
            'X-Requested-With': 'XMLHttpRequest',
        },
        body: new FormData(form),
    }).then(function(response) {
        if (response.status !== 200) {
            throw new Error(response.status);
        } else {
            return response.text();
        }
    }).then(function(text) {
        let doc = document.implementation.createHTMLDocument("products-list");
        let productTr = button.closest('tr');
        $('[data-tooltip="true"]', $(productTr)).tooltip('dispose');
        doc.documentElement.innerHTML = text;
        let productNew = doc.querySelector('#' + productTr.getAttribute('id'));
        productTr.parentNode.replaceChild(productNew, productTr);
        $('[data-tooltip="true"]', $(productNew)).tooltip();
    }).catch(function(err) {
        console.log(err);
    })
}

document.addEventListener('DOMContentLoaded', function(){
    
    let token = document.querySelector('meta[name="csrf-token"]').content;
    
    $('.order-dec').on('click', function(){
        setImageOrder.call(this, -1)
    });

    $('.order-inc').on('click', function(){
        setImageOrder.call(this, +1)
    });

    $('.images-add button').on('click', function(){
        insertInputBlockBefore('images_url[]', '.images-add');
    });

    $('.options-add button').on('click', function(){
        insertInputBlockBefore('options_name[]', '.options-add');
    });

    $('.related-add button').on('click', function(){
        insertInputBlockBefore('related_code[]', '.related-add');
    });

    let productsMoving = document.querySelector('#products-moving')
    if (productsMoving) {
        productsMoving.addEventListener('click', function() {
            let action = this.dataset.action;
            let formData = new FormData;

            appendProductsChecked(formData);
            formData.append('category_id', document.querySelector('#categories').value);

            fetch(action, {
                method: this.dataset.method,
                credentials: 'include',
                headers: {
                    'X-CSRF-Token': token,
                    'X-Requested-With': 'XMLHttpRequest',
                },
                body: formData
            }).then(function(response) {
                if (response.status !== 200) {
                    throw new Error(response.status);
                } else {
                    return response.text();
                }
            }).then(function(text) {
                console.log('Успешно');
            }).catch(function(err) {
                console.log(err);
            })
        });
    }

    let productsCheck = document.getElementById('products-check');
    if (productsCheck) {
        productsCheck.addEventListener('click', function() {
            document.querySelectorAll('input[type="checkbox"]').forEach(function(product) {
                product.checked = true;
            });
        });
    }

    let productsToCsv = document.getElementById('products-toCsv');
    if (productsToCsv) {
        productsToCsv.addEventListener('click', function() {
            saveProducts(this.dataset.action, this.dataset.method);
        });
    }

    let productsToWord = document.getElementById('products-toWord');
    if (productsToWord) {
        productsToWord.addEventListener('click', function() {
            saveProducts(this.dataset.action, this.dataset.method);
        });
    }

    document.body.addEventListener('submit', function(event) {
        if ( event.target.querySelector('.product-delete') ) {
            console.log('DELETE!');
            event.preventDefault();
            productDelete(event.target);
        } else if ( event.target.querySelector('.product-restore') ) {
            console.log('RESTORE!');
            event.preventDefault();
            productRestore(event.target);
        }
    });


    // let productsDelete = document.querySelectorAll('.product-delete')
    // if (productsDelete) {
    //     productsDelete.forEach(function(button){
    //         button.closest('form').addEventListener('submit', function() {
    //             event.preventDefault();
    //             button.innerHTML = '<i class="fas fa-spinner fa-pulse"></i>';
    //             fetch(button.getAttribute('formaction'), { 
    //                 method: 'POST',
    //                 credentials: 'include',
    //                 headers: {
    //                     'X-CSRF-Token': token,
    //                     'X-Requested-With': 'XMLHttpRequest',
    //                 },
    //                 body: new FormData(this),
    //             }).then(function(response) {
    //                 if (response.status !== 200) {
    //                     throw new Error(response.status);
    //                 } else {
    //                     return response.text();
    //                 }
    //             }).then(function(text) {
    //                 let doc = document.implementation.createHTMLDocument("products-list");
    //                 let productTr = button.closest('tr');
                    
    //                 doc.documentElement.innerHTML = text;
    //                 productTr.parentNode.replaceChild(doc.querySelector('#' + productTr.getAttribute('id')), productTr);
    //             }).catch(function(err) {
    //                 console.log(err);
    //             })
    //         });
    //     });
    // }




});