function debounce(fn, delay = 300) {
    var timeoutID = null;

    return function () {
        clearTimeout(timeoutID);

        var args = arguments;
        var that = this;

        timeoutID = setTimeout(function () {
            fn.apply(that, args);
        }, delay);
    }
};

document.addEventListener('DOMContentLoaded', function(){
    window.token = document.querySelector('meta[name="csrf-token"]').content;

    $('[data-tooltip="true"]').tooltip();

    let copyUrlBtns = document.querySelectorAll('.copy-url');  
    if (copyUrlBtns) {
        copyUrlBtns.forEach(function(entry) {
            entry.addEventListener('click', function(event) {  
                window.getSelection().removeAllRanges();
                
                let imageLink = this.nextElementSibling;  
                let range = document.createRange();  
                range.selectNode(imageLink);  
                window.getSelection().addRange(range);  

                try {  
                    let successful = document.execCommand('copy');  
                    let msg = successful ? 'successful' : 'unsuccessful';  
                    console.log('Copy url command was ' + msg + ': ' + range);  
                } catch(err) {  
                    console.log('Oops, unable to copy');  
                }  

                window.getSelection().removeAllRanges();  
            });
        });
    }

    let formDelete = document.querySelectorAll('.form-delete');
    if (formDelete) {
        formDelete.forEach(function(form) {
            form.addEventListener('submit', function(event) {
                let form = this;
                let tr = form.closest('tr');
                let button = this.querySelector('[type="submit"]');

                button.innerHTML = '<i class="fas fa-spinner fa-pulse"></i>';
                $('[data-tooltip="true"]', $(tr)).tooltip('dispose');
                fetch(form.action, {
                    method: 'POST',
                    credentials: 'include',
                    headers: {
                        'X-CSRF-Token': token,
                        'X-Requested-With': 'XMLHttpRequest',
                    },
                    body: new FormData(form)
                }).then(function(response) {
                    if (response.status !== 200) {
                        throw new Error(response.status);
                    } else {
                        return response.text();
                    }
                }).then(function(text) {
                    tr.remove();
                    console.log('Успешно');
                }).catch(function(err) {
                    console.log(err);
                })
                event.preventDefault();
            });
        });
    }

});