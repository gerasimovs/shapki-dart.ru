var vueApp;

document.addEventListener('DOMContentLoaded', function(){
    'use strict';

    if ($('[name="message"]').length > 0 && $('[name="salutation"]').length > 0) {
        $('[name="message"]').summernote();
        // $('[name="salutation"]').summernote();

        $('[name="message"]').on('summernote.blur', function() {
            $(this).summernote('saveRange');
        });
    }

    Vue.directive('debounce', (el, binding) => {
        if (binding.value !== binding.oldValue) {
            el.oninput = debounce(ev => {
                el.dispatchEvent(new Event('change'));
            }, parseInt(binding.value) || 300);
        }
    });

    vueApp = new Vue({

        data: {
            type: null,
            type_id: null,
            entity: null,

            keywords: null,
            userdata: null,
            products: [],
            results: [],
            entities: [],
        },

        computed: {
            token() {
                return document.querySelector('meta[name="csrf-token"]').content;
            }
        },

        watch: {
            keywords: function (query, oldQuery) {
                axios
                    .get('/api/products.search', {
                        params: {
                            text: query,
                        },
                    })
                    .then(response => (this.results = response.data));
            },
            userdata: function (query, oldQuery) {
                if (query.length < 3) { return; }
                axios
                    .get('/api/' + this.entity + '.search', {
                        params: {
                            text: query,
                        },
                    })
                    .then(response => (this.results = response.data));
            },
        },
        
        methods: {
            highlight(text) {
                return text.replace(new RegExp(this.keywords, 'gi'), '<span class="highlighted">$&</span>');
            },
            insertInMessage(productId) {
                let message = document.querySelector('[name="message"]');
                if (undefined == message) { console.log('message is undefined'); return; }

                let insertText = '[[Product:'+productId+']]';

                let supernote = $(message);

                supernote.summernote('restoreRange');
                supernote.summernote('focus');
                supernote.summernote('insertText', insertText);

                return;

                if (document.selection) {
                    message.focus();
                    sel = document.selection.createRange();
                    sel.text = insertText;
                } else if (message.selectionStart || message.selectionStart == '0') {
                    var startPos = message.selectionStart;
                    var endPos = message.selectionEnd;
                    message.value = message.value.substring(0, startPos)
                        + insertText
                        + message.value.substring(endPos, message.value.length);
                } else {
                    message.value += insertText;
                }
            },
            attach(entityId) {
                axios
                    .get('/api/' + this.entity + '.attach', {
                        params: {
                            type: this.type,
                            type_id: this.type_id,
                            id: entityId,
                        },
                    })
                    .then(response => (this.entities = response.data));
            },
            detach(entityId) {
                axios
                    .get('/api/' + this.entity + '.detach', {
                        params: {
                            type: this.type,
                            type_id: this.type_id,
                            id: entityId,
                        },
                    })
                    .then(response => (this.entities = response.data));
            },
            getEntities() {
                axios
                    .get('/api/' + this.entity + '.get', {
                        params: {
                            type: this.type,
                            type_id: this.type_id,
                        },
                    })
                    .then(response => (this.entities = response.data));
            }
        },

        created: function () {
            let type = document.querySelector('[name=type]');
            let typeId = document.querySelector('[name=type_id]');
            let entity = document.querySelector('[name=entity]');

            if (type) { this.type = type.value; }
            if (typeId) { this.type_id = typeId.value; }
            if (entity) { 
                this.entity = entity.value; 
                this.getEntities();
            }
        }

    });

    // mount
    if (document.getElementById('app')) {
        vueApp.$mount('#app')
    }

});