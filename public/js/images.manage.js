document.addEventListener('DOMContentLoaded', function(){
    'use strict';

    let uploadProgress = document.getElementById('upload-progress');
    let token = document.querySelector('meta[name="csrf-token"]').content;

    let jsDropZone = new Dropzone('#upload-dropzone', {
        url: '/admin/images/uploads',
        headers: {
            'X-CSRF-Token': token,
        },
        withCredentials: true,
        paramName: 'image',
        thumbnailWidth: 80,
        thumbnailHeight: 80,
        parallelUploads: 1,
        previewTemplate: document.getElementById('template').innerHTML,
        autoQueue: true,
        previewsContainer: '#upload-previews',
    });

    jsDropZone.on('addedfile', function(file) {
        file.previewElement.querySelector('.start').onclick = function() { jsDropZone.enqueueFile(file); };
    });

    jsDropZone.on('totaluploadprogress', function(progress) {
        uploadProgress.querySelector('.progress-bar').style.width = progress + '%';
        uploadProgress.querySelector('span').innerHTML = progress + '%';
        uploadProgress.querySelector('.sr-only').innerHTML = progress + '% Complete';
    });

    jsDropZone.on("sending", function(file) {
        uploadProgress.style.opacity = "1";
        file.previewElement.querySelector(".start").setAttribute("disabled", "disabled");
    });

    jsDropZone.on("queuecomplete", function(progress) {
        uploadProgress.style.opacity = "0";
    });

});

document.addEventListener('DOMContentLoaded', function(){
    'use strict';

    let copyUrlBtns = document.querySelectorAll('.copy-url');
    copyUrlBtns.forEach(function(entry) {
        entry.addEventListener('click', function(event) {  
            window.getSelection().removeAllRanges();
            
            let imageLink = this.nextElementSibling;  
            let range = document.createRange();  
            range.selectNode(imageLink);  
            window.getSelection().addRange(range);  

            try {  
                let successful = document.execCommand('copy');  
                let msg = successful ? 'successful' : 'unsuccessful';  
                console.log('Copy url command was ' + msg + ': ' + range);  
            } catch(err) {  
                console.log('Oops, unable to copy');  
            }  

            window.getSelection().removeAllRanges();  
        });
    });
});
